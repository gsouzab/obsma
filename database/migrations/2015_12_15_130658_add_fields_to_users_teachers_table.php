<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsersTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_teachers', function (Blueprint $table) {
          $table->string('previuos_participations');
          $table->string('graduation_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_teachers', function (Blueprint $table) {
          $table->dropColumn('previuos_participations');
          $table->dropColumn('graduation_type');
        });
    }
}
