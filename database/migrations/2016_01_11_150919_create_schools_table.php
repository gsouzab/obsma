<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->increments('id');
            $table->date('year');
            $table->string('name');
            $table->integer('code');
            $table->string('type');
            $table->string('administrative_department');
            $table->string('address');
            $table->string('neighborhood');
            $table->string('zip_code');
            $table->string('mailbox');
            $table->string('phone_number');
            $table->string('email');


            $table->integer('state_id')->unsigned()->index();
            $table->integer('city_id')->unsigned()->index();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('schools');
    }
}
