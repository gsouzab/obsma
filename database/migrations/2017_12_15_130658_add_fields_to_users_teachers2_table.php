<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsersTeachers2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_teachers', function (Blueprint $table) {
          $table->string('other_gender')->nullable();
          $table->string('race')->nullable();;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_teachers', function (Blueprint $table) {
          $table->dropColumn('other_gender');
          $table->dropColumn('race');
        });
    }
}
