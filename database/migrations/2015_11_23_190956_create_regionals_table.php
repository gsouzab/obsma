<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('regionals', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->string('place');
          $table->string('address');
          $table->string('complement');
          $table->string('zip_code');
          $table->string('neighborhood');
          $table->integer('city_id')->index();
          $table->string('phone_number');
          $table->string('fax_number');
          $table->string('coordinator_name');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('regionals');
    }
}
