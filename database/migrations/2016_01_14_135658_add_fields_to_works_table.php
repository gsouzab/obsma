<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('works', function (Blueprint $table) {
          $table->integer('category_id')->unsigned()->index();
          $table->integer('participation_type_id')->unsigned()->index();
          $table->integer('teachers_total');
          $table->integer('students_total');
          $table->text('sent_material');
          $table->text('abstract');
          $table->text('methodology');
          $table->text('results');
          $table->integer('inscription_number');
          $table->boolean('is_valid');
          $table->timestamp('validation_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('works', function (Blueprint $table) {
          $table->dropColumn('category_id');
          $table->dropColumn('participation_type_id');
          $table->dropColumn('teachers_total');
          $table->dropColumn('students_total');
          $table->dropColumn('sent_material');
          $table->dropColumn('abstract');
          $table->dropColumn('methodology');
          $table->dropColumn('results');
          $table->dropColumn('inscription_number');
          $table->dropColumn('is_valid');
          $table->dropColumn('validation_date');
        });
    }
}
