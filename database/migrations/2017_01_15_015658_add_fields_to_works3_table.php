<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToWorks3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('works', function (Blueprint $table) {
          $table->string('area')->nullable();
          $table->string('other_area')->nullable();
          $table->string('keywords')->nullable();
          $table->boolean('has_fiocruz_material')->nullable();
          $table->text('fiocruz_fonts')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('works', function (Blueprint $table) {
          $table->dropColumn('area');
          $table->dropColumn('other_area');
          $table->dropColumn('has_fiocruz_material');
          $table->dropColumn('fiocruz_fonts');
          $table->dropColumn('keywords');
        });
    }
}
