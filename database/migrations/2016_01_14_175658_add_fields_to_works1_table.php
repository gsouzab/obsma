<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToWorks1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('works', function (Blueprint $table) {
          $table->integer('modality_id')->unsigned()->index();
          $table->integer('work_status_id')->unsigned()->index();
          $table->integer('number_of_pages');
          $table->text('theme');
          $table->integer('duration');
          $table->boolean('uses_images');
          $table->text('techniques');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('works', function (Blueprint $table) {
          $table->dropColumn('modality_id');
          $table->dropColumn('work_status_id');
          $table->dropColumn('number_of_pages');
          $table->dropColumn('theme');
          $table->dropColumn('duration');
          $table->dropColumn('uses_images');
          $table->dropColumn('techniques');
        });
    }
}
