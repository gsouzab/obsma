<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_teachers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('academic_qualification_id')->index();
            $table->string('graduation_course');
            $table->date('birthdate');
            $table->string('phone_number');
            $table->string('commercial_phone_number');
            $table->string('mobile_phone_number');
            $table->string('address');
            $table->string('complement');
            $table->string('zip_code');
            $table->string('neighborhood');
            $table->string('gender');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_teachers');
    }
}
