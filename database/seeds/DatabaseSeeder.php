<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //perfis de acesso
        DB::table('roles')->insert([
            'id' => 1,
            'name' => 'Professor',
            'created_at' => 'now()',
            'updated_at' => 'now()'
        ]);

        DB::table('roles')->insert([
            'id' => 2,
            'name' => 'Bolsista',
            'created_at' => 'now()',
            'updated_at' => 'now()'
        ]);

        DB::table('roles')->insert([
            'id' => 3,
            'name' => 'Coordenador',
            'created_at' => 'now()',
            'updated_at' => 'now()'
        ]);

        DB::table('roles')->insert([
            'id' => 4,
            'name' => 'Coordenador Nacional',
            'created_at' => 'now()',
            'updated_at' => 'now()'
        ]);

        //especializoes
        DB::table('academic_qualifications')->insert([
            'id' => 1,
            'name' => 'Especialização',
            'created_at' => 'now()',
            'updated_at' => 'now()'
        ]);

        DB::table('academic_qualifications')->insert([
            'id' => 2,
            'name' => 'Pós-Graduação',
            'created_at' => 'now()',
            'updated_at' => 'now()'
        ]);

        DB::table('academic_qualifications')->insert([
            'id' => 3,
            'name' => 'Mestrado',
            'created_at' => 'now()',
            'updated_at' => 'now()'
        ]);

        DB::table('academic_qualifications')->insert([
            'id' => 4,
            'name' => 'Doutorado',
            'created_at' => 'now()',
            'updated_at' => 'now()'
        ]);

        DB::table('academic_qualifications')->insert([
            'id' => 5,
            'name' => 'Graduação',
            'created_at' => 'now()',
            'updated_at' => 'now()'
        ]);

        //categorias
        DB::table('categories')->insert([
            'id' => 1,
            'name' => 'Ensino Fundamental',
            'created_at' => 'now()',
            'updated_at' => 'now()'
        ]);

        DB::table('academic_qualifications')->insert([
            'id' => 2,
            'name' => 'Ensino Médio',
            'created_at' => 'now()',
            'updated_at' => 'now()'
        ]);
    }
}
