var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss').coffee(['app.coffee', 'register.coffee'])
    .scripts(['jquery.min.js','jquery.mask.min.js','bootstrap.min.js', 'summernote.min.js','summernote-pt-BR.js','jquery_ujs.js', 'select2.min.js', 'select2-pt-BR.js']);
});
