<?php

namespace OBSMA;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name'];

    public function years()
    {
        return $this->hasMany(CategoryYear::class);
    }

    public function getYearsPaginatedAttribute()
    {
        return $this->years()->paginate(15);
    }
}
