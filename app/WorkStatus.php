<?php

namespace OBSMA;

use Illuminate\Database\Eloquent\Model;

class WorkStatus extends Model
{
    const DELIVERED = 1;
    const NOT_DELIVERED = 2;
    const PENDING = 3;
}
