<?php

namespace OBSMA;

use Illuminate\Database\Eloquent\Model;

class AcademicQualification extends Model
{
     protected $fillable = ['name'];

}
