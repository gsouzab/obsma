<?php

namespace OBSMA;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Edition extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'editions';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'year', 'start_date', 'end_date'];

    protected $dates = [ 'start_date', 'end_date', 'year'];


    public function setYearAttribute($date) {
        $this->attributes['year'] = Carbon::createFromFormat('Y', $date);
    }

    public function setStartDateAttribute($date) {
        $this->attributes['start_date'] = Carbon::createFromFormat('d/m/Y', $date);
    }

    public function setEndDateAttribute($date) {
        $this->attributes['end_date'] = Carbon::createFromFormat('d/m/Y', $date);
    }

    public function scopeActive($query) {
        return $query->where('is_active', 1)->first();
    }

}
