<?php

namespace OBSMA;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
     protected $fillable = ['name'];

     const TEACHER = 1;
     const FELLOW = 2; // BOLSISTA
     const COORDINATOR = 3;
     const NATIONAL_COORDINATOR = 4;

     public function users()
     {
        return $this->hasMany(User::class);
     }

}
