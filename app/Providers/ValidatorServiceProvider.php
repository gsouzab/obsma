<?php

namespace OBSMA\Providers;

use Illuminate\Support\ServiceProvider;

class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
      $this->app['validator']->extend('cpf', function($attribute, $value, $parameters){

        $cpf = preg_replace('/\D/', '', $value);

        if (preg_match('/^(.)\1*$/', $cpf))
          return false;

      	// Valida tamanho
      	if (strlen($cpf) != 11)
      		return false;
      	// Calcula e confere primeiro dígito verificador
      	for ($i = 0, $j = 10, $soma = 0; $i < 9; $i++, $j--)
      		$soma += $cpf{$i} * $j;

        $resto = $soma % 11;
      	if ($cpf{9} != ($resto < 2 ? 0 : 11 - $resto))
      		return false;

        // Calcula e confere segundo dígito verificador
      	for ($i = 0, $j = 11, $soma = 0; $i < 10; $i++, $j--)
      		$soma += $cpf{$i} * $j;
      	$resto = $soma % 11;

        return $cpf{10} == ($resto < 2 ? 0 : 11 - $resto);
      });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
