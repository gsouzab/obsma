<?php

namespace OBSMA;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
     protected $fillable = ['name'];

     public function cities()
     {
        return $this->hasMany(City::class);
     }

     public function regional()
     {
        return $this->belongsTo(Regional::class);
     }

     public function getCitiesPaginatedAttribute()
     {
        return $this->cities()->paginate(10);
     }

     public function getCitiesOrderedAttribute()
     {
        return $this->cities()->orderBy('name', 'asc');
     }
}
