<?php

namespace OBSMA;

use Illuminate\Database\Eloquent\Model;

class CategoryYear extends Model
{
    protected $fillable = ['name', 'category_id'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
