<?php

namespace OBSMA;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class StudentWork extends Model
{
    protected $dates = ['birthdate'];

    private $genderLabel = array(
        1 => 'Masculino',
        2 => 'Feminino'
    );

    public function setBirthdateAttribute($date) {
        $this->attributes['birthdate'] = Carbon::createFromFormat('d/m/Y', $date);
    }

    public function getGenderLabelAttribute() {
        return $this->genderLabel[$this->gender_id];
    }
}
