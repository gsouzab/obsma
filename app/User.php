<?php

namespace OBSMA;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'cpf', 'email', 'password', 'city_id', 'role_id','is_blocked'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $dates = ['created_at', 'updated_at'];

    public function userable()
    {
        return $this->morphTo();
    }

    public function state()
    {
        return $this->city()->state();
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function scopeOfRole($query, $role_id)
    {
        if($role_id)
            return $query->where('role_id', $role_id);
        return $query;
    }

    public function getRegionalAttribute()
    {
        $state = State::find($this->city->state_id);
        return $state->regional->name;
    }

    public function scopeOfRegional($query, $regional_id)
    {
        if($regional_id) {
            $state_ids = State::where('regional_id', $regional_id)->lists('id');
            return $query->join('cities','cities.id', '=', 'users.city_id')->whereIn('cities.state_id',  $state_ids);
        }
        return $query;
    }
}
