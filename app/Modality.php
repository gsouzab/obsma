<?php

namespace OBSMA;

use Illuminate\Database\Eloquent\Model;

class Modality extends Model
{
    const TEXT_PRODUCTION = 1;
    const AUDIOVISUAL_PRODUCTION = 2;
    const SCIENCE_PROJECT = 3;

}
