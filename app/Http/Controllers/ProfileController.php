<?php

namespace OBSMA\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use OBSMA\Http\Requests;

use Illuminate\Support\Facades\Validator;
use OBSMA\State;
use OBSMA\AcademicQualification;
use OBSMA\Subject;
use OBSMA\Role;
use OBSMA\User;

class ProfileController extends Controller
{

    public function getProfile(Request $request)
    {
      if ($user = $request->user()) {
        $states = State::lists('name', 'id');
        $academicQualifications = AcademicQualification::lists('name', 'id');
        $subjects = Subject::lists('name', 'id');

        return view('profile.edit', compact('user', 'states', 'academicQualifications', 'subjects'));
      }
    }

    /**
     * Update the user's profile.
     *
     * @param  Request  $request
     * @return Response
     */
    public function updateProfile(Request $request)
    {
        if ($user = $request->user()) {

          $validationArray = [
            'name' => 'required|max:255',
            'cpf' => 'required|cpf|unique:users,cpf,'.$user->id,
            'email' => 'required|email|max:255|unique:users,email,'.$user->id,
            'city_id' => 'required'
          ];

          $isTeacher = $user->role_id == Role::TEACHER;

          if ($isTeacher) {

            $validationTeacher = [
              'userable.phone_number' => 'required',
              'userable.gender' => 'required',
              'userable.other_gender' => 'required_if:userable.gender,3',
              'userable.address' => 'required',
              'userable.zip_code' => 'required',
              'userable.birthdate' => 'required',
              'userable.graduation_type' => 'required',
              'userable.race' => 'required',
              'userable.subjects' => 'required'
            ];

            $validationArray = array_collapse([$validationArray,$validationTeacher]);
          }

          $this->validate($request, $validationArray);

          //common user data
          $user->name = $request->name;
          $user->email = $request->email;
          $user->cpf = $request->cpf;
          $user->city_id = $request->city_id;

          $user->save();

          if ($isTeacher) {
            //teacher data
            $teacher = $user->userable;
            $teacherData = $request->userable;
            $teacher->academic_qualification_id = $teacherData['academic_qualification_id'];
            $teacher->birthdate = $teacherData['birthdate'];
            $teacher->graduation_type = $teacherData['graduation_type'];
            $teacher->graduation_course = $teacherData['graduation_course'];
            $teacher->phone_number = $teacherData['phone_number'];
            $teacher->mobile_phone_number = $teacherData['mobile_phone_number'];
            $teacher->address = $teacherData['address'];
            $teacher->complement = $teacherData['complement'];
            $teacher->zip_code = $teacherData['zip_code'];
            $teacher->neighborhood = $teacherData['neighborhood'];
            $teacher->gender = $teacherData['gender'];
            $teacher->other_gender = $teacherData['other_gender'];
            $teacher->race = $teacherData['race'];

            $teacher->subjects()->sync($teacherData['subjects']);

            $teacher->save();

          }

          Mail::send('emails.profile_alteration', ['user' => $user], function ($message) use ($user) {

            $message->to($user->email, $user->name)
                ->subject('Perfil atualizado - Sistema OBSMA');

            $nationalCoordinators = User::ofRole(Role::NATIONAL_COORDINATOR)->get();
            foreach($nationalCoordinators as $coordinator) {
                $message->bcc($coordinator->email, $coordinator->name);
            }

          });

          return redirect('home')->with('success', 'Perfil atualizado com sucesso!');
        }
    }

    public function getPassword(Request $request)
    {
      if ($user = $request->user()) {
          return view('profile.edit_password', compact('user'));
      }
    }

    public function updatePassword(Request $request)
    {

        $validator = Validator::make($request->all(), [
          'current_password' => 'required',
          'password' => 'required|confirmed|min:6'
        ]);

        if ($validator->fails()) {
            return redirect('profile/password')
                ->withErrors($validator)
                ->withInput();
        }

        if ($user = $request->user()) {
            if(Hash::check($request->current_password, $request->user()->password)) {
                $request->user()->password = bcrypt($request->password);
                if ($request->user()->save() ) {

                    Mail::send('emails.password_alteration', ['user' => $user], function ($message) use ($user) {

                        $message->to($user->email, $user->name)
                            ->subject('Senha atualizada - Sistema OBSMA');

                        $nationalCoordinators = User::ofRole(Role::NATIONAL_COORDINATOR)->get();
                        foreach($nationalCoordinators as $coordinator) {
                            $message->bcc($coordinator->email, $coordinator->name);
                        }

                    });


                    return redirect('home')->with('success', 'Senha atualizada com sucesso!');
                }
            } else {
                $validator->getMessageBag()->add('current_password', 'Senha atual incorreta.');

                return redirect('profile/password')
                    ->withErrors($validator)
                    ->withInput();
            }

        }


    }
}
