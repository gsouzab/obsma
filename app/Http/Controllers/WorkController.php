<?php

namespace OBSMA\Http\Controllers;

use Carbon\Carbon;

use Illuminate\Http\Request;
use OBSMA\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use OBSMA\User;
use PDF;
use OBSMA\Category;
use OBSMA\Edition;
use OBSMA\Modality;
use OBSMA\ParticipationType;
use OBSMA\Role;
use OBSMA\School;
use OBSMA\State;
use OBSMA\StudentWork;
use OBSMA\Teacher;
use OBSMA\Work;
use OBSMA\WorkStatus;

class WorkController extends Controller
{

    protected $lastStep = 4;

    public function show(Request $request, $id)
    {
        $work = Work::findOrFail($id);

        $closingDate = "2018-08-16 15:00:00";
        $isClosed = (time() > strtotime($closingDate));

        $statuses = WorkStatus::lists('name', 'id');
        return view('work.show', compact('work', 'statuses', 'isClosed'));
    }

    public function index()
    {
        $closingDate = "2018-08-16 15:00:00";
        $isClosed = (time() > strtotime($closingDate));

        if (Auth::user()->role_id == Role::TEACHER)
        {
            $works = Work::where('teacher_id', '=', Auth::user()->userable->id)->paginate(15);
        } else {
            $works = Work::paginate(15);
        }

        return view('work.index', compact('works', 'isClosed'));
    }

    public function edit(Request $request, $id)
    {
        $work = Work::findOrFail($id);

        if(!$work->work_status_id) {
            //trabalho ainda nao finalizado
            $step = 1;
            $request->session()->put('work', $work);
            $request->session()->put('step', $step);

            return redirect()->action('WorkController@createStep', ['step' => $step]);
        }

        $work->adm_dept = $work->school === null ? '' : $work->school->administrative_department;
        $work->type = $work->school === null ? '' : $work->school->type;
        $work->genre = explode(',', $work->genre);
        $work->techniques = explode(',', $work->techniques);

        $teachers = Teacher::join('users', 'users.userable_id', '=', 'users_teachers.id')->lists('name','userable_id');
        $states = State::lists('name', 'id');
        $cities = $work->state === null ? [] : $work->state->citiesOrdered->lists('name', 'id');
        $schools = $work->state === null ? [] : School::where('state_id', $work->state_id)->where('city_id', $work->city_id)->lists('name', 'id');
        $categories = Category::lists('name', 'id');
        $modalities = Modality::lists('name', 'id');
        $participation_types = ParticipationType::lists('name', 'id');
        $years = $work->category === null ? [] : $work->category->years->lists('name', 'id');

        return view('work.edit', compact('work', 'states', 'cities', 'categories', 'years', 'schools', 'modalities', 'participation_types', 'teachers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $work = Work::findOrFail($id);

        $rules = [
            'school_id' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',
            'teacher_id' => 'required',
            'category_id' => 'required',
            'years' => 'required',
            'teachers_total' => 'required|integer|min:1',
            'modality_id' => 'required',
            'participation_type_id' => 'required',
            'title' => "required|max:2000|unique:works,title,{$work->id},id,teacher_id,".$work->teacher_id,
            'theme' => 'required|max:2000',
            'keywords' => 'required|max:200',
            'area' => 'required',
            'other_area' => 'required_if:area,3'
        ];

        if($request->modality_id == Modality::SCIENCE_PROJECT)
        {
            $rules = array_merge($rules, [
                'abstract' => 'required|max:2000',
                'objective' => 'required|max:2000',
                'theoretical_references' => 'required|max:2000',
                'methodology' => 'required|max:2000',
                'results' => 'required|max:2000',
                'sent_material' => 'required|max:2000',
                'students' => 'required|min:2',
                'students_total' => 'required|integer|min:2',
            ]);
        } elseif ($request->modality_id == Modality::AUDIOVISUAL_PRODUCTION) {
            $rules = array_merge($rules, [
                'abstract' => 'required|max:2000',
                'students' => 'required|min:2',
                'students_total' => 'required|integer|min:2',
                'duration' => 'required|integer|min:1'
            ]);
        } elseif ($request->modality_id == Modality::TEXT_PRODUCTION) {
            $rules = array_merge($rules, [
                'methodology' => 'required|max:2000',
                'genre' => 'required',
                'students' => 'required',
                'students_total' => 'required|integer|min:1',
                'number_of_pages' => 'required|integer|min:1'
            ]);
        }

        if($request->uses_images)
        {
            $rules = array_merge($rules, [
                'techniques' => 'required',
            ]);
        }

        $this->validate($request, $rules);

        $activeEdition = Edition::active();
        
        $work->edition_id = $activeEdition->id;
        $work->teacher_id = $request->teacher_id;
        $work->school_id = $request->school_id;
        $work->city_id = $request->city_id;
        $work->state_id = $request->state_id;
        $work->regional_id = $request->state_id == null ? null : State::find($request->state_id)->regional_id;

        $work->category_id = $request->category_id;
        $work->teachers_total = $request->teachers_total;
        $work->students_total = $request->students_total;
        $students = [];

        if($request->students)
        {
            foreach ($request->students as $student) {
                $studentWork = new StudentWork();
                $studentWork->name = $student['name'];
                $studentWork->gender_id = $student['gender'];
                $studentWork->birthdate = $student['birthdate'];

                $students[] = $studentWork;
            }

            $work->students()->delete();
            $work->students()->saveMany($students);
        }

        $request->years !== null ? $work->categoryYears()->sync($request->years) : null;

        $work->modality_id = $request->modality_id;
        $work->participation_type_id = $request->participation_type_id;
        $work->area = $request->area;
        $work->other_area = $request->other_area;
        $work->keywords = $request->keywords;
        $work->title = $request->title;
        $work->theme = $request->theme;
        $work->abstract = $request->abstract;
        $work->duration = $request->duration;
        $work->objective = $request->objective;
        $work->theoretical_references = $request->theoretical_references;
        $work->methodology = $request->methodology;
        $work->results = $request->results;
        $work->sent_material = $request->sent_material;

        $work->number_of_pages = $request->number_of_pages;
        $work->genre = $request->genre != null ? implode(',', $request->genre) : '';
        $work->genre .= $request->other_genre != null ? ',outros:'. $request->other_genre : '';

        $work->uses_images = $request->uses_images;
        $work->techniques = $request->techniques != null ? implode(',', $request->techniques) : '';
        $work->techniques .= $request->other_techniques != null ? ',outros:'. $request->other_techniques : '';

        $work->save();

        $user = $work->teacher->user;

        Mail::send('emails.work_alteration', ['work' => $work, 'activeEdition' => $work->edition], function ($message) use ($user) {

            $message->to($user->email, $user->name)
                ->subject('Trabalho alterado - Sistema OBSMA');

            $nationalCoordinators = User::ofRole(Role::NATIONAL_COORDINATOR)->get();
            foreach($nationalCoordinators as $coordinator) {
                $message->bcc($coordinator->email, $coordinator->name);
            }
        });

        return redirect('work')->with('success', 'Trabalho atualizado com sucesso!');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $step = 1;
        $request->session()->put('work', new Work());
        $request->session()->put('step', $step);

        return redirect()->action('WorkController@createStep', ['step' => $step]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createStep(Request $request, $step)
    {
        $vars['step'] = $step;

        $request->session()->put('step', $step);

        $vars['cities'] = [];
        $vars['schools'] = [];
        $vars['years'] = [];

        if ($request->session()->get('work') === null || !$request->session()->get('work')->exists) {
            $vars['work'] = new Work();
            $request->session()->put('work', $vars['work']);
        } else {
            $vars['work'] = $request->session()->get('work');
            $vars['cities'] = $request->session()->get('work')->state_id != null ? State::find($request->session()->get('work')->state_id)->citiesOrdered->lists('name', 'id') : [];
            $vars['schools'] = $request->session()->get('work')->city_id != null ? School::where('state_id', $request->session()->get('work')->state_id)->where('city_id', $request->session()->get('work')->city_id)->lists('name', 'id') : [];
            $vars['years'] = $request->session()->get('work')->category_id != null ? Category::find($request->session()->get('work')->category_id)->years->lists('name', 'id') : [];
        }

        switch ($step) {
            case 1:
                $vars['states'] = State::lists('name', 'id');
                $vars['teachers'] = Teacher::join('users', 'users.userable_id', '=', 'users_teachers.id')->lists('name','userable_id');
                break;
            case 2:
                $vars['categories'] = Category::lists('name', 'id');
                $vars['modalities'] = Modality::lists('name', 'id');
                break;
            case 3:
                $vars['participation_types'] = ParticipationType::lists('name', 'id');
                break;
        }

        return view('work.create_step_'.$step, $vars);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeStep(Request $request, $step)
    {

        $activeEdition = Edition::active();

        $work = $request->session()->get('work');
        switch ($step) {
            case 1:
                $rules = [
                    'school_id' => 'required', 
                    'state_id' => 'required', 
                    'city_id' => 'required', 
                    'teacher_id' => 'required', 
                    'area' => 'required',
                    'other_area' => 'required_if:area,3'
                ];
                $work->edition_id = $activeEdition->id;
                $work->area = $request->area;
                $work->other_area = $request->other_area;
                $work->teacher_id = $request->teacher_id;
                $work->school_id = $request->school_id;
                $work->city_id = $request->city_id;
                $work->state_id = $request->state_id;
                $work->regional_id = $request->state_id == null ? null : State::find($request->state_id)->regional_id;
                break;
            case 2:
                $rules = ['modality_id' => 'required','category_id' => 'required', 'years' => 'required', 'teachers_total' => 'required|integer|min:1'];
                $work->category_id = $request->category_id;
                $work->modality_id = $request->modality_id;
                $work->teachers_total = $request->teachers_total;
                $work->students_total = $request->students_total;
                $students = [];

                if ($request->modality_id == Modality::TEXT_PRODUCTION)
                {
                    $rules['students'] = 'required';
                    $rules['students_total'] = 'required|integer|min:1';
                } else {
                    $rules['students'] = 'required|min:2';
                    $rules['students_total'] = 'required|integer|min:2';
                }

                if($request->students)
                {
                    foreach ($request->students as $student) {
                        $studentWork = new StudentWork();
                        $studentWork->name = $student['name'];
                        $studentWork->gender_id = $student['gender'];
                        $studentWork->birthdate = $student['birthdate'];

                        $students[] = $studentWork;
                    }

                    $work->students()->delete();
                    $work->students()->saveMany($students);
                }


                $request->years != null ? $work->categoryYears()->sync($request->years) : null;
                break;
            case 3:

                $rules = [
                    'participation_type_id' => 'required',
                    'title' => "required|max:2000|unique:works,title,{$work->id},id,teacher_id,".$work->teacher_id,
                    'theme' => 'required|max:2000',  
                    'keywords' => 'required|max:200',                    
                ];

                if($work->modality_id == Modality::SCIENCE_PROJECT)
                {
                    $rules = array_merge($rules, [
                        'abstract' => 'required|max:2000',
                        'objective' => 'required|max:2000',
                        'theoretical_references' => 'required|max:2000',
                        'methodology' => 'required|max:2000',
                        'results' => 'required|max:2000',
                        'sent_material' => 'required|max:2000']);
                } elseif ($work->modality_id == Modality::AUDIOVISUAL_PRODUCTION) {
                    $rules = array_merge($rules, [
                        'abstract' => 'required|max:2000',
                        'methodology' => 'required|max:2000',
                        'duration' => 'required|integer|min:1'
                    ]);
                } elseif ($work->modality_id == Modality::TEXT_PRODUCTION) {
                    $rules = array_merge($rules, [
                        'methodology' => 'required|max:2000',
                        'genre' => 'required',
                        'number_of_pages' => 'required|integer|min:1'
                    ]);

                    if($request->uses_images)
                    {
                        $rules = array_merge($rules, [
                            'techniques' => 'required',
                        ]);
                    }
                }

                $work->participation_type_id = $request->participation_type_id;
                $work->title = $request->title;
                $work->theme = $request->theme;
                $work->abstract = $request->abstract;
                $work->duration = $request->duration;
                $work->objective = $request->objective;
                $work->theoretical_references = $request->theoretical_references;
                $work->methodology = $request->methodology;
                $work->results = $request->results;
                $work->sent_material = $request->sent_material;

                $work->keywords = $request->keywords;

                $work->number_of_pages = $request->number_of_pages;
                $work->genre = $request->genre != null ? implode(',', $request->genre) : '';
                $work->genre .= $request->other_genre != null ? ',outros:'. $request->other_genre : '';
                $work->uses_images = $request->uses_images;
                $work->techniques = $request->techniques != null ? implode(',', $request->techniques) : '';
                $work->techniques .= $request->other_techniques != null ? ',outros:'. $request->other_techniques : '';
                break;

            case 4:
                $rules = [
                    'agreed' => 'accepted',        
                    'has_fiocruz_material' => 'required',         
                    'fiocruz_fonts' => 'required_if:has_fiocruz_material,1',         
                ];

                $work->has_fiocruz_material = $request->has_fiocruz_material;
                $work->fiocruz_fonts = $request->fiocruz_fonts;
                
                break;
        }

        $this->validate($request, $rules);

        $work->save();
        $request->session()->put('work',$work);
        $request->session()->put('work_id',$work->id);

        if ($step == $this->lastStep) {
            return redirect()->action('WorkController@review');
        }

        return redirect()->action('WorkController@createStep', ['step' => $step+1])->with('success', "Dados salvos com sucesso!");

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function review(Request $request)
    {

        $work = Work::find($request->session()->get('work_id'));

        if($work && $work->exists)
        {
            return view('work.review', compact('work'));
        } else {
            return redirect('work/create');
        }
    }


    public function discard(Request $request)
    {
        $work = $request->session()->get('work');

        if($work->exists)
        {
            $work->delete();
        }

        $request->session()->set('work', null);
        $request->session()->set('step', null);

        return redirect('work')->with('success', "Trabalho descartado com sucesso!");
    }

    public function confirm(Request $request)
    {
        $activeEdition = Edition::active();

        $work = Work::find($request->session()->get('work_id'));

        if ($work && $work->exists ) {
            $work->work_status_id = WorkStatus::NOT_DELIVERED;
            $work->save();

            $user = $work->teacher->user;

            Mail::send('emails.work', ['work' => $work, 'activeEdition' => $work->edition], function ($message) use ($user) {

                $message->to($user->email, $user->name)
                    ->subject('Trabalho incluído - Sistema OBSMA');

                $nationalCoordinators = User::ofRole(Role::NATIONAL_COORDINATOR)->get();
                foreach($nationalCoordinators as $coordinator) {
                    $message->bcc($coordinator->email, $coordinator->name);
                }
            });

            $request->session()->set('work_id',null);
            $request->session()->set('step',null);
            return redirect('work')->with('success', "Inscrição de trabalho realizado com sucesso! Um email de confirmação foi enviado para {$request->user()->email}. Atenção: o material físico relativo ao seu trabalho deve ser encaminhado à sua Regional até o dia {$activeEdition->end_date->format('d/m/Y')} (o selo dos Correios vale como comprovante). BOA SORTE!");
        } else {
            return redirect('work/create');

        }
    }

    public function setIsWinner(Request $request, $id)
    {
        $work = Work::findOrFail($id);

        $work->is_winner = true;

        $work->save();
        return Redirect::route('report.works',$request->session()->get('report_inputs'))->with('success', 'Trabalho marcado como destaque com sucesso!');
    }

    public function validateWork(Request $request, $id)
    {
        $work = Work::findOrFail($id);

        $lastNumber = Work::orderBy('inscription_number',  'DESC')->select('inscription_number')->first()->inscription_number;
        $insNumber = $lastNumber !== null ? $lastNumber+1 : 1;

        $work->is_valid = true;
        $work->inscription_number = $insNumber;
        $work->validation_date = Carbon::now();

        $work->save();

        return Redirect::route('report.works',$request->session()->get('report_inputs'))->with('success', 'Trabalho validado com sucesso!');
    }

    public function updateStatus(Request $request, $id)
    {
        $work = Work::findOrFail($id);
        $work->work_status_id = $request->work_status_id;
        $work->save();

        if($request->user()->role_id == Role::TEACHER)
        {
            return redirect('work')->with('success', "Trabalho atualizado com sucesso!");
        } else {
            return Redirect::route('report.works',$request->session()->get('report_inputs'))->with('success', 'Trabalho atualizado com sucesso!');
        }

    }

    public function printWork(Request $request, $id)
    {
        $data['work'] = Work::findOrFail($id);
        $pdf = PDF::loadView('work.print', $data);
        return $pdf->stream('trabalho.pdf');
    }
}
