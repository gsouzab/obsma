<?php

namespace OBSMA\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use OBSMA\Edition;
use OBSMA\Http\Requests;
use OBSMA\Role;
use OBSMA\State;
use OBSMA\User;
use OBSMA\Work;
use DB;

class ReportController extends Controller
{
    public function teachers(Request $request) {
        $teachersQuery = $this->generateTeachersQuery($request);

        $teachers_count = $teachersQuery->count();

        $request->session()->put('teachers_report_inputs', Input::except('_excel'));

        if($request->_excel) {           
            $data = $teachersQuery
                ->join('users_teachers', 'users.userable_id', '=', 'users_teachers.id')
                ->join('cities', 'cities.id', '=', 'users.city_id')
                ->join('states', 'states.id', '=', 'cities.state_id')
                ->join('regionals', 'regionals.id', '=', 'states.regional_id')
                ->join('academic_qualifications', 'academic_qualifications.id', '=', 'users_teachers.academic_qualification_id')
                ->select(
                    "users.name AS Nome",
                    'users.cpf AS CPF',
                    'users.email AS Email',
                    DB::raw('case users_teachers.gender
                        when \'1\' then \'Masculino\'
                        when \'2\' then \'Feminino\'
                        when \'3\' then users_teachers.other_gender
                    end as \'Gênero\''),            
                    DB::raw('case users_teachers.race
                                when \'1\' then \'Brancos\'
                                when \'2\' then \'Pretos\'
                                when \'3\' then \'Amarelos\'
                                when \'4\' then \'Amarelos\'
                                when \'5\' then \'Indígenas\'
                            end as \'Cor/Raça\''),            
                    'regionals.name AS Regional',
                    'states.name AS Estado',
                    'cities.name AS Município',
                    'academic_qualifications.name AS Titulação',
                    'users_teachers.graduation_course AS Curso de Formação',
                    DB::raw('case users_teachers.graduation_type
                                when \'1\' then \'Superior Completo\'
                                when \'2\' then \'Superior Incompleto\'
                            end as \'Formação\'')

                )
                ->get()->toArray();

            Excel::create('relatorio_professores', function($excel) use($data) {

                $excel->sheet('Professores', function($sheet) use($data) {

                    $sheet->fromArray($data);

                });

            })->export('xlsx');
        }

        $teachers = $teachersQuery->orderBy('users.name', 'ASC')
            ->select('users.id', 'users.name', 'users.email', 'users.userable_id', 'users.city_id', 'users.userable_type')
            ->paginate(15);

        $request->flash();
        return view('report.teachers', compact('teachers', 'teachers_count'));
    }

    public function works(Request $request) {
        $workQuery = $this->generateWorksQuery($request);

        $works_count = $workQuery->count();

        $request->session()->put('report_inputs', Input::except('_excel'));

        $teachers = User::where('role_id', Role::TEACHER)->lists('name', 'userable_id');

        $teachers_total = $workQuery->sum('teachers_total');
        $students_total = $workQuery->sum('students_total');

        if($request->_excel) {
            $data = $workQuery
                ->join('users', 'users.userable_id', '=', 'works.teacher_id')
                ->join('schools', 'schools.id', '=', 'works.school_id')
                ->join('states', 'states.id', '=', 'works.state_id')
                ->join('cities', 'cities.id', '=', 'works.city_id')
                ->join('regionals', 'regionals.id', '=', 'works.regional_id')
                ->join('work_statuses', 'work_statuses.id', '=', 'works.work_status_id')
                ->join('participation_types', 'participation_types.id', '=', 'works.participation_type_id')
                ->join('modalities', 'modalities.id', '=', 'works.modality_id')
                ->select(
                    "inscription_number AS Nr_Inscrição",
                    'title AS Título',
                    'modalities.name AS Modalidade',
                    'participation_types.name AS Participação',
                    'keywords AS Palavras-chave',
                    'users.name AS Professor',
                    'regionals.name AS Regional',
                    'states.name AS Estado',
                    'cities.name AS Município',
                    'schools.name AS Escola',
                    'schools.administrative_department AS Dep_Adm_Escola',
                    'schools.type AS Tipo_da_escola',
                    'works.students_total AS Total_Alunos',
                    'works.teachers_total AS Total_Professores',
                    'work_statuses.name AS Situação',
                    'is_valid AS Validado',
                    'is_winner AS Classificado',
                    DB::raw('case area
                        when \'1\' then \'Indígena\'
                        when \'2\' then \'Quilombola\'
                        when \'3\' then other_area
                        when \'4\' then \'Não se aplica\'
                    end as \'Área/Localidade\''),
                    DB::raw('case has_fiocruz_material
                        when \'1\' then \'Sim\'
                        when \'2\' then \'Não\'
                    end as \'Prêmio Ano Oswaldo Cruz 2017\''),
                    'fiocruz_fonts AS Fontes'
                )
                ->get()->toArray();

            Excel::create('relatorio_trabalhos', function($excel) use($data) {

                $excel->sheet('Trabalhos enviados', function($sheet) use($data) {

                    $sheet->fromArray($data);

                });

            })->export('xlsx');
        }

        $works = $workQuery->orderBy('works.updated_at', 'DESC')
            ->orderBy('works.inscription_number', 'DESC')
            ->select('works.title', 'works.id', 'works.is_valid', 'works.inscription_number', 'works.teacher_id', 'works.school_id', 'works.work_status_id')
            ->paginate(15);

        $request->flash();
        return view('report.works', compact('students_total', 'teachers_total', 'works_count', 'teachers', 'works'));
    }

    private function generateTeachersQuery(Request $request)
    {
        $teacherQuery = User::query();
        $teacherQuery->where('users.role_id', Role::TEACHER);
        $teacherQuery->where('users.userable_id', "<>", 'null');

        if ($request->s_regional_id != "") {
            $citiesIDs = [];

            $states = State::where('regional_id',$request->s_regional_id)->get();
            foreach($states as $stateOfRegional) {

                $citiesIDs = array_merge($citiesIDs, $stateOfRegional->cities->lists('id')->toArray());
            }

            $teacherQuery->whereIn('users.city_id', $citiesIDs);
        }

        if ($request->s_state_id != "")
            $teacherQuery->whereIn('users.city_id', State::find($request->s_state_id)->cities->lists('id'));

        if ($request->s_city_id != "")
            $teacherQuery->where('users.city_id', $request->s_city_id);

        if ($request->s_academic_qualification_id != "")
            $teacherQuery->join('users_teachers as ut1', 'users.userable_id', '=','ut1.id')->where('ut1.academic_qualification_id', $request->s_academic_qualification_id);

        if ($request->s_graduation_course != "")
            $teacherQuery->join('users_teachers as ut2', 'users.userable_id', '=','ut2.id')->where('ut2.graduation_course', 'LIKE' , "%{$request->s_graduation_course}%");

        if ($request->s_gender != "")
            $teacherQuery->join('users_teachers as ut3', 'users.userable_id', '=','ut3.id')->where('ut3.gender', 'LIKE', "%{$request->s_gender}%");

        if ($request->s_race != "")
            $teacherQuery->join('users_teachers as ut4', 'users.userable_id', '=','ut4.id')->where('ut4.race', 'LIKE', "%{$request->s_race}%");

        return $teacherQuery;
    }

    private function generateWorksQuery(Request $request)
    {
        // nao mostra as caixas com o numero de professores e alunos
        session(['show_boxes' => false]);

        $workQuery = Work::query();
        $workQuery->where('works.work_status_id', '<>' , "NULL");

        if ($request->s_modality_id != "")
            $workQuery->where('works.modality_id', $request->s_modality_id);

        if ($request->s_category_id != "")
            $workQuery->where('works.category_id', $request->s_category_id);

        if ($request->s_participation_type_id != "")
            $workQuery->where('works.participation_type_id', $request->s_participation_type_id);

        if ($request->s_edition_id != "") {
            $workQuery->where('works.edition_id', $request->s_edition_id);
            $edition = Edition::findOrFail($request->s_edition_id);

            if($edition->year >= Carbon::createFromDate(2015))
                session(['show_boxes' => true]);
        }

        if ($request->s_teacher_id != "")
            $workQuery->where('works.teacher_id', $request->s_teacher_id);

        if ($request->s_regional_id  != "")
            $workQuery->where('works.regional_id', $request->s_regional_id);

        if ($request->s_title != "")
            $workQuery->where('works.title','LIKE', "%{$request->s_title}%");

        if ($request->s_state_id != "")
            $workQuery->where('works.state_id', $request->s_state_id);

        if ($request->s_city_id != "")
            $workQuery->where('works.city_id', $request->s_city_id);

        if ($request->s_school != "")
            $workQuery->join('schools as s1', 's1.id', '=','works.school_id')->where('s1.name', 'LIKE', "%{$request->s_school}%");

        if ($request->s_administrative_department != "")
            $workQuery->join('schools as s2', 's2.id', '=','works.school_id')->where('s2.administrative_department', 'LIKE', "%{$request->s_administrative_department}%");

        if ($request->s_is_valid != "")
            $workQuery->where('works.is_valid', $request->s_is_valid);

        if ($request->s_is_winner != "")
            $workQuery->where('works.is_winner', $request->s_is_winner);

        if ($request->s_work_status_id != "")
            $workQuery->where('works.work_status_id', $request->s_work_status_id);

        if ($request->s_has_fiocruz_material != "")
            $workQuery->where('works.has_fiocruz_material', $request->s_has_fiocruz_material);

        if ($request->s_school_type != "")
            $workQuery->join('schools as s3', 's3.id', '=','works.school_id')->where('s3.type', 'LIKE', "%{$request->s_school_type}%");

        if ($request->s_school_area != "")
        $workQuery->where('works.area', $request->s_school_area);

        return $workQuery;
    }
}
