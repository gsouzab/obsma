<?php

namespace OBSMA\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use OBSMA\Http\Requests;
use OBSMA\Http\Controllers\Controller;
use OBSMA\Jobs\SendInternalEmail;
use OBSMA\Regional;
use OBSMA\Role;
use OBSMA\User;

class EmailController extends Controller
{
    /**
     * Show the email compose page.
     *
     */
     public function compose() {
         $roles = Role::lists('name','id');
         $regionals = Regional::lists('name','id');

         $queueCount = DB::table('jobs')->where('queue', 'internal-emails')->count();

         return view('email.index', compact('roles', 'regionals', 'queueCount'));
     }

     public function upload()
     {
        $file = \Input::file('file');
        $image_name = time()."-".$file->getClientOriginalName();        
        $destinationPath = public_path().DIRECTORY_SEPARATOR.'files';
        $file->move('uploads', $image_name);

        return url('/') . "/uploads/{$image_name}";
     }

     public function sendEmail(Request $request) {
         $this->validate($request, [
            'subject' => 'required',
            'email' => 'required'
         ]);

         $emailsCount = 0;

         User::ofRole($request->role_id)->ofRegional($request->regional_id)->groupBy('users.id')->select('users.name', 'users.email')->chunk(30, function($users) use ($request,&$emailsCount) {
             $this->dispatch((new SendInternalEmail($users, $request->subject, $request->email))->onQueue('internal-emails'));
             $emailsCount++;
         });

         return redirect('email')->with('success', "{$emailsCount} emails adicionados na fila para o envio!");
     }

     public function sendEmailContact(Request $request)
     {
       $this->validate($request, [
         'name' => 'required',
         'email' => 'required|email',
         'subject' => 'required',
         'message' => 'required'
       ]);

       Mail::send('emails.contact', ['request' => $request], function ($message) use ($request) {

         $message->to('olimpiada@fiocruz.br', 'OBSMA')
             ->from($request->email, $request->name)
             ->replyTo($request->email, $request->name)
             ->subject('Contato - Sistema OBSMA');
       });
     }
}
