<?php

namespace OBSMA\Http\Controllers;

use Illuminate\Support\Facades\DB;
use OBSMA\Http\Requests;
use OBSMA\Http\Controllers\Controller;

use OBSMA\Edition;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class EditionController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $editions = Edition::orderBy('start_date', 'desc')->paginate(15);

        return view('edition.index', compact('editions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $edition = new Edition;
        return view('edition.create', compact('edition'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'year' => 'required|numeric',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);

        Edition::create($request->all());

        return redirect('edition')->with('success', 'Edição criada com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $edition = Edition::findOrFail($id);

        return view('edition.show', compact('edition'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $edition = Edition::findOrFail($id);

        return view('edition.edit', compact('edition'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'year' => 'required|numeric',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);

        $edition = Edition::findOrFail($id);
        $edition->update($request->all());

        return redirect('edition')->with('success', 'Edição atualizada com sucesso!');
    }

    public function activate($id) {
        $edition = Edition::findOrFail($id);

        DB::transaction(function () use ($edition) {

            Edition::where('id', '<>', $edition->id)->update(['is_active' => 0]);

            $edition->is_active = 1;
            $edition->save();
        });

        return redirect('edition')->with('success', "Edição {$edition->name} ativada com sucesso!");

    }

}
