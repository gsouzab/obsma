<?php

namespace OBSMA\Http\Controllers;

use Illuminate\Http\Request;

use OBSMA\Http\Requests;
use OBSMA\Http\Controllers\Controller;

use OBSMA\AcademicQualification;

class AcademicQualificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $academic_qualifications = AcademicQualification::orderBy('name')->paginate(15);

        return view('academic_qualification.index', ['academic_qualifications' => $academic_qualifications]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $academic_qualification = new AcademicQualification;

      return view('academic_qualification.create', compact('academic_qualification'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'name' => 'required'
      ]);

      AcademicQualification::create([
        'name' => $request->name
      ]);

      return redirect('academic_qualification')->with('success', 'Titulação criada com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $academic_qualification = AcademicQualification::findOrFail($id);

      return view('academic_qualification.show', compact('academic_qualification'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $academic_qualification = AcademicQualification::findOrFail($id);

        return view('academic_qualification.edit', compact('academic_qualification'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $academic_qualification = AcademicQualification::findOrFail($id);

      $this->validate($request, [
        'name' => 'required'
      ]);

      $academic_qualification->name = $request->name;

      $academic_qualification->save();

      return redirect('academic_qualification')->with('success', 'Titulação atualizada com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
