<?php

namespace OBSMA\Http\Controllers;

use Illuminate\Http\Request;

use OBSMA\Category;
use OBSMA\CategoryYear;
use OBSMA\Http\Requests;
use OBSMA\Http\Controllers\Controller;


class CategoryYearController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoryYears = CategoryYear::orderBy('name')->paginate(15);

        return view('category_year.index', compact('categoryYears'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoryYear = new CategoryYear;
        $categories = Category::lists('name', 'id');

        return view('category_year.create', compact('categoryYear','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'category_id' => 'required',
        ]);

        CategoryYear::create([
            'name' => $request->name,
            'category_id' => $request->category_id,
        ]);

        return redirect('category_year')->with('success', 'Ano criado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categoryYear = CategoryYear::findOrFail($id);

        return view('category_year.show', compact('categoryYear'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoryYear = CategoryYear::findOrFail($id);
        $categories = Category::lists('name', 'id');

        return view('category_year.edit', compact('categoryYear','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categoryYear = CategoryYear::findOrFail($id);

        $this->validate($request, [
            'name' => 'required',
            'category_id' => 'required',
        ]);

        $categoryYear->name = $request->name;
        $categoryYear->category_id = $request->category_id;

        $categoryYear->save();

        return redirect('category_year')->with('success', 'Ano atualizado com sucesso!');
    }

}
