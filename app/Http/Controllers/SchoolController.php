<?php

namespace OBSMA\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use OBSMA\Http\Requests;

use OBSMA\School;
use OBSMA\State;

class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $schoolQuery = School::query();

        $request->session()->put('school_inputs', Input::except('_excel'));
        if ($request->s_name != "")
          $schoolQuery->where('schools.name', 'LIKE', "%{$request->s_name}%");

        if ($request->s_code != "")
          $schoolQuery->where('schools.code', 'LIKE', "%{$request->s_code}%");

        $request->flash();
        $schools = $schoolQuery->paginate(15);

        return view('school.index', compact('schools'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $school = new School;
      $states = State::lists('name', 'id');

      return view('school.create', compact('school','states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'code' => 'integer',
        'name' => 'required',
        'email' => 'email',
        'type' => 'required',
        'administrative_department' => 'required',
        'address' => 'required',
        'neighborhood' => 'required',
        'state_id' => 'integer|required',
        'city_id' => 'integer|required',
      ]);

      School::create(array_add($request->all(), 'year', Carbon::today()));

      return redirect('/school')->with('success', 'Escola cadastrada com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $school = School::findOrFail($id);

      return view('school.show', compact('school'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $school = School::findOrFail($id);
      $states = State::lists('name', 'id');

      return view('school.edit', compact('school','states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $school = School::findOrFail($id);

      $this->validate($request, [
        'name' => 'required',
        'acronym' => 'required',
        'regional_id' => 'integer'
      ]);

      $school->acronym = $request->acronym;
      $school->name = $request->name;
      $school->regional_id = $request->regional_id;

      $school->save();
      return redirect('/school')->with('success', 'Estado atualizado com sucesso!');
    }

    public function query($state_id, $city_id, $query = null)
    {
        $schoolQuery = School::where(['state_id' => $state_id, 'city_id' => $city_id]);

        if ($query !== null)
            $schoolQuery->where('name', 'like', "%{$query}%");

        $schools = $schoolQuery->select('id', 'name')->get();
        return response()->json( ['schools' => $schools->toArray()] );
    }

    public function getAdministrativeDepartment($school_id)
    {
        $school = School::findOrFail($school_id);
        return response()->json( ['school' => $school] );

    }

}
