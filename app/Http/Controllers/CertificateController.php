<?php

namespace OBSMA\Http\Controllers;

use Illuminate\Http\Request;

use PDF;
use OBSMA\Http\Requests;
use OBSMA\Http\Controllers\Controller;
use OBSMA\Role;
use OBSMA\Work;

class CertificateController extends Controller
{
    public function index(Request $request) {

        if($request->user()->role_id == Role::TEACHER)
        {
            $works = Work::where('is_valid',1)->where('teacher_id',$request->user()->userable_id)->paginate(15);

            return view('certificate.index', compact('works'));
        }

        return redirect('home');

    }

    public function teacherCertificate(Request $request, $workId)
    {
        $data['work'] = Work::findOrFail($workId);
        if($data['work']->is_valid) {
            $pdf = PDF::loadView('certificate.teacher', $data);
            return $pdf->setOrientation('landscape')->stream('certificado.pdf');
        } else {
            return redirect('home')->with('danger', 'Você não tem permissao para acessar esse recurso.');
        }

    }

    public function studentCertificate(Request $request, $workId)
    {
        $data['work'] = Work::findOrFail($workId);

        if($data['work']->is_valid) {
            $pdf = PDF::loadView('certificate.student', $data);
            return $pdf->setOrientation('landscape')->stream('certificado.pdf');
        } else {
            return redirect('home')->with('danger', 'Você não tem permissao para acessar esse recurso.');
        }
    }

    public function winnerTeacherCertificate(Request $request, $workId)
    {
        $data['work'] = Work::findOrFail($workId);
        if($data['work']->is_valid && $data['work']->is_winner) {
            $pdf = PDF::loadView('certificate.winner_teacher', $data);
            return $pdf->setOrientation('landscape')->stream('certificado.pdf');
        } else {
            return redirect('home')->with('danger', 'Você não tem permissao para acessar esse recurso.');
        }
    }

    public function winnerStudentCertificate(Request $request, $workId)
    {
        $data['work'] = Work::findOrFail($workId);
        if($data['work']->is_valid && $data['work']->is_winner) {
            $pdf = PDF::loadView('certificate.winner_student', $data);
            return $pdf->setOrientation('landscape')->stream('certificado.pdf');
        } else {
            return redirect('home')->with('danger', 'Você não tem permissao para acessar esse recurso.');
        }
    }



}
