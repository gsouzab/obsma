<?php

namespace OBSMA\Http\Controllers;

use Illuminate\Http\Request;

use OBSMA\Http\Requests;
use OBSMA\Http\Controllers\Controller;

use OBSMA\State;
use OBSMA\Regional;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $states = State::orderBy('name')->paginate(15);

      return view('state.index', compact('states'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $state = new State;
      $regionals = Regional::lists('name', 'id');

      return view('state.create', compact('state','regionals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'name' => 'required',
        'acronym' => 'required',
        'regional_id' => 'integer',
      ]);

      State::create([
        'name' => $request->name,
        'acronym' => $request->acronym,
        'regional_id' => $request->regional_id
      ]);

      return redirect('/state')->with('success', 'Estado cadastrado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $state = State::findOrFail($id);

      return view('state.show', compact('state'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $state = State::findOrFail($id);
      $regionals = Regional::lists('name', 'id');

      return view('state.edit', compact('state','regionals'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $state = State::findOrFail($id);

      $this->validate($request, [
        'name' => 'required',
        'acronym' => 'required',
        'regional_id' => 'integer'
      ]);

      $state->acronym = $request->acronym;
      $state->name = $request->name;
      $state->regional_id = $request->regional_id;

      $state->save();
      return redirect('/state')->with('success', 'Estado atualizado com sucesso!');
    }

    public function cities($id)
    {
      $state = State::findOrFail($id);
      return response()->json( ['cities' => $state->citiesOrdered->select('id','name')->get()->toArray()]);
    }

}
