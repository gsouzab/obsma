<?php

namespace OBSMA\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Maatwebsite\Excel\Facades\Excel;
use OBSMA\Http\Requests;

use OBSMA\User;
use OBSMA\Role;
use OBSMA\State;
use OBSMA\Regional;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('national_coodinator', ['only' => ['create', 'store']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function searchTeachers(Request $request){

        $query = $request->q;

        $teachers = User::where('role_id', Role::TEACHER)->where('name', 'LIKE', "%{$query}%")->select('userable_id AS id','name AS text')->get()->toArray();

        return response()->json( ['items' => $teachers]);
    }

    public function index(Request $request)
    {

        $userQuery = User::query();

        $request->session()->put('user_inputs', Input::except('_excel'));

        if ($request->s_name != "")
            $userQuery->where('users.name', 'LIKE', "%{$request->s_name}%");

        if ($request->s_cpf != "")
            $userQuery->where('users.cpf', 'LIKE', "%{$request->s_cpf}%");

        if ($request->s_email != "")
            $userQuery->where('users.email', 'LIKE', "%{$request->s_email}%");

        if ($request->s_role_id != "")
            $userQuery->where('users.role_id', $request->s_role_id);


        if ($request->o_name != "")
            $userQuery->orderBy('users.name', $request->o_name);

        if ($request->o_created_at != "")
            $userQuery->orderBy('users.created_at', $request->o_created_at);


        if($request->_excel) {
            $data = $userQuery
                ->join('roles', 'roles.id', '=', 'users.role_id')
                ->join('cities', 'cities.id', '=', 'users.city_id')
                ->join('states', 'states.id', '=', 'cities.state_id')
                ->join('regionals', 'regionals.id', '=', 'states.regional_id')
                ->select(
                    'users.name AS Nome',
                    'users.cpf AS CPF',
                    'users.email AS Email',
                    'roles.name AS Perfil',
                    'regionals.name AS Regional',
                    'states.name AS Estado',
                    'cities.name AS Cidade'
                )
                ->get()->toArray();

            Excel::create('usuarios', function($excel) use($data) {

                $excel->sheet('Usuarios do sistema', function($sheet) use($data) {

                    $sheet->fromArray($data);

                });

            })->export('xlsx');
        }


        $request->flash();

        $users = $userQuery->orderBy('name')->paginate(15);
        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $user = new User;
      $states = State::lists('name', 'id');
      $roles = Role::lists('name', 'id');
      array_except($roles, Role::TEACHER);

      if(Auth::user()->role_id != Role::NATIONAL_COORDINATOR)
      {
        array_except($roles, Role::NATIONAL_COORDINATOR);
      }

      $cities = [];

      return view('user.create', compact('user','states','roles', 'cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'name' => 'required',
        'cpf' => 'required|cpf|unique:users',
        'email' => 'required|email|max:255|unique:users',
        'password' => 'required|confirmed|min:6',
        'city_id' => 'integer|required',
        'state_id' => 'integer|required',
        'role_id' => 'integer|required',
      ]);

      User::create([
        'name' => $request->name,
        'cpf' => $request->cpf,
        'email' => $request->email,
        'password' => bcrypt($request->password),
        'role_id' => $request->role_id,
        'city_id' => $request->city_id,
        'is_blocked' => 0
      ]);

      return Redirect::route('user.index',$request->session()->get('user_inputs'))->with('success', 'Usuário cadastrado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $user = User::findOrFail($id);

      return view('user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $user->state_id = $user->city->state_id;
        $states = State::lists('name', 'id');
        $roles = Role::lists('name', 'id');

        // if($user->role_id != Role::TEACHER)
        // {
        //     array_except($roles, Role::TEACHER);
        // }

        if(Auth::user()->role_id != Role::NATIONAL_COORDINATOR)
        {
            array_except($roles, Role::NATIONAL_COORDINATOR);
        }
        $regionals = Regional::lists('name', 'id');
        $cities = $user->city->state->citiesOrdered->lists('name','id');

        return view('user.edit', compact('user','regionals','states','roles', 'cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $user = User::findOrFail($id);

      $this->validate($request, [
          'name' => 'required',
          'cpf' => 'required|cpf|unique:users,cpf,'.$user->id,
          'email' => 'required|email|max:255|unique:users,email,'.$user->id,
          'password' => 'confirmed|min:6',
          'city_id' => 'integer|required',
          'state_id' => 'integer|required',
          'role_id' => 'integer|required',
      ]);


        if($request->password)
        {
            $user->password = bcrypt($request->password);
        }

        $user->name = $request->name;
        $user->cpf = $request->cpf;
        $user->email = $request->email;
        $user->role_id = $request->role_id;
        $user->city_id = $request->city_id;

      $user->save();
      return Redirect::route('user.index',$request->session()->get('user_inputs'))->with('success', 'Usuário atualizado com sucesso!');
    }

    /**
     * Block the specified user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function block(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->is_blocked = true;

        $user->save();
        return Redirect::route('user.index',$request->session()->get('user_inputs'))->with('success', 'Usuário bloqueado com sucesso!');
    }

    /**
     * Unblock the specified user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function unblock(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->is_blocked = false;

        $user->save();
        return Redirect::route('user.index',$request->session()->get('user_inputs'))->with('success', 'Usuário desbloqueado com sucesso!');

    }
}
