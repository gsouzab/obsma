<?php

namespace OBSMA\Http\Controllers;

use Illuminate\Http\Request;

use OBSMA\Http\Requests;
use OBSMA\Http\Controllers\Controller;

use OBSMA\Regional;
use OBSMA\State;

class RegionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $regionals = Regional::orderBy('name')->paginate(15);

      return view('regional.index', ['regionals' => $regionals]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $regional = new Regional;
      $states = State::lists('name','id');
      return view('regional.create', ['regional' => $regional, 'states' => $states]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'name' => 'required|alpha',
        'address' => 'required|alpha_dash',
        'place' => 'required|alpha_dash',
        'neighborhood' => 'required|alpha_dash',
        'coordinator_name' => 'required|alpha',
        'email' => 'required|email',
        'city_id' => 'required|integer'
      ]);

      Regional::create([
        'name' => $request->name,
        'address' => $request->address,
        'complement' => $request->complement,
        'zip_code' => $request->zip_code,
        'place' => $request->place,
        'neighborhood' => $request->neighborhood,
        'coordinator_name' => $request->coordinator_name,
        'email' => $request->email,
        'city_id' => $request->city_id,
        'phone_number' => $request->phone_number,
        'fax_number' => $request->fax_number
      ]);

      return redirect('/regional')->with('success', 'Regional cadastrada com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $regional = Regional::findOrFail($id);
      return view('regional.show', ['regional' => $regional]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $regional = Regional::findOrFail($id);
      $states = State::lists('name','id');
      return view('regional.edit', ['regional' => $regional, 'states' => $states]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $regional = Regional::findOrFail($id);

      $this->validate($request, [
        'name' => 'required',
        'address' => 'required',
        'place' => 'required',
        'neighborhood' => 'required',
        'coordinator_name' => 'required',
        'email' => 'required|email',
        'city_id' => 'required|integer'
      ]);

      $regional->name = $request->name;
      $regional->place = $request->place;
      $regional->address = $request->address;
      $regional->neighborhood = $request->neighborhood;
      $regional->complement = $request->complement;
      $regional->zip_code = $request->zip_code;
      $regional->coordinator_name = $request->coordinator_name;
      $regional->email = $request->email;
      $regional->city_id = $request->city_id;
      $regional->phone_number = $request->phone_number;
      $regional->fax_number = $request->fax_number;

      return redirect('/regional')->with('success', 'Regional atualizada com sucesso!');

    }
}
