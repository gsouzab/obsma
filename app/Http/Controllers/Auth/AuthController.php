<?php

namespace OBSMA\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use OBSMA\User;
use OBSMA\Teacher;
use OBSMA\AcademicQualification;
use OBSMA\Subject;
use OBSMA\State;
use OBSMA\Role;
use OBSMA\Work;
use Validator;
use OBSMA\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    protected $username = 'cpf';

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['getLogout', 'getAuthenticateCertificate', 'postAuthenticateCertificate']]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
      return Validator::make($data, [
          'name' => 'required|max:255',
          'cpf' => 'required|cpf|unique:users',
          'email' => 'required|email|max:255|unique:users',
          'password' => 'required|confirmed|min:6',
          'phone_number' => 'required',
          'city_id' => 'required',
          'gender' => 'required',
          'other_gender' => 'required_if:gender,3',
          'address' => 'required',
          'zip_code' => 'required',
          'birthdate' => 'required',
          'graduation_type' => 'required',
          'subjects' => 'required',
          'race' => 'required'
      ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
      $teacher = Teacher::create([
        'academic_qualification_id' => $data['academic_qualification_id'],
        'birthdate' => $data['birthdate'],
        'graduation_type' => $data['graduation_type'],
        'graduation_course' => $data['graduation_course'],
        'phone_number' => $data['phone_number'],
        'mobile_phone_number' => $data['mobile_phone_number'],
        'address' => $data['address'],
        'complement' => $data['complement'],
        'zip_code' => $data['zip_code'],
        'neighborhood' => $data['neighborhood'],
        'gender' => $data['gender'],
        'other_gender' => $data['other_gender'],
        'race' => $data['race'],
      ]);

      $teacher->subjects()->attach($data['subjects']);

      Session::flash('success', "PREZADO PROFESSOR {$data['name']}, \nObrigada por seu cadastro! \nUma mensagem de confirmação foi enviado ao seu email.");

        $user = $teacher->user()->create([
            'name' => $data['name'],
            'email' => $data['email'],
            'cpf' => $data['cpf'],
            'city_id' => $data['city_id'],
            'role_id' => Role::TEACHER,
            'password' => bcrypt($data['password']),
            'is_blocked' => false,
        ]);

        Mail::send('emails.register', ['user' => $user], function ($message) use ($user) {

            $message->to($user->email, $user->name)
                ->subject('Registro concluído - Sistema OBSMA');

            $nationalCoordinators = User::ofRole(Role::NATIONAL_COORDINATOR)->get();
            foreach($nationalCoordinators as $coordinator) {
                $message->bcc($coordinator->email, $coordinator->name);
            }

        });

      return $user;
    }


    protected function getCredentials(Request $request) {
        return [
            'cpf' => $request->get('cpf'),
            'password' => $request->get('password'),
            'is_blocked' => false
        ];
    }

    public function getRegister()
    {
      $academicQualifications = AcademicQualification::lists('name','id');
      $states = State::lists('name','id');
      $subjects = Subject::lists('name','id');

      $teacher = new Teacher;
      return view('auth.register',compact('teacher','academicQualifications', 'states', 'subjects'));
    }


    public function getAuthenticateCertificate() {
        return view('auth.authenticate_certificate');
    }

    public function postAuthenticateCertificate(Request $request) {

        $this->validate($request, [
            'authentication_code' => 'required|size:12',
        ]);

        $authentication_code = $request->get('authentication_code');

        $isAuthentic = Work::where(DB::raw("MD5(id)"), "LIKE",  "{$authentication_code}%")->exists();

        return view('auth.authenticate_certificate', compact('isAuthentic'));
    }
}
