<?php

namespace OBSMA\Http\Controllers;

use Illuminate\Http\Request;

use OBSMA\Http\Requests;
use OBSMA\Http\Controllers\Controller;

use OBSMA\City;
use OBSMA\State;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::orderBy('name')->paginate(15);

        return view('city.index', ['cities' => $cities]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $city = new City;
      $states = State::lists('name', 'id');

      return view('city.create', compact('city','states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'name' => 'required',
        'state_id' => 'required|integer'
      ]);

      City::create([
        'name' => $request->name,
        'state_id' => $request->state_id
      ]);

      return redirect('city')->with('success', 'Município criado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $city = City::findOrFail($id);

      return view('city.show', compact('city'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $city = City::findOrFail($id);
        $states = State::lists('name', 'id');

        return view('city.edit', compact('city','states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $city = City::findOrFail($id);

      $this->validate($request, [
        'name' => 'required',
        'state_id' => 'required|integer'
      ]);

      $city->name = $request->name;
      $city->state_id = $request->state_id;

      $city->save();

      return redirect('city')->with('success', 'Município atualizado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
