<?php

namespace OBSMA\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use OBSMA\Role;

class NationalCoodinator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && ($request->user()->role_id != Role::NATIONAL_COORDINATOR)) {
            return redirect('home')->with('danger', 'Você não possui permissão para acessar essa página.');
        }

        return $next($request);
    }
}
