<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// Authentication Routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Authenticate certificate
Route::get('autenticar', 'Auth\AuthController@getAuthenticateCertificate');
Route::post('autenticar', 'Auth\AuthController@postAuthenticateCertificate');

// Registration Routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::post('mail/contact', 'EmailController@sendEmailContact');
Route::get('state/{id}/cities', 'StateController@cities');

Route::get('/', 'Auth\AuthController@getLogin');

Route::group(array('middleware' => 'auth'), function(){

  $closingDate = "2018-08-16 15:00:00";

  $isClosed = time() > strtotime($closingDate);

  if(!$isClosed) {
    Route::get('work/create_step/{step}', 'WorkController@createStep')->where(['step' => '[1-4]']);
    Route::post('work/store_step/{step}', 'WorkController@storeStep')->where(['step' => '[1-4]']);
  }

  Route::get('work/review', 'WorkController@review');
  Route::get('work/confirm', 'WorkController@confirm');
  Route::get('work/discard', 'WorkController@discard');

  Route::resource('work', 'WorkController');
  Route::get('work/print/{id}', ['as' => 'work.print', 'uses' => 'WorkController@printWork']);
  Route::get('school/find/{state_id}/{city_id}/{query?}', 'SchoolController@query');
  Route::get('school/{school_id}/adm_dept', 'SchoolController@getAdministrativeDepartment');
  Route::get('category/{category_id}/years', 'CategoryController@getYears');

  Route::get('certificate','CertificateController@index');
  Route::get('certificate/teacher/{work_id}',['as' => 'certificate.teacher', 'uses' => 'CertificateController@teacherCertificate']);
  Route::get('certificate/student/{work_id}',['as' => 'certificate.student', 'uses' => 'CertificateController@studentCertificate']);
  Route::get('certificate/winnerStudent/{work_id}',['as' => 'certificate.winner_student', 'uses' => 'CertificateController@winnerStudentCertificate']);
  Route::get('certificate/winnerTeacher/{work_id}',['as' => 'certificate.winner_teacher', 'uses' => 'CertificateController@winnerTeacherCertificate']);

  Route::group(array('middleware' => 'national_coodinator'), function() {
    Route::resource('city', 'CityController');
    Route::resource('state', 'StateController');
    Route::resource('regional', 'RegionalController');
    Route::resource('subject', 'SubjectController');
    Route::resource('academic_qualification', 'AcademicQualificationController');
    Route::resource('edition', 'EditionController');
    Route::resource('school', 'SchoolController');
    Route::resource('category', 'CategoryController');
    Route::resource('category_year', 'CategoryYearController');
    Route::get('edition/activate/{id}', ['as' => 'edition.activate', 'uses' => 'EditionController@activate']);
    Route::get('work/validate/{id}', ['as' => 'work.validate', 'uses' => 'WorkController@validateWork']);
    Route::get('work/winner/{id}', ['as' => 'work.winner', 'uses' => 'WorkController@setIsWinner']);
    Route::get('users/search-teachers', 'UserController@searchTeachers');
  });

  Route::group(array('middleware' => 'admin'), function() {
    Route::get('report/works', ['as' => 'report.works', 'uses' => 'ReportController@works']);
    Route::get('report/teachers', ['as' => 'report.teachers', 'uses' => 'ReportController@teachers']);
    Route::resource('user', 'UserController');
    Route::get('user/block/{id}', ['as' => 'user.block', 'uses' => 'UserController@block']);
    Route::get('user/unblock/{id}', ['as' => 'user.unblock', 'uses' => 'UserController@unblock']);
    Route::post('work/update_status/{id}', ['as' => 'work.update_status', 'uses' => 'WorkController@updateStatus']);
    Route::get('email', 'EmailController@compose');
    Route::post('email/send', 'EmailController@sendEmail');
    Route::post('email/upload', 'EmailController@upload');
  });

  Route::get('home', function() {

    if(Auth::user()->role_id == \OBSMA\Role::TEACHER) {

      $closingDate = "2018-08-16 15:00:00";

      $isClosed = time() > strtotime($closingDate);

      $showCertificates = \OBSMA\Work::where('is_valid',1)->where('teacher_id',Auth::user()->userable_id)->count() > 0;
      return view('home', compact('showCertificates', 'isClosed'));
    } else {

      if (Session::has('success')) {
        Session::keep('success');
      }

      if (Session::has('warning')) {
        Session::keep('warning');
      }

      if (Session::has('danger')) {
        Session::keep('danger');
      }
      
      return redirect('report/works');
    }
  });

  Route::get('profile', 'ProfileController@getProfile');
  Route::post('profile/update', 'ProfileController@updateProfile');
  Route::get('profile/password', 'ProfileController@getPassword');
  Route::post('profile/update_password', 'ProfileController@updatePassword');


});