<?php

namespace OBSMA;

use Illuminate\Database\Eloquent\Model;

class ParticipationType extends Model
{
    protected $fillable = ['name'];
}
