<?php

namespace OBSMA;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $fillable = [
        'name',
        'code',
        'year',
        'administrative_department',
        'type',
        'state_id',
        'city_id',
        'address',
        'neighborhood',
        'zip_code',
        'mailbox',
        'email',
        'phone_number'
    ];

    protected $dates = ['year'];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

}
