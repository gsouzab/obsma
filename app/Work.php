<?php

namespace OBSMA;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    protected $fillable = [
        'regional_id',
        'teacher_id',
        'school_id',
        'city_id',
        'category_id',
        'teachers_total',
        'students_total',
    ];


    private $areaLabel = array(
        1 => 'Indígena',
        2 => 'Quilombola',
        3 => 'Outra',
        4 => 'Não se aplica'
    );
    
    protected $dates = ['validation_date'];

    public function participationType()
    {
        return $this->belongsTo(ParticipationType::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function edition()
    {
        return $this->belongsTo(Edition::class);
    }

    public function workStatus()
    {
        return $this->belongsTo(WorkStatus::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function modality()
    {
        return $this->belongsTo(Modality::class);
    }

    public function regional()
    {
        return $this->belongsTo(Regional::class);
    }

    public function teacher()
    {
        return $this->belongsTo(Teacher::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function categoryYears() {
        return $this->belongsToMany(CategoryYear::class)->withTimestamps();
    }

    public function students() {
        return $this->hasMany(StudentWork::class);
    }

    public function getAuthenticityCodeAttribute() {
        return substr(md5($this->id), 0 ,12);
    }

    public function getAreaLabelAttribute() {
        if(!$this->area) return '-';
        return $this->area == 3 ? $this->other_area : $this->areaLabel[$this->area];
    }

    public function getCategoryYearsFormattedAttribute()
    {
        return implode(', ',$this->categoryYears()->lists('name')->toArray());
    }

}
