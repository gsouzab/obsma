<?php

namespace OBSMA;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Teacher extends Model
{
  protected $table = "users_teachers";

  protected $fillable = [
    'academic_qualification_id',
    'graduation_course',
    'graduation_type',
    'birthdate',
    'mobile_phone_number',
    'phone_number',
    'address',
    'complement',
    'zip_code',
    'neighborhood',
    'previuos_participations',
    'gender',
    'other_gender',
    'race'
  ];

  private $graduationTypeLabel = array(
    1 => 'Superior Completo',
    2 => 'Superior Incompleto'
  );

  private $genderLabel = array(
    1 => 'Masculino',
    2 => 'Feminino',
    3 => 'Outro',
  );

  private $raceLabel = array(
    1 => 'Brancos',
    2 => 'Pardos',
    3 => 'Pretos',
    4 => 'Amarelos',
    5 => 'Indígenas'
  );

  protected $dates = ['birthdate'];

  public function user() {
    return $this->morphOne('OBSMA\User', 'userable');
  }

  public function subjects() {
    return $this->belongsToMany(Subject::class)->withTimestamps();
  }

  public function getGraduationTypeLabelAttribute() {
    return $this->graduationTypeLabel[$this->graduation_type];
  }

  public function getRaceLabelAttribute() {
    return $this->race ? $this->raceLabel[$this->race] : '-';
  }

  public function getGenderLabelAttribute() {
    return $this->gender < 3 ? $this->genderLabel[$this->gender] : $this->other_gender;
  }

  public function setBirthdateAttribute($date) {
    $this->attributes['birthdate'] = Carbon::createFromFormat('d/m/Y', $date);
  }

  public function academicQualification() {
    return $this->belongsTo(AcademicQualification::class);
  }
}
