<?php

namespace OBSMA\Jobs;

use Illuminate\Mail\Mailer;
use OBSMA\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use OBSMA\User;

class SendInternalEmail extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $users;
    protected $subject;
    protected $message;

    /**
     * Create a new job instance.
     *
     * @param  User  $user
     * @return void
     */
    public function __construct($users, $subject, $message)
    {
        $this->users = $users;
        $this->message = $message;
        $this->subject = $subject;
    }

    /**
     * Execute the job.
     *
     * @param  Mailer  $mailer
     * @return void
     */
    public function handle(Mailer $mailer)
    {

        $mailer->send('emails.internal', ['message_text' => $this->message], function ($message) {

            $message->subject($this->subject);
            $message->to('olimpiada@fiocruz.br', 'OBSMA');

            foreach($this->users as $user) {
                if(filter_var($user->email, FILTER_VALIDATE_EMAIL))
                    $message->bcc($user->email, $user->name);
            }

        });
    }
}
