<?php

namespace OBSMA;

use Illuminate\Database\Eloquent\Model;

class Regional extends Model
{
  protected $fillable = [
    'name',
    'place',
    'address',
    'complement',
    'zip_code',
    'neighborhood',
    'city_id',
    'phone_number',
    'fax_number',
    'coordinator_name'
  ];

  public function city()
  {
     return $this->belongsTo(City::class);
  }

  public function states()
  {
     return $this->hasMany(State::class);
  }

  public function getStatesPaginatedAttribute()
  {
     return $this->states()->paginate(10);
  }
}
