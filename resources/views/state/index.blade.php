@extends('layouts.internal')

@section('header_title')
Estados
@endsection

@section('content')
<div class="container-fluid">
  {!! link_to('state/create', "Novo", array('class' => 'btn btn-primary')) !!}
  <br>
  <br>

  <div class="row">
    <div class="col-xs-12 text-right">
      {!! "Mostrando <b>" . $states->count() . "</b> de <b>" . $states->total() . "</b> resultados" !!}
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Sigla</th>
            <th>Nome</th>
            <th>Regional</th>
            <th>Ações</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($states as $state)
            <tr>
              <td>{{ $state->acronym }}</td>
              <td>{{ $state->name }}</td>
              <td>{{ $state->regional == null ? '' : $state->regional->name }}</td>
              <td>
                <a class="btn btn-xs btn-default" href="{{url('state',[$state])}}"> <i class="fa fa-eye"> </i> Ver </a>
                <a class="btn btn-xs btn-default" href="{{route('state.edit',[$state])}}"> <i class="fa fa-pencil"> </i> Editar </a>
              </td>
            </tr>
          @empty
            <tr>
              <td colspan="2">Nenhum estado cadastrado</td>
            </tr>
          @endforelse
        </tbody>
      </table>
    </div>
  </div>
  {!! $states->render() !!}
</div>
@endsection
