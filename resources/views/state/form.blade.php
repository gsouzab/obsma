{!! Form::model($state, array('route' => ($state->exists ? array('state.update', $state->id) : 'state.store'),'method' => ($state->exists ? 'PUT' : 'POST')) )!!}
  <div class="row">
    <div class="col-sm-6">
      <div class='form-group @if ($errors->has('acronym')) has-error @endif'>
        {!! Form::label('acronym', "* Sigla") !!}
        {!! Form::text('acronym', null,array('class' => 'form-control')) !!}
        @if ($errors->has('acronym')) <p class="help-block">{{ $errors->first('acronym') }}</p> @endif
      </div>
      <div class='form-group @if ($errors->has('name')) has-error @endif'>
        {!! Form::label('name', "* Nome") !!}
        {!! Form::text('name', null,array('class' => 'form-control')) !!}
        @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
      </div>

      <div class='form-group @if ($errors->has('regional_id')) has-error @endif'>
        {!! Form::label('regional_id', "Regional") !!}
        {!! Form::select('regional_id', $regionals,  null, array('placeholder' => 'Selecione a regional...', 'class' => 'form-control state-selector')) !!}
        @if ($errors->has('regional_id')) <p class="help-block">{{ $errors->first('regional_id') }}</p> @endif
      </div>
    </div>
  </div>

  {!! Form::submit('Salvar', array('class' => 'btn btn-primary')) !!}
  {!! link_to('state', "Voltar", array('class' => 'btn btn-default')) !!}
{!! Form::close() !!}
