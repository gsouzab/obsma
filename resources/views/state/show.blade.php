@extends('layouts.internal')

@section('header_title')
Estado
@endsection

@section('content')
<div class="container-fluid">
  <fieldset>
    <legend>{{$state->name}}</legend>
    <div class="row">
      <div class="col-sm-6">
        <p><strong>Sigla:</strong> {{$state->acronym}} </p>
        <p><strong>Regional:</strong> {{$state->regional === null ? '' : $state->regional->name}} </p>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Cidades</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($state->cities_paginated as $city)
              <tr>
                <td>{{ $city->name }}</td>
              </tr>
            @empty
              <tr>
                <td>Nenhuma cidade cadastrada</td>
              </tr>
            @endforelse
          </tbody>
        </table>
        {!! $state->cities_paginated->render() !!}
      </div>
    </div>
  </fieldset>

  {!! link_to('state', "Voltar", array('class' => 'btn btn-default')) !!}
  {!! link_to_route('state.edit', "Editar", array('id' => $state->id), array('class' => 'btn btn-default')) !!}
</div>
@endsection
