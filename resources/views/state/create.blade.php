@extends('layouts.internal')

@section('header_title')
Novo Estado
@endsection

@section('content')
<div class="container-fluid">
  @include('state/form')
</div>
@endsection
