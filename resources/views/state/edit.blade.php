@extends('layouts.internal')

@section('header_title')
Editar Estado
@endsection

@section('content')
<div class="container-fluid">
  @include('state/form')
</div>
@endsection
