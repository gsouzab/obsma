@extends('layouts.internal')

@section('header_title')
Novo email
@endsection

@section('content')
<div class="container-fluid">

  {{--<div class="alert alert-info">--}}
{{--    {!! $queueCount !!} emails na fila de envio--}}
  {{--</div>--}}
  {!! Form::open(array('url' => 'email/send')) !!}
    {!! Form::token() !!}
    <div class="form-group">
      {!! Form::label('role_id', 'Perfil') !!}
      {!! Form::select('role_id', $roles,null,array('placeholder' => 'Todos os perfis', 'class' => 'form-control')) !!}
    </div>

    <div class="form-group">
      {!! Form::label('regional_id', 'Regional') !!}
      {!! Form::select('regional_id', $regionals,null,array('placeholder' => 'Todas as regionais', 'class' => 'form-control')) !!}
    </div>
    <div class="form-group {{ $errors->has('subject') ? 'has-error' : ''}}">
      {!! Form::label('subject', 'Assunto') !!}
      {!! Form::text('subject',null, array('class' => 'form-control')) !!}
      {!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
      {!! Form::textarea('email',null, array('class' => 'summernote')) !!}
      {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>


    {!! Form::submit('Enviar', array('class' => 'btn btn-primary')) !!}
  {!! Form::close() !!}
</div>
@endsection
