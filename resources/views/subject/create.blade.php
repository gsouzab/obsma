@extends('layouts.internal')

@section('header_title')
Nova Disciplina
@endsection

@section('content')
<div class="container-fluid">
  @include('subject/form')
</div>
@endsection
