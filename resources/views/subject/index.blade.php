@extends('layouts.internal')

@section('header_title')
Disciplinas
@endsection

@section('content')
<div class="container-fluid">
  {!! link_to('subject/create', "Novo", array('class' => 'btn btn-primary')) !!}
  <br>
  <br>

  <div class="row">
    <div class="col-xs-12 text-right">
      {!! "Mostrando <b>" . $subjects->count() . "</b> de <b>" . $subjects->total() . "</b> resultados" !!}
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Nome</th>
            <th>Ações</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($subjects as $subject)
            <tr>
              <td>{{ $subject->name }}</td>
              <td>
                <a class="btn btn-xs btn-default" href="{{url('subject',[$subject])}}"> <i class="fa fa-eye"> </i> Ver </a>
                <a class="btn btn-xs btn-default" href="{{route('subject.edit',[$subject])}}"> <i class="fa fa-pencil"> </i> Editar </a>
              </td>
            </tr>
          @empty
            <tr>
              <td colspan="2">Nenhuma disciplina cadastrada</td>
            </tr>
          @endforelse
        </tbody>
      </table>
    </div>
  </div>
  {!! $subjects->render() !!}
</div>
@endsection
