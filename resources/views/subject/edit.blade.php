@extends('layouts.internal')

@section('header_title')
Editar Disciplina
@endsection

@section('content')
<div class="container-fluid">
  @include('subject/form')
</div>
@endsection
