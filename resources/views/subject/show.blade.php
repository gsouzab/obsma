@extends('layouts.internal')

@section('header_title')
Disciplina
@endsection

@section('content')
<div class="container-fluid">
  <fieldset>
    <legend>{{$subject->name}}</legend>
  </fieldset>

  {!! link_to('subject', "Voltar", array('class' => 'btn btn-default')) !!}
  {!! link_to_route('subject.edit', "Editar", array('id' => $subject->id), array('class' => 'btn btn-default')) !!}
</div>
@endsection
