<html>
<head>

    <style>
        @page { margin: 0in; }
        
        img {
            position: absolute;
        }

        p {
            width: 800px;
            margin: auto;
            z-index: 400;
            margin-top: 350px;
            font-size: 22pt;
            /*font-family: Swis;*/
        }

        .signature {
            margin-top: 4em;
            font-size: 12pt !important;
            text-align: center;
        }
    </style>
    <title>Certificado - Olimpíada Brasileira de Saúde e Meio Ambiente</title>
</head>
<body>
<div>
    <img src='img/certificado.jpg' width="100%">
    <p><b>Certificamos que</b> o(a) aluno(a) ........................................................................................................... participou da <b>{!! $work->edition->name !!}</b> da <b>Fundação Oswaldo Cruz</b>, com o trabalho {!! $work->title !!}.</p>
    <p class="signature">Este documento foi gerado pelo Sistema de Inscrições OBSMA / FIOCRUZ<br>
        Para verificar a autenticidade desse documento acesse: www.obsma.fiocruz.br/autenticar<br>
        Código de autenticação: {{$work->authenticity_code}}
    </p>
</div>
</body>
</html>