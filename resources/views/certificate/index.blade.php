@extends('layouts.internal')

@section('header_title')
Certificados
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-xs-12 text-right">
      {!! "Mostrando <b>" . $works->count() . "</b> de <b>" . $works->total() . "</b> resultados" !!}
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Nr. Inscrição</th>
            <th>Título</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @forelse ($works as $work)
            <tr>
              <td>{{ $work->inscription_number }}</td>
              <td>{{ str_limit($work->title, 50) }}</td>
              <td>
                {!! link_to_route('certificate.teacher', 'Professor', ['work_id' => $work->id], ['class' => 'btn btn-xs btn-default', 'target' => '_blank']) !!}
                {!! link_to_route('certificate.student', 'Aluno', ['work_id' => $work->id], ['class' => 'btn btn-xs btn-default', 'target' => '_blank']) !!}
                @if($work->is_winner)
                  {!! link_to_route('certificate.winner_teacher', 'Destaque Professor', ['work_id' => $work->id], ['class' => 'btn btn-xs btn-default', 'target' => '_blank']) !!}
                  {!! link_to_route('certificate.winner_student', 'Destaque Aluno', ['work_id' => $work->id], ['class' => 'btn btn-xs btn-default', 'target' => '_blank']) !!}
                @endif
              </td>
            </tr>
          @empty
            <tr>
              <td colspan="2">Nenhum trabalho validado</td>
            </tr>
          @endforelse
        </tbody>
      </table>
    </div>
  </div>
  {!! $works->render() !!}
  {!! link_to('/', "Voltar", array('class' => 'btn btn-default')) !!}

</div>
@endsection
