@extends('layouts.internal')

@section('header_title')
Ano
@endsection

@section('content')
<div class="container-fluid">
  <fieldset>
    <legend>{{$categoryYear->name}}</legend>
    <div class="row">
      <div class="col-sm-6">
        <p><strong>Categoria:</strong> {{$categoryYear->category->name}} </p>
      </div>
    </div>
  </fieldset>

  {!! link_to('category_year', "Voltar", array('class' => 'btn btn-default')) !!}
  {!! link_to_route('category_year.edit', "Editar", array('id' => $categoryYear->id), array('class' => 'btn btn-default')) !!}
</div>
@endsection
