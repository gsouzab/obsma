{!! Form::model($categoryYear, array('route' => ($categoryYear->exists ? array('category_year.update', $categoryYear->id) : 'category_year.store'),'method' => ($categoryYear->exists ? 'PUT' : 'POST')) )!!}
  <div class="row">

    <div class="col-sm-6">
      <div class='form-group @if ($errors->has('category_id')) has-error @endif'>
        {!! Form::label('category_id', "* Categoria") !!}
        {!! Form::select('category_id', $categories,  null, array('placeholder' => 'Selecione a categoria...', 'class' => 'form-control state-selector')) !!}
        @if ($errors->has('category_id')) <p class="help-block">{{ $errors->first('category_id') }}</p> @endif
      </div>

      <div class='form-group @if ($errors->has('name')) has-error @endif'>
        {!! Form::label('name', "* Nome") !!}
        {!! Form::text('name', null,array('class' => 'form-control')) !!}
        @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
      </div>


    </div>
  </div>

  {!! Form::submit('Salvar', array('class' => 'btn btn-primary')) !!}
  {!! link_to('category_year', "Voltar", array('class' => 'btn btn-default')) !!}
{!! Form::close() !!}
