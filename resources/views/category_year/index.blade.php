@extends('layouts.internal')

@section('header_title')
Anos
@endsection

@section('content')
<div class="container-fluid">
  {!! link_to('category_year/create', "Novo", array('class' => 'btn btn-primary')) !!}
  <br>
  <br>

  <div class="row">
    <div class="col-xs-12 text-right">
      {!! "Mostrando <b>" . $categoryYears->count() . "</b> de <b>" . $categoryYears->total() . "</b> resultados" !!}
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Nome</th>
            <th>Categoria</th>
            <th>Ações</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($categoryYears as $categoryYear)
            <tr>
              <td>{{ $categoryYear->name }}</td>
              <td>{{ $categoryYear->category->name }}</td>
              <td>
                <a class="btn btn-xs btn-default" href="{{url('category_year',[$categoryYear])}}"> <i class="fa fa-eye"> </i> Ver </a>
                <a class="btn btn-xs btn-default" href="{{route('category_year.edit',[$categoryYear])}}"> <i class="fa fa-pencil"> </i> Editar </a>
              </td>
            </tr>
          @empty
            <tr>
              <td colspan="2">Nenhum ano cadastrado</td>
            </tr>
          @endforelse
        </tbody>
      </table>
    </div>
  </div>
  {!! $categoryYears->render() !!}
</div>
@endsection
