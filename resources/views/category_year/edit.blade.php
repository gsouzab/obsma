@extends('layouts.internal')

@section('header_title')
Editar Ano
@endsection

@section('content')
<div class="container-fluid">
  @include('category_year/form')
</div>
@endsection
