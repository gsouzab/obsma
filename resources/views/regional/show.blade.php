@extends('layouts.internal')

@section('header_title')
Regional
@endsection

@section('content')
<div class="container-fluid">
  <fieldset>
    <legend>{{$regional->name}}</legend>
    <div class="row">
      <div class="col-sm-6">
        <p><strong>Local:</strong> {{$regional->place}} </p>
        <p><strong>Endereço:</strong> {{$regional->address}} </p>
        <p><strong>Complemento:</strong> {{$regional->complement}} </p>
        <p><strong>Bairro:</strong> {{$regional->neighborhood}} </p>
        <p><strong>CEP:</strong> {{$regional->zip_code}} </p>
        <p class="text-lowercase text-capitalize"><strong>Cidade:</strong> {{$regional->city->name}} </p>
        <p class="text-capitalize"><strong>Estado:</strong> {{$regional->city->state->name}} </p>
      </div>
      <div class="col-sm-6">
        <p><strong>Coordenador:</strong> {{$regional->coordinator_name}} </p>
        <p><strong>Email:</strong> {{$regional->email}} </p>
        <p><strong>Telefone:</strong> {{$regional->phone_number}} </p>
        <p><strong>Fax:</strong> {{$regional->fax_number}} </p>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Estados</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($regional->states_paginated as $state)
              <tr>
                <td>{{ $state->name }}</td>
              </tr>
            @empty
              <tr>
                <td>Nenhuma estado cadastrado</td>
              </tr>
            @endforelse
          </tbody>
        </table>
        {!! $regional->states_paginated->render() !!}
      </div>
    </div>
  </fieldset>

  {!! link_to('regional', "Voltar", array('class' => 'btn btn-default')) !!}
  {!! link_to_route('regional.edit', "Editar", array('id' => $regional->id), array('class' => 'btn btn-default')) !!}
</div>
@endsection
