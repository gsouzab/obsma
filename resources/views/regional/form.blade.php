{!! Form::model($regional, array('route' => ($regional->exists ? array('regional.update', $regional->id) : 'regional.store'),'method' => ($regional->exists ? 'PUT' : 'POST'))) !!}
  <div class="row">
    <div class="col-sm-6">
      <div class='form-group @if ($errors->has('name')) has-error @endif'>
        {!! Form::label('name', "* Nome") !!}
        {!! Form::text('name', null,array('class' => 'form-control')) !!}
        @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
      </div>
      <div class='form-group @if ($errors->has('place')) has-error @endif'>
        {!! Form::label('place', "* Local") !!}
        {!! Form::text('place', null,array('class' => 'form-control')) !!}
        @if ($errors->has('place')) <p class="help-block">{{ $errors->first('place') }}</p> @endif
      </div>

      <div class='form-group @if ($errors->has('address')) has-error @endif'>
        {!! Form::label('address', "* Endereço") !!}
        {!! Form::text('address', null,array('class' => 'form-control')) !!}
        @if ($errors->has('address')) <p class="help-block">{{ $errors->first('address') }}</p> @endif
      </div>

      <div class='form-group @if ($errors->has('neighborhood')) has-error @endif'>
        {!! Form::label('neighborhood', "* Bairro") !!}
        {!! Form::text('neighborhood', null,array('class' => 'form-control')) !!}
        @if ($errors->has('neighborhood')) <p class="help-block">{{ $errors->first('neighborhood') }}</p> @endif
      </div>

      <div class='form-group @if ($errors->has('complement')) has-error @endif'>
        {!! Form::label('complement', "Complemento") !!}
        {!! Form::text('complement', null,array('class' => 'form-control')) !!}
        @if ($errors->has('complement')) <p class="help-block">{{ $errors->first('complement') }}</p> @endif
      </div>

      <div class='form-group @if ($errors->has('zip_code')) has-error @endif'>
        {!! Form::label('zip_code', "CEP") !!}
        {!! Form::text('zip_code', NULL, array('class' => 'form-control')) !!}
        @if ($errors->has('zip_code')) <p class="help-block">{{ $errors->first('zip_code') }}</p> @endif
      </div>
      <div class='form-group @if ($errors->has('state_id')) has-error @endif'>
        {!! Form::label('state_id', "* Estado") !!}
        {!! Form::select('state_id', $states, ($regional->exists ? $regional->city->state->id : null), array('placeholder' => 'Selecione o estado...', 'class' => 'form-control state-selector')) !!}
        @if ($errors->has('state_id')) <p class="help-block">{{ $errors->first('state_id') }}</p> @endif
      </div>

      <div class='form-group @if ($errors->has('city_id')) has-error @endif'>
        {!! Form::label('city_id', "* Cidade") !!}
        {!! Form::select('city_id', ($regional->exists ? $regional->city->state->cities->sortBy('name')->lists('name','id') : []), null, array('placeholder' => 'Selecione a cidade...','class' => 'form-control city-selector')) !!}
        @if ($errors->has('city_id')) <p class="help-block">{{ $errors->first('city_id') }}</p> @endif
      </div>
    </div>
    <div class="col-sm-6">
      <div class='form-group @if ($errors->has('coordinator_name')) has-error @endif'>
        {!! Form::label('coordinator_name', "* Coordenador") !!}
        {!! Form::text('coordinator_name', null,array('class' => 'form-control')) !!}
        @if ($errors->has('coordinator_name')) <p class="help-block">{{ $errors->first('coordinator_name') }}</p> @endif
      </div>

      <div class='form-group @if ($errors->has('email')) has-error @endif'>
        {!! Form::label('email', "* Email") !!}
        {!! Form::text('email', null,array('class' => 'form-control')) !!}
        @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
      </div>

      <div class='form-group @if ($errors->has('phone_number')) has-error @endif'>
        {!! Form::label('phone_number', "Telefone") !!}
        {!! Form::text('phone_number', null,array('class' => 'form-control phone_number')) !!}
        @if ($errors->has('phone_number')) <p class="help-block">{{ $errors->first('phone_number') }}</p> @endif
      </div>

      <div class='form-group @if ($errors->has('fax_number')) has-error @endif'>
        {!! Form::label('fax_number', "Fax") !!}
        {!! Form::text('fax_number', null,array('class' => 'form-control')) !!}
        @if ($errors->has('fax_number')) <p class="help-block">{{ $errors->first('fax_number') }}</p> @endif
      </div>
    </div>
  </div>

  {!! Form::submit('Salvar', array('class' => 'btn btn-primary')) !!}
  {!! link_to('regional', "Voltar", array('class' => 'btn btn-default')) !!}
{!! Form::close() !!}
