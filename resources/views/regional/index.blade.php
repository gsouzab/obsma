@extends('layouts.internal')

@section('header_title')
Regionais
@endsection

@section('content')
<div class="container-fluid">

  {!! link_to('regional/create', "Novo", array('class' => 'btn btn-primary')) !!}
  <br>
  <br>

  <div class="row">
    <div class="col-xs-12 text-right">
      {!! "Mostrando <b>" . $regionals->count() . "</b> de <b>" . $regionals->total() . "</b> resultados" !!}
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Nome</th>
            <th>Coordenador</th>
            <th>Telefone</th>
            <th>Ações</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($regionals as $regional)
            <tr>
              <td>{{ $regional->name }}</td>
              <td>{{ $regional->coordinator_name }}</td>
              <td>{{ $regional->phone_number }}</td>
              <td>
                <a class="btn btn-xs btn-default" href="{{url('regional',[$regional])}}"> <i class="fa fa-eye"> </i> Ver </a>
                <a class="btn btn-xs btn-default" href="{{route('regional.edit',[$regional])}}"> <i class="fa fa-pencil"> </i> Editar </a>
                {{-- <a class="btn btn-xs btn-default" href="{{url('regional',[$regional])}}"> <i class="fa fa-eye"> </i> Ver </a> --}}
              </td>
            </tr>
          @empty
            <tr>
              <td colspan="2">Nenhuma regional cadastrada</td>
            </tr>
          @endforelse
        </tbody>
      </table>
    </div>
  </div>
  {!! $regionals->render() !!}

  {!! link_to('/', "Voltar", array('class' => 'btn btn-default')) !!}

</div>
@endsection
