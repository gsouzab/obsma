@extends('layouts.internal')

@section('header_title')
Editar Regional
@endsection

@section('content')
<div class="container-fluid">
  @include('regional/form')
</div>
@endsection
