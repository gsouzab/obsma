@extends('layouts.internal')

@section('header_title')
    Inscriçao de Trabalho
@endsection

@section('content')
    <div class="container-fluid">
        <legend>
            Passo 3 - Dados do trabalho
            <p><small><small> {!! link_to('work/create_step/1', "1 - Dados da escola") !!} / {!! link_to('work/create_step/2', "2 - Dados dos participantes") !!} / <b>{!! link_to('work/create_step/3', "3 - Dados do trabalho") !!} / 4 - Prêmio Ano Oswaldo Cruz 2017</b></small></small></p>
        </legend>
        @include('work/form_step_3')
    </div>
@endsection