@extends('layouts.internal')

@section('header_title')
    Inscriçao de Trabalho
@endsection

@section('content')
    <div class="container-fluid">
        <fieldset>
            <legend>
                Passo 1 - Dados da escola
                <p><small><small> <b>{!! link_to('work/create_step/1', "1 - Dados da escola") !!}</b> / 2 - Dados dos participantes / 3 - Dado do trabalho / 4 - Prêmio Ano Oswaldo Cruz 2017</small></small></p>
            </legend>
        </fieldset>
        @include('work/form_step_1')
    </div>
@endsection