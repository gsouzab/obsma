@extends('layouts.internal')

@section('header_title')
    Inscriçao de Trabalho
@endsection

@section('content')
    <div class="container-fluid">
        <fieldset>
            <legend>
                Passo 5 - Confirmação
                <p><small><small> {!! link_to('work/create_step/1', "1 - Dados da escola") !!} / {!! link_to('work/create_step/2', "2 - Dados dos participantes") !!} / {!! link_to('work/create_step/3', "3 - Dados do trabalho") !!} / {!! link_to('work/create_step/4', "4 - Prêmio Ano Oswaldo Cruz 2017") !!} / <b>{!! link_to('work/review', "5 - Confirmação") !!}</b></small></small></p>
            </legend>

            <h3 class="text-center">Passo 1 - Dados da escola</h3>
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <p><strong>Estado:</strong> {{$work->state->name}} </p>
                    <p><strong>Cidade:</strong> {{$work->city->name}} </p>
                    <p><strong>Escola:</strong> {{$work->school->name}} </p>
                    <p><strong>Área / Localização:</strong> {{$work->areaLabel}} </p>
                </div>
            </div>

            <h3 class="text-center">Passo 2 - Dados dos participantes</h3>
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <p><strong>Categoria:</strong> {{$work->category->name}} </p>
                    <p><strong>Anos:</strong> {{ $work->categoryYearsFormatted}} </p>
                    <p><strong>Total de professores:</strong> {{$work->teachers_total}} </p>
                    <p><strong>Total de alunos:</strong> {{$work->students_total}} </p>
                </div>
            </div>
            <h4>Alunos</h4>
            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>Nome</th>
                    <th>Data de Nascimento</th>
                    <th>Gênero</th>
                </tr>
                </thead>
                <tbody id="students-list">
                @foreach($work->students()->get() as $key => $student)
                    <tr>
                        <td>
                            {!! $student->name !!}
                        </td>
                        <td>
                            {!! $student->birthdate->format('d/m/Y') !!}
                        </td>
                        <td>
                            {!! $student->genderLabel !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <h3 class="text-center">Passo 3 - Dados do trabalho</h3>
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <p><strong>Modalidade:</strong> {{$work->modality->name}} </p>
                    <p><strong>Participação:</strong> {{$work->participationType->name}} </p>
                    @if($work->modality_id == \OBSMA\Modality::AUDIOVISUAL_PRODUCTION)
                        <p><strong>Título:</strong> {{$work->title}} </p>
                        <p><strong>Duração:</strong> {{$work->duration}} </p>
                        <p><strong>Sinopse:</strong> {{$work->abstract}} </p>
                        <p><strong>Resumo do projeto pedagógico:</strong> {{$work->methodology}} </p>
                    @endif

                    @if($work->modality_id == \OBSMA\Modality::SCIENCE_PROJECT)
                        <p><strong>Título:</strong> {{$work->title}} </p>
                        <p><strong>Resumo:</strong> {{$work->abstract}} </p>
                        <p><strong>Temática abordada:</strong> {{$work->theme}} </p>
                        <p><strong>Objetivo:</strong> {{$work->objective}} </p>
                        <p><strong>Referencial Teórico:</strong> {{$work->theoretical_references}} </p>
                        <p><strong>Metodologia:</strong> {{$work->methodology}} </p>
                        <p><strong>Resultado:</strong> {{$work->results}} </p>
                        <p><strong>Materiais Enviados:</strong> {{$work->sent_material}} </p>
                    @endif

                    @if($work->modality_id == \OBSMA\Modality::TEXT_PRODUCTION)
                        <p><strong>Título:</strong> {{$work->title}} </p>
                        <p><strong>Temática abordada:</strong> {{$work->theme}} </p>
                        <p><strong>Metodologia:</strong> {{$work->methodology}} </p>
                        <p><strong>Gênero:</strong> {{$work->genre}} </p>
                        <p><strong>Número de páginas:</strong> {{$work->number_of_pages}} </p>
                        <p><strong>Utiliza Imagens:</strong> {{$work->uses_images ? 'Sim' : 'Não'}} </p>
                        @if($work->uses_images)
                            <p><strong>Técnicas:</strong> {{$work->techniques }} </p>
                        @endif
                    @endif
                    <p><strong>Palavras-chave:</strong> {{$work->keywords}} </p>
                </div>
            </div>

            <h3 class="text-center">Passo 4 - Prêmio Ano Oswaldo Cruz 2017</h3>
            <br>
            <div class="row">
                <div class="col-sm-12">
                <p><strong>Participando do Prêmio Ano Oswaldo Cruz 2017:</strong> {{$work->has_fiocruz_material ? 'Sim' : 'Não'}} </p>
                @if($work->has_fiocruz_material)
                    <p><strong>Fontes:</strong> {{$work->fiocruz_fonts}} </p>
                @endif
                </div>
            </div>
        </fieldset>
        <div class="row">
            <div class=" col-sm-12">
                {!! link_to('work/confirm', 'Confirmar inscrição', array('class' => 'btn btn-success')) !!}
                {!! link_to('work/create_step/4', "Voltar", array('class' => 'btn btn-default')) !!}
                {!! link_to('work/discard', "Descartar", array('class' => 'btn btn-warning')) !!}
            </div>
        </div>
    </div>
@endsection