{!! Form::model($work, ['method' => 'POST','url' => "work/store_step/{$step}",'class' => 'form-horizontal']) !!}

<div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
  {!! Form::label('category_id', '*Categoria: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::select('category_id', $categories,  null, array('placeholder' => 'Selecione a categoria...', 'class' => 'form-control category-selector')) !!}
    {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('years') ? 'has-error' : ''}}">
  {!! Form::label('years', '*Anos: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::select('years[]', old('category_id') == null ? $years : OBSMA\Category::find(old('category_id'))->years->lists('name','id'),  $work->categoryYears == null ? null : $work->categoryYears()->lists('id')->toArray(), array('multiple' => true, 'data-placeholder' => 'Selecione os anos...', 'class' => 'form-control select-2 years-selector')) !!}
    {!! $errors->first('years', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('modality_id') ? 'has-error' : ''}}">
  {!! Form::label('modality_id', '*Modalidade: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-5">
    {!! Form::select('modality_id', $modalities,  null, array('placeholder' => 'Selecione a modalidade...', 'class' => 'form-control modality-selector')) !!}
    {!! $errors->first('modality_id', '<p class="help-block">:message</p>') !!}
  </div>

  <div class="col-sm-4">
    <b>{!! link_to("http://www.olimpiada.fiocruz.br/duvidas", "Dúvidas no preenchimento, clique aqui!", array('target' => '_blank')) !!}</b>
  </div>
</div>

<br>
<div class="row">
  <div class="col-sm-9 col-sm-offset-1 text-justify">
    <lead>
      PROFESSOR(A), O IMPACTO DE SEU TRABALHO NA COMUNIDADE ESCOLAR É UM QUESITO MUITO IMPORTANTE EM NOSSA AVALIAÇÃO. INFORME AQUI O NÚMERO TOTAL DE PROFESSORES E ALUNOS ENVOLVIDOS EM SEU PROJETO:
    </lead>
  </div>
</div>
<br>
<br>

<div class="form-group {{ $errors->has('teachers_total') ? 'has-error' : ''}}">
  {!! Form::label('teachers_total', '*Total de professores: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-2">
    {!! Form::text('teachers_total', null, array('class' => 'form-control')) !!}
    {!! $errors->first('teachers_total', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('students_total') ? 'has-error' : ''}}">
  {!! Form::label('students_total', '*Total de alunos: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-2">
    {!! Form::text('students_total', null, array('class' => 'form-control')) !!}
    {!! $errors->first('students_total', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<br>
<br>
<lead>*Alunos: <span class="text-danger">inclua o nome e os dados de até 10 alunos</span></lead>
<br>
<lead>O nome de todos os alunos e professores participantes deverá, obrigatoriamente, constar no trabalho / material enviado para a Regional (são também aceitas fotocópias de listas de chamada e diários de classe como comprovante).</lead>

<div class="row">
  <div class="col-sm-12 {{ $errors->has('students') ? 'has-error' : ''}}">
    <table class="table table-condensed">
      <thead>
      <tr>
        <th>Nome</th>
        <th>Data de Nascimento</th>
        <th>Gênero</th>
        <th></th>
      </tr>
      <tr>
        <td>
          {!! Form::text('student_name', null, array('id' => 'student_name','class' => 'form-control')) !!}
        </td>
        <td>
          {!! Form::text('student_birthdate', null, array('id' => 'student_birthdate','class' => 'form-control date')) !!}
        </td>
        <td>
          {!! Form::select('student_gender', [1 => 'Masculino', 2 => 'Feminino'],null, array('id' => 'student_gender','placeholder' => 'Selecione o gênero..','class' => 'form-control')) !!}
        </td>
        <td>
          {!! link_to(null, "[+] Adicionar", array('class' => 'btn btn-success btn-sm btn-add-student')) !!}
        </td>
      </tr>
      </thead>
      <tbody id="students-list">
        @foreach($work->students()->get() as $key => $student)
          <tr>
            <td>
              {!! Form::hidden("students[{$key}][name]", $student->name) !!}
              {!! $student->name !!}
            </td>
            <td>
              {!! Form::hidden("students[{$key}][birthdate]", $student->birthdate->format('d/m/Y')) !!}
              {!! $student->birthdate->format('d/m/Y') !!}
            </td>
            <td>
              {!! Form::hidden("students[{$key}][gender]", $student->gender_id) !!}
              {!! $student->genderLabel !!}
            </td>
            <td>
              {!! link_to(null, "[-] Remover", array('class' => 'btn btn-danger btn-xs btn-remove-student')) !!}
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    {!! $errors->first('students', '<p class="help-block">:message</p>') !!}

  </div>
</div>

<div class="form-group">
  <div class=" col-sm-12">
    {!! Form::submit('Salvar', array('class' => 'btn btn-primary')) !!}
    {!! link_to('work/create_step/1', "Voltar", array('class' => 'btn btn-default')) !!}
    {!! link_to('work/discard', "Descartar", array('class' => 'btn btn-warning')) !!}
  </div>
</div>
{!! Form::close() !!}