{!! Form::model($work, ['method' => 'POST','url' => "work/store_step/{$step}",'class' => 'form-horizontal']) !!}

@if (Auth::user()->role_id == \OBSMA\Role::TEACHER)
  {!! Form::hidden('teacher_id', Auth::user()->userable->id) !!}
@else
  <div class="form-group {{ $errors->has('teacher_id') ? 'has-error' : ''}}">
    {!! Form::label('teacher_id', '*Professor: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
      {!! Form::select('teacher_id', [], old('teacher_id'), ['class' => 'form-control select-2-teachers', 'placeholder' => 'Selecione o professor']) !!}
      {!! $errors->first('teacher_id', '<p class="help-block">:message</p>') !!}
    </div>
  </div>
@endif

<div class="form-group {{ $errors->has('state_id') ? 'has-error' : ''}}">
  {!! Form::label('state_id', '*Estado: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::select('state_id', $states,  null, array('placeholder' => 'Selecione o estado...', 'class' => 'form-control state-selector')) !!}
    {!! $errors->first('state_id', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('city_id') ? 'has-error' : ''}}">
  {!! Form::label('city_id', '*Município: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::select('city_id', old('state_id') == null ? $cities : OBSMA\State::find(old('state_id'))->citiesOrdered->lists('name','id'),  null, array('placeholder' => 'Selecione o município...', 'class' => 'form-control city-selector')) !!}
    {!! $errors->first('city_id', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('city_id') ? 'has-error' : ''}}">
  {!! Form::label('school_query', 'Localize a escola: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::text('school_query', null, array('class' => 'form-control school-query')) !!}
    {!! $errors->first('school_query', '<p class="help-block">:message</p>') !!}
  </div>
  <div class="col-sm-3">
    {!! link_to(null, "Localizar", array('class' => 'btn btn-default btn-find-school')) !!}
  </div>
</div>

<div class="form-group {{ $errors->has('school_id') ? 'has-error' : ''}}">
  {!! Form::label('school_id', '*Escola: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::select('school_id', old('state_id') != null ? \OBSMA\School::where('state_id',old('state_id'))->where('city_id',old('city_id'))->lists('name','id') : $schools ,  null, array('placeholder' => 'Selecione a escola...', 'class' => 'form-control school-selector')) !!}
    {!! $errors->first('school_id', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('adm_dept') ? 'has-error' : ''}}">
  {!! Form::label('adm_dept', 'Dep. Adm. da Escola: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::text('adm_dept', $work->school ? $work->school->administrative_department : "", array('class' => 'form-control', 'disabled' => 'disabled')) !!}
    {!! $errors->first('adm_dept', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
  {!! Form::label('type', 'Tipo: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::text('type', $work->school ? $work->school->type : "", array('class' => 'form-control', 'disabled' => 'disabled')) !!}
    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class='form-group @if ($errors->has('area')) has-error @endif'>
  {!! Form::label('area', "*Área / Localização", ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::select('area', [ 1 => 'Indígena',
                                2 => 'Quilombola',
                                3 => 'Outra',
                                4 => 'Não se aplica'],  null, array('placeholder' => 'Selecione a área / localização...', 'class' => 'form-control area-selector')) !!}
    @if ($errors->has('area')) <p class="help-block">{{ $errors->first('area') }}</p> @endif
  </div>
</div>

<div id='other-area-wrapper' class='@if ($work->area == 3) visible @endif'>
  <div  class='form-group @if ($errors->has('other_area')) has-error @endif'>
    {!! Form::label('other_area', "* Outra Área:", ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
      {!! Form::text('other_area', null, array('class' => 'form-control')) !!}
      @if ($errors->has('other_area')) <p class="help-block">{{ $errors->first('other_area') }}</p> @endif
    </div>
  </div> 
</div> 

<div class="form-group">
  <div class=" col-sm-12">
    {!! Form::submit('Salvar', array('class' => 'btn btn-primary')) !!}
    {!! link_to('work', "Voltar", array('class' => 'btn btn-default')) !!}
    {!! link_to('work/discard', "Descartar", array('class' => 'btn btn-warning')) !!}
  </div>
</div>
{!! Form::close() !!}