@extends('layouts.internal')

@section('header_title')
    Inscriçao de Trabalho
@endsection

@section('content')
    <div class="container-fluid">
        <legend>
            Passo 4 – Prêmio Ano Oswaldo Cruz 2017
            <p><small><small> {!! link_to('work/create_step/1', "1 - Dados da escola") !!} / {!! link_to('work/create_step/2', "2 - Dados dos participantes") !!} / {!! link_to('work/create_step/3', "3 - Dados do trabalho") !!} / <b>{!! link_to('work/create_step/4', "4 - Prêmio Ano Oswaldo Cruz 2017") !!}</b></small></small></p>
        </legend>
        @include('work/form_step_4')
    </div>
@endsection