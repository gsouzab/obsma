@extends('layouts.internal')

@section('header_title')
    Editar Trabalho
@endsection

@section('content')
    <div class="container-fluid">
        {!! Form::model($work, ['method' => 'PUT','route' => array("work.update", $work->id),'class' => 'form-horizontal']) !!}

        @if (Auth::user()->role_id == \OBSMA\Role::TEACHER)
            {!! Form::hidden('teacher_id', Auth::user()->userable->id) !!}
        @else
            <div class="form-group {{ $errors->has('teacher_id') ? 'has-error' : ''}}">
                {!! Form::label('teacher_id', '*Professor: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('teacher_id', $teachers,  null, array('placeholder' => 'Selecione o professor...', 'class' => 'form-control select-2')) !!}
                    {!! $errors->first('teacher_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        @endif

        <div class="form-group {{ $errors->has('state_id') ? 'has-error' : ''}}">
            {!! Form::label('state_id', '*Estado: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::select('state_id', $states,  null, array('placeholder' => 'Selecione o estado...', 'class' => 'form-control state-selector')) !!}
                {!! $errors->first('state_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('city_id') ? 'has-error' : ''}}">
            {!! Form::label('city_id', '*Município: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::select('city_id', old('state_id') == null ? $cities : OBSMA\State::find(old('state_id'))->citiesOrdered->lists('name','id'),  null, array('placeholder' => 'Selecione o município...', 'class' => 'form-control city-selector')) !!}
                {!! $errors->first('city_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('city_id') ? 'has-error' : ''}}">
            {!! Form::label('school_query', 'Localize a escola: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('school_query', null, array('class' => 'form-control school-query')) !!}
                {!! $errors->first('school_query', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-sm-3">
                {!! link_to(null, "Localizar", array('class' => 'btn btn-default btn-find-school')) !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('school_id') ? 'has-error' : ''}}">
            {!! Form::label('school_id', '*Escola: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::select('school_id', $schools ,  null, array('placeholder' => 'Selecione a escola...', 'class' => 'form-control school-selector')) !!}
                {!! $errors->first('school_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('adm_dept') ? 'has-error' : ''}}">
            {!! Form::label('adm_dept', 'Dep. Adm. da Escola: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('adm_dept', null, array('class' => 'form-control', 'disabled' => 'disabled')) !!}
                {!! $errors->first('adm_dept', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
            {!! Form::label('type', 'Tipo: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('type', null, array('class' => 'form-control', 'disabled' => 'disabled')) !!}
                {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class='form-group @if ($errors->has('area')) has-error @endif'>
            {!! Form::label('area', "*Área / Localização", ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::select('area', [ 1 => 'Indígena',
                                            2 => 'Quilombola',
                                            3 => 'Outra',
                                            4 => 'Não se aplica'],  null, array('placeholder' => 'Selecione a área / localização...', 'class' => 'form-control area-selector')) !!}
                @if ($errors->has('area')) <p class="help-block">{{ $errors->first('area') }}</p> @endif
            </div>
        </div>

        <div id='other-area-wrapper' class='@if ($work->area == 3) visible @endif'>
            <div  class='form-group @if ($errors->has('other_area')) has-error @endif'>
                {!! Form::label('other_area', "* Outra Área:", ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                {!! Form::text('other_area', null, array('class' => 'form-control')) !!}
                @if ($errors->has('other_area')) <p class="help-block">{{ $errors->first('other_area') }}</p> @endif
                </div>
            </div> 
        </div> 


        <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
            {!! Form::label('category_id', '*Categoria: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::select('category_id', $categories,  null, array('placeholder' => 'Selecione a categoria...', 'class' => 'form-control category-selector')) !!}
                {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('years') ? 'has-error' : ''}}">
            {!! Form::label('years', '*Anos: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::select('years[]', old('category_id') == null ? $years : OBSMA\Category::find(old('category_id'))->years->lists('name','id'),  $work->categoryYears == null ? null : $work->categoryYears()->lists('id')->toArray(), array('multiple' => true, 'data-placeholder' => 'Selecione os anos...', 'class' => 'form-control select-2 years-selector')) !!}
                {!! $errors->first('years', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <br>
        <div class="row">
            <div class="col-sm-9 col-sm-offset-1 text-justify">
                <lead>
                    PROFESSOR(A), O IMPACTO DE SEU TRABALHO NA COMUNIDADE ESCOLAR É UM QUESITO MUITO IMPORTANTE EM NOSSA AVALIAÇÃO. INFORME AQUI O NÚMERO TOTAL DE PROFESSORES E ALUNOS ENVOLVIDOS EM SEU PROJETO:
                </lead>
            </div>
        </div>
        <br>
        <br>

        <div class="form-group {{ $errors->has('teachers_total') ? 'has-error' : ''}}">
            {!! Form::label('teachers_total', '*Total de professores: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-2">
                {!! Form::text('teachers_total', null, array('class' => 'form-control')) !!}
                {!! $errors->first('teachers_total', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('students_total') ? 'has-error' : ''}}">
            {!! Form::label('students_total', '*Total de alunos: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-2">
                {!! Form::text('students_total', null, array('class' => 'form-control')) !!}
                {!! $errors->first('students_total', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <br>
        <br>
        <lead>*Alunos: <span class="text-danger">inclua o nome e os dados de até 10 alunos</span></lead>
        <br>
        <lead>O nome de todos os alunos e professores participantes deverá, obrigatoriamente, constar no trabalho / material enviado para a Regional (são também aceitas fotocópias de listas de chamada e diários de classe como comprovante).</lead>
        <div class="row">
            <div class="col-sm-12 {{ $errors->has('students') ? 'has-error' : ''}}">
                <table class="table table-condensed">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Data de Nascimento</th>
                        <th>Gênero</th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>
                            {!! Form::text('student_name', null, array('id' => 'student_name','class' => 'form-control')) !!}
                        </td>
                        <td>
                            {!! Form::text('student_birthdate', null, array('id' => 'student_birthdate','class' => 'form-control date')) !!}
                        </td>
                        <td>
                            {!! Form::select('student_gender', [1 => 'Masculino', 2 => 'Feminino'],null, array('id' => 'student_gender','placeholder' => 'Selecione o gênero..','class' => 'form-control')) !!}
                        </td>
                        <td>
                            {!! link_to(null, "[+] Adicionar", array('class' => 'btn btn-success btn-sm btn-add-student')) !!}
                        </td>
                    </tr>
                    </thead>
                    <tbody id="students-list">
                    @foreach($work->students as $key => $student)
                        <tr>
                            <td>
                                {!! Form::hidden("students[{$key}][name]", $student->name) !!}
                                {!! $student->name !!}
                            </td>
                            <td>
                                {!! Form::hidden("students[{$key}][birthdate]", $student->birthdate->format('d/m/Y')) !!}
                                {!! $student->birthdate->format('d/m/Y') !!}
                            </td>
                            <td>
                                {!! Form::hidden("students[{$key}][gender]", $student->gender_id) !!}
                                {!! $student->genderLabel !!}
                            </td>
                            <td>
                                {!! link_to(null, "[-] Remover", array('class' => 'btn btn-danger btn-xs btn-remove-student')) !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $errors->first('students', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('modality_id') ? 'has-error' : ''}}">
            {!! Form::label('modality_id', '*Modalidade: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-5">
                {!! Form::select('modality_id', $modalities,  null, array('placeholder' => 'Selecione a modalidade...', 'class' => 'form-control modality-selector')) !!}
                {!! $errors->first('modality_id', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-sm-4">
                <b>{!! link_to("http://www.olimpiada.fiocruz.br/duvidas", "Dúvidas no preenchimento, clique aqui!", array('target' => '_blank')) !!}</b>
            </div>
        </div>

        <div id="modality-1-wrapper" class="modality-wrapper {!! (($work->modality_id == \OBSMA\Modality::TEXT_PRODUCTION && !old('modality_id')) || old('modality_id') == \OBSMA\Modality::TEXT_PRODUCTION) ? 'modality-selected' : 'hidden' !!}">
            <div class="form-group {{ $errors->has('participation_type_id') ? 'has-error' : ''}}">
                {!! Form::label('participation_type_id', '*Participação: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('participation_type_id', $participation_types,  null, array('placeholder' => 'Selecione a participação...', 'class' => 'form-control participation-type-selector')) !!}
                    {!! $errors->first('participation_type_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                {!! Form::label('title', '*Título: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::textarea('title',  null, array('class' => 'form-control')) !!}
                    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('theme') ? 'has-error' : ''}}">
                {!! Form::label('theme', '*Temática abordada: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::textarea('theme',  null, array('class' => 'form-control')) !!}
                    {!! $errors->first('theme', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('methodology') ? 'has-error' : ''}}">
                {!! Form::label('methodology', '*Metodologia (descrição das atividades desenvolvidas): ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::textarea('methodology',  null, array('class' => 'form-control')) !!}
                    {!! $errors->first('methodology', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('genre') ? 'has-error' : ''}}">
                {!! Form::label('genre', '*Gênero: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    <label>
                        {!! Form::checkbox('genre[]', 'Artigo') !!}
                        Artigo
                    </label>
                    <label>
                        {!! Form::checkbox('genre[]', 'Conto') !!}
                        Conto
                    </label>
                    <label>
                        {!! Form::checkbox('genre[]', 'Cordel') !!}
                        Cordel
                    </label>
                    <label>
                        {!! Form::checkbox('genre[]', 'Crônica') !!}
                        Crônica
                    </label>
                    <label>
                        {!! Form::checkbox('genre[]', 'Ensaio') !!}
                        Ensaio
                    </label>
                    <label>
                        {!! Form::checkbox('genre[]', 'Entrevista') !!}
                        Entrevista
                    </label>
                    <label>
                        {!! Form::checkbox('genre[]', 'Fábula') !!}
                        Fábula
                    </label>
                    <label>
                        {!! Form::checkbox('genre[]', 'História em quadrinho') !!}
                        História em quadrinho
                    </label>
                    <label>
                        {!! Form::checkbox('genre[]', 'Novela') !!}
                        Novela
                    </label>
                    <label>
                        {!! Form::checkbox('genre[]', 'Paródia') !!}
                        Paródia
                    </label>
                    <label>
                        {!! Form::checkbox('genre[]', 'Peça Teatral') !!}
                        Peça Teatral
                    </label>
                    <label>
                        {!! Form::checkbox('genre[]', 'Poema') !!}
                        Poema
                    </label>
                    <label>
                        {!! Form::checkbox('genre[]', 'Redação') !!}
                        Redação
                    </label>
                    <label>
                        {!! Form::checkbox('genre[]', 'Reportagem') !!}
                        Reportagem
                    </label>
                    <label>
                        {!! Form::checkbox('genre[]', 'Romance') !!}
                        Romance
                    </label>
                    <label>
                        {!! Form::checkbox('genre[]', 'Trova') !!}
                        Trova
                    </label>
                    <label>
                        Outros
                        {!! Form::text('other_genre', null, array('class' => 'form-control')) !!}
                    </label>
                    {!! $errors->first('genre', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('number_of_pages') ? 'has-error' : ''}}">
                {!! Form::label('number_of_pages', '*Número de páginas: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-1">
                    {!! Form::text('number_of_pages',  null, array('class' => 'form-control')) !!}
                    {!! $errors->first('number_of_pages', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('uses_images') ? 'has-error' : ''}}">
                {!! Form::label('uses_images', '*Utiliza imagem: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    <label>
                        {!! Form::radio('uses_images', 1) !!}
                        Sim
                    </label>
                    <label>
                        {!! Form::radio('uses_images', 0) !!}
                        Não
                    </label>
                    {!! $errors->first('uses_images', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group techniques-div {{ $errors->has('techniques') ? 'has-error' : ''}}">
                {!! Form::label('techniques', '*Técnica: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    <label>
                        {!! Form::checkbox('techniques[]', 'Aquarela') !!}
                        Aquarela
                    </label>
                    <label>
                        {!! Form::checkbox('techniques[]', 'Colagens') !!}
                        Colagens
                    </label>
                    <label>
                        {!! Form::checkbox('techniques[]', 'Desenho') !!}
                        Desenho
                    </label>
                    <label>
                        {!! Form::checkbox('techniques[]', 'Fotografia') !!}
                        Fotografia
                    </label>
                    <label>
                        {!! Form::checkbox('techniques[]', 'Gravura') !!}
                        Gravura
                    </label>
                    <label>
                        {!! Form::checkbox('techniques[]', 'Pintura') !!}
                        Pintura
                    </label>
                    <label>
                        {!! Form::checkbox('techniques[]', 'Pop Art') !!}
                        Pop Art
                    </label>
                    <label>
                        {!! Form::checkbox('techniques[]', 'Xilogravura') !!}
                        Xilogravura
                    </label>
                    <label>
                        Outros
                        {!! Form::text('other_techniques', null, array('class' => 'form-control')) !!}
                    </label>
                    {!! $errors->first('techniques', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>


        <div id="modality-2-wrapper" class="modality-wrapper {!! (($work->modality_id == \OBSMA\Modality::AUDIOVISUAL_PRODUCTION && !old('modality_id'))  || old('modality_id') == \OBSMA\Modality::AUDIOVISUAL_PRODUCTION) ? 'modality-selected' : 'hidden' !!}">
            <div class="form-group {{ $errors->has('participation_type_id') ? 'has-error' : ''}}">
                {!! Form::label('participation_type_id', '*Participação: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('participation_type_id', array_except($participation_types, array(2)),  [1], array('class' => 'form-control participation-type-selector')) !!}
                    {!! $errors->first('participation_type_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                {!! Form::label('title', '*Título: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('title',  null, array('class' => 'form-control')) !!}
                    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('theme') ? 'has-error' : ''}}">
                {!! Form::label('theme', '*Temática abordada: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('theme',  null, array('class' => 'form-control')) !!}
                    {!! $errors->first('theme', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('abstract') ? 'has-error' : ''}}">
                {!! Form::label('methodology', '*Resumo do projeto pedagógico: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('methodology',  null, array('class' => 'form-control')) !!}
                    {!! $errors->first('methodology', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('duration') ? 'has-error' : ''}}">
                {!! Form::label('duration', '*Tempo de duração: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-2">
                    {!! Form::text('duration',  null, array('class' => 'form-control')) !!}
                    <p class="help-block">*em minutos</p>
                    {!! $errors->first('duration', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('abstract') ? 'has-error' : ''}}">
                {!! Form::label('abstract', '*Sinopse (resumo ou síntese da obra audiovisual): ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('abstract',  null, array('class' => 'form-control')) !!}
                    {!! $errors->first('abstract', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>

        <div id="modality-3-wrapper" class="modality-wrapper {!! (($work->modality_id == \OBSMA\Modality::SCIENCE_PROJECT && !old('modality_id')) || old('modality_id') == \OBSMA\Modality::SCIENCE_PROJECT) ? 'modality-selected' : 'hidden' !!}">
            <div class="form-group {{ $errors->has('participation_type_id') ? 'has-error' : ''}}">
                {!! Form::label('participation_type_id', '*Participação: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('participation_type_id', $participation_types,  null, array('placeholder' => 'Selecione a participação...', 'class' => 'form-control participation-type-selector')) !!}
                    {!! $errors->first('participation_type_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                {!! Form::label('title', '*Título: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::textarea('title',  null, array('class' => 'form-control')) !!}
                    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('abstract') ? 'has-error' : ''}}">
                {!! Form::label('abstract', '*Resumo: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::textarea('abstract',  null, array('class' => 'form-control')) !!}
                    {!! $errors->first('abstract', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('theme') ? 'has-error' : ''}}">
                {!! Form::label('theme', '*Temática abordada: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::textarea('theme',  null, array('class' => 'form-control')) !!}
                    {!! $errors->first('theme', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('objective') ? 'has-error' : ''}}">
                {!! Form::label('objective', '*Objetivo: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::textarea('objective',  null, array('class' => 'form-control')) !!}
                    {!! $errors->first('objective', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('theoretical_references') ? 'has-error' : ''}}">
                {!! Form::label('theoretical_references', '*Referencial Teórico: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::textarea('theoretical_references',  null, array('class' => 'form-control')) !!}
                    {!! $errors->first('theoretical_references', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('methodology') ? 'has-error' : ''}}">
                {!! Form::label('methodology', '*Metodologia (descrição das atividades desenvolvidas): ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::textarea('methodology',  null, array('class' => 'form-control')) !!}
                    {!! $errors->first('methodology', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('results') ? 'has-error' : ''}}">
                {!! Form::label('results', '*Resultado: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::textarea('results',  null, array('class' => 'form-control')) !!}
                    {!! $errors->first('results', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('sent_material') ? 'has-error' : ''}}">
                {!! Form::label('sent_material', '*Materiais Enviados: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::textarea('sent_material',  null, array('class' => 'form-control')) !!}
                    {!! $errors->first('sent_material', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>

        <div class="form-group {{ $errors->has('keywords') ? 'has-error' : ''}}">
            {!! Form::label('keywords', '*Palavras-chave: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-8">
                {!! Form::text('keywords',  null, array('class' => 'form-control')) !!}
                {!! $errors->first('keywords', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12">
                {!! Form::submit('Atualizar', array('class' => 'btn btn-primary')) !!}
                {!! link_to('work', "Voltar", array('class' => 'btn btn-default')) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection