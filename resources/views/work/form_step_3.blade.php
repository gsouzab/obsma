{!! Form::model($work, ['method' => 'POST','url' => "work/store_step/{$step}",'class' => 'form-horizontal']) !!}


@if($work->modality_id == \OBSMA\Modality::TEXT_PRODUCTION)
  <div>
    <div class="form-group {{ $errors->has('participation_type_id') ? 'has-error' : ''}}">
      {!! Form::label('participation_type_id', '*Participação: ', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-6">
        {!! Form::select('participation_type_id', $participation_types,  null, array('placeholder' => 'Selecione a participação...', 'class' => 'form-control participation-type-selector')) !!}
        {!! $errors->first('participation_type_id', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
      {!! Form::label('title', '*Título: ', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-8">
        {!! Form::textarea('title',  null, array('class' => 'form-control')) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div class="form-group {{ $errors->has('theme') ? 'has-error' : ''}}">
      {!! Form::label('theme', '*Temática abordada: ', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-8">
        {!! Form::textarea('theme',  null, array('class' => 'form-control')) !!}
        {!! $errors->first('theme', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div class="form-group {{ $errors->has('methodology') ? 'has-error' : ''}}">
      {!! Form::label('methodology', '*Metodologia (descrição das atividades desenvolvidas): ', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-8">
        {!! Form::textarea('methodology',  null, array('class' => 'form-control')) !!}
        {!! $errors->first('methodology', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div class="form-group {{ $errors->has('genre') ? 'has-error' : ''}}">
      {!! Form::label('genre', '*Gênero: ', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-8">
        <label>
          {!! Form::checkbox('genre[]', 'Artigo') !!}
          Artigo
        </label>
        <label>
          {!! Form::checkbox('genre[]', 'Conto') !!}
          Conto
        </label>
        <label>
          {!! Form::checkbox('genre[]', 'Cordel') !!}
          Cordel
        </label>
        <label>
          {!! Form::checkbox('genre[]', 'Crônica') !!}
          Crônica
        </label>
        <label>
          {!! Form::checkbox('genre[]', 'Ensaio') !!}
          Ensaio
        </label>
        <label>
          {!! Form::checkbox('genre[]', 'Entrevista') !!}
          Entrevista
        </label>
        <label>
          {!! Form::checkbox('genre[]', 'Fábula') !!}
          Fábula
        </label>
        <label>
          {!! Form::checkbox('genre[]', 'História em quadrinho') !!}
          História em quadrinho
        </label>
        <label>
          {!! Form::checkbox('genre[]', 'Novela') !!}
          Novela
        </label>
        <label>
          {!! Form::checkbox('genre[]', 'Paródia') !!}
          Paródia
        </label>
        <label>
          {!! Form::checkbox('genre[]', 'Peça Teatral') !!}
          Peça Teatral
        </label>
        <label>
          {!! Form::checkbox('genre[]', 'Poema') !!}
          Poema
        </label>
        <label>
          {!! Form::checkbox('genre[]', 'Redação') !!}
          Redação
        </label>
        <label>
          {!! Form::checkbox('genre[]', 'Reportagem') !!}
          Reportagem
        </label>
        <label>
          {!! Form::checkbox('genre[]', 'Romance') !!}
          Romance
        </label>
        <label>
          {!! Form::checkbox('genre[]', 'Trova') !!}
          Trova
        </label>
        <label>
          Outros
          {!! Form::text('other_genre', null, array('class' => 'form-control')) !!}
        </label>
        {!! $errors->first('genre', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div class="form-group {{ $errors->has('number_of_pages') ? 'has-error' : ''}}">
      {!! Form::label('number_of_pages', '*Número de páginas: ', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-1">
        {!! Form::text('number_of_pages',  null, array('class' => 'form-control')) !!}
        {!! $errors->first('number_of_pages', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div class="form-group {{ $errors->has('uses_images') ? 'has-error' : ''}}">
      {!! Form::label('uses_images', '*Utiliza imagem: ', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        <label>
          {!! Form::radio('uses_images', 1) !!}
          Sim
        </label>
        <label>
          {!! Form::radio('uses_images', 0) !!}
          Não
        </label>
        {!! $errors->first('uses_images', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div class="form-group techniques-div {{ $errors->has('techniques') ? 'has-error' : ''}}">
      {!! Form::label('techniques', '*Técnica: ', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-8">
        <label>
          {!! Form::checkbox('techniques[]', 'Aquarela') !!}
          Aquarela
        </label>
        <label>
          {!! Form::checkbox('techniques[]', 'Colagens') !!}
          Colagens
        </label>
        <label>
          {!! Form::checkbox('techniques[]', 'Desenho') !!}
          Desenho
        </label>
        <label>
          {!! Form::checkbox('techniques[]', 'Fotografia') !!}
          Fotografia
        </label>
        <label>
          {!! Form::checkbox('techniques[]', 'Gravura') !!}
          Gravura
        </label>
        <label>
          {!! Form::checkbox('techniques[]', 'Pintura') !!}
          Pintura
        </label>
        <label>
          {!! Form::checkbox('techniques[]', 'Pop Art') !!}
          Pop Art
        </label>
        <label>
          {!! Form::checkbox('techniques[]', 'Xilogravura') !!}
          Xilogravura
        </label>
        <label>
          Outros
          {!! Form::text('other_techniques', null, array('class' => 'form-control')) !!}
        </label>
        {!! $errors->first('techniques', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
  </div>
@elseif($work->modality_id == \OBSMA\Modality::AUDIOVISUAL_PRODUCTION)

  <div >
    <div class="form-group {{ $errors->has('participation_type_id') ? 'has-error' : ''}}">
      {!! Form::label('participation_type_id', '*Participação: ', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-6">
        {!! Form::select('participation_type_id', array_except($participation_types, array(2)),  [1], array('class' => 'form-control participation-type-selector')) !!}
        {!! $errors->first('participation_type_id', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
      {!! Form::label('title', '*Título: ', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-8">
        {!! Form::textarea('title',  null, array('class' => 'form-control')) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div class="form-group {{ $errors->has('theme') ? 'has-error' : ''}}">
      {!! Form::label('theme', '*Temática abordada: ', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-8">
        {!! Form::textarea('theme',  null, array('class' => 'form-control')) !!}
        {!! $errors->first('theme', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div class="form-group {{ $errors->has('abstract') ? 'has-error' : ''}}">
      {!! Form::label('methodology', '*Resumo do projeto pedagógico: ', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-8">
        {!! Form::textarea('methodology',  null, array('class' => 'form-control')) !!}
        {!! $errors->first('methodology', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div class="form-group {{ $errors->has('duration') ? 'has-error' : ''}}">
      {!! Form::label('duration', '*Duração: ', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-2">
        {!! Form::text('duration',  null, array('class' => 'form-control')) !!}
        <p class="help-block">*em minutos</p>
        {!! $errors->first('duration', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div class="form-group {{ $errors->has('abstract') ? 'has-error' : ''}}">
      {!! Form::label('abstract', '*Sinopse (resumo ou síntese da obra audiovisual): ', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-8">
        {!! Form::textarea('abstract',  null, array('class' => 'form-control')) !!}
        {!! $errors->first('abstract', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    
  </div>

@elseif($work->modality_id == \OBSMA\Modality::SCIENCE_PROJECT)

  <div>
    <div class="form-group {{ $errors->has('participation_type_id') ? 'has-error' : ''}}">
      {!! Form::label('participation_type_id', '*Participação: ', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-6">
        {!! Form::select('participation_type_id', array_except($participation_types, array(2)),  [1], array('placeholder' => 'Selecione a participação...', 'class' => 'form-control participation-type-selector')) !!}
        {!! $errors->first('participation_type_id', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
      {!! Form::label('title', '*Título: ', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-8">
        {!! Form::textarea('title',  null, array('class' => 'form-control')) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div class="form-group {{ $errors->has('abstract') ? 'has-error' : ''}}">
      {!! Form::label('abstract', '*Resumo: ', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-8">
        {!! Form::textarea('abstract',  null, array('class' => 'form-control')) !!}
        {!! $errors->first('abstract', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div class="form-group {{ $errors->has('theme') ? 'has-error' : ''}}">
      {!! Form::label('theme', '*Temática abordada: ', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-8">
        {!! Form::textarea('theme',  null, array('class' => 'form-control')) !!}
        {!! $errors->first('theme', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div class="form-group {{ $errors->has('objective') ? 'has-error' : ''}}">
      {!! Form::label('objective', '*Objetivo: ', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-8">
        {!! Form::textarea('objective',  null, array('class' => 'form-control')) !!}
        {!! $errors->first('objective', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div class="form-group {{ $errors->has('theoretical_references') ? 'has-error' : ''}}">
      {!! Form::label('theoretical_references', '*Referencial Teórico: ', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-8">
        {!! Form::textarea('theoretical_references',  null, array('class' => 'form-control')) !!}
        {!! $errors->first('theoretical_references', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div class="form-group {{ $errors->has('methodology') ? 'has-error' : ''}}">
      {!! Form::label('methodology', '*Metodologia (descrição das atividades desenvolvidas): ', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-8">
        {!! Form::textarea('methodology',  null, array('class' => 'form-control')) !!}
        {!! $errors->first('methodology', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div class="form-group {{ $errors->has('results') ? 'has-error' : ''}}">
      {!! Form::label('results', '*Resultado: ', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-8">
        {!! Form::textarea('results',  null, array('class' => 'form-control')) !!}
        {!! $errors->first('results', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

    <div class="form-group {{ $errors->has('sent_material') ? 'has-error' : ''}}">
      {!! Form::label('sent_material', '*Materiais Enviados: ', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-8">
        {!! Form::textarea('sent_material',  null, array('class' => 'form-control')) !!}
        {!! $errors->first('sent_material', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
  </div>
@endif

<div class="form-group {{ $errors->has('keywords') ? 'has-error' : ''}}">
  {!! Form::label('keywords', '*Palavras-chave: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-8">
    {!! Form::text('keywords',  null, array('class' => 'form-control')) !!}
    {!! $errors->first('keywords', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group">
  <div class=" col-sm-12">
    {!! Form::submit('Salvar', array('class' => 'btn btn-primary')) !!}
    {!! link_to('work/create_step/2', "Voltar", array('class' => 'btn btn-default')) !!}
    {!! link_to('work/discard', "Descartar", array('class' => 'btn btn-warning')) !!}
  </div>
</div>

{!! Form::close() !!}