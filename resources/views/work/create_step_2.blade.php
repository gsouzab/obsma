@extends('layouts.internal')

@section('header_title')
    Inscriçao de Trabalho
@endsection

@section('content')
    <div class="container-fluid">
        <fieldset>
            <legend>
                Passo 2 - Dados dos participantes
                <p><small><small> {!! link_to('work/create_step/1', "1 - Dados da escola") !!} / <b>{!! link_to('work/create_step/2', "2 - Dados dos participantes") !!}</b> / 3 - Dados do trabalho / 4 - Prêmio Ano Oswaldo Cruz 2017</small></small></p>
            </legend>

        </fieldset>
        @include('work/form_step_2')
    </div>
@endsection