@extends('layouts.internal')

@section('header_title')
    Trabalho
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(Auth::user()->role_id == \OBSMA\Role::NATIONAL_COORDINATOR)
                    @if(!$work->is_valid)
                        <a class="btn btn-success pull-left" href="{{route('work.validate',[$work])}}"> Validar</a>
                    @elseif(!$work->is_winner)
                        <a class="btn btn-success pull-left" href="{{route('work.winner',[$work])}}"> Marcar como Destaque</a>
                    @endif
                @endif

                <a class="btn btn-default pull-right" target="_blank" href="{{route('work.print',[$work])}}"> <i class="fa fa-print"></i> Imprimir</a>
            </div>
        </div>
        <h3 class="text-center">Dados do envio</h3>
        <hr>
        <br>
        <div class="row">
            <div class="col-sm-6">
                <p><strong>Edição: </strong>{!! $work->edition->name !!}</p>
                <p>
                    <strong>Situação:</strong>
                    @if(Auth::user()->role_id != \OBSMA\Role::TEACHER)
                        <div class="row">
                            {!! Form::model($work, ['method' => 'POST','route' => ["work.update_status", $work]]) !!}
                            <div class="col-sm-6">
                                {!! Form::select('work_status_id', $statuses,  null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="col-sm-3">
                                {!! Form::submit('Atualizar', array( 'class' => 'btn btn-default')) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    @else
                        {{$work->workStatus ? $work->workStatus->name : ""}}
                    @endif
                </p>
            </div>
            <div class="col-sm-6 text-right">
                <p><strong>Data de envio: </strong>{!! $work->created_at->format('d/m/Y \à\s H:i:s') !!}</p>
                <p><strong>Data de atualização: </strong>{!! $work->updated_at->format('d/m/Y \à\s H:i:s') !!}</p>
                @if($work->is_valid)
                    <p><strong>Data de validação: </strong>{!! $work->validation_date->format('d/m/Y \à\s H:i:s') !!}</p>
                @endif
            </div>
        </div>
        <h3 class="text-center">Dados da escola</h3>
        <hr>
        <br>
        <div class="row">
            <div class="col-sm-12">
                @if(Auth::user()->role_id != \OBSMA\Role::TEACHER)
                    <p><strong>Professor:</strong> <a target="_blank" href="{{url('user',[$work->teacher->user])}}"> <i class="fa fa-eye"> </i> {!! $work->teacher->user->name !!} </a></p>
                @endif
                <p><strong>Regional:</strong> {{$work->regional->name}} </p>
                <p><strong>Estado:</strong> {{$work->state->name}} </p>
                <p><strong>Cidade:</strong> {{$work->city->name}} </p>
                <p><strong>Escola:</strong> {{$work->school->name}} </p>
                <p><strong>Tipo da escola:</strong> {{$work->school->type}} </p>
                <p><strong>Área / Localização:</strong> {{$work->areaLabel}} </p>
            </div>
        </div>

        <h3 class="text-center">Dados dos participantes</h3>
        <hr>
        <br>
        <div class="row">
            <div class="col-sm-12">
                <p><strong>Categoria:</strong> {{ $work->category ? $work->category->name : '' }} </p>
                <p><strong>Anos:</strong> {{ $work->categoryYearsFormatted}} </p>
                <p><strong>Total de professores:</strong> {{$work->teachers_total}} </p>
                <p><strong>Total de alunos:</strong> {{$work->students_total}} </p>
            </div>
        </div>
        <h4>Alunos</h4>
        <table class="table table-condensed">
            <thead>
            <tr>
                <th>Nome</th>
                <th>Data de Nascimento</th>
                <th>Gênero</th>
            </tr>
            </thead>
            <tbody id="students-list">
            @foreach($work->students()->get() as $key => $student)
                <tr>
                    <td>
                        {!! $student->name !!}
                    </td>
                    <td>
                        {!! $student->birthdate->format('d/m/Y') !!}
                    </td>
                    <td>
                        {!! $student->genderLabel !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <h3 class="text-center">Dados do trabalho</h3>
        <hr>
        <br>

        <div class="row">
            <div class="col-sm-12">
                <p><strong>Modalidade:</strong> {{$work->modality ? $work->modality->name : ''}} </p>
                <p><strong>Participação:</strong> {{$work->participationType ? $work->participationType->name : ''}} </p>
                @if($work->modality_id == \OBSMA\Modality::AUDIOVISUAL_PRODUCTION)
                    <p><strong>Título:</strong> {{$work->title}} </p>
                    <p><strong>Duração:</strong> {{$work->duration}} </p>
                    <p><strong>Sinopse:</strong> {{$work->abstract}} </p>
                    <p><strong>Temática abordada:</strong> {{$work->theme}} </p>
                    <p><strong>Resumo do projeto pedagógico:</strong> {{$work->methodology}} </p>
                @endif

                @if($work->modality_id == \OBSMA\Modality::SCIENCE_PROJECT)
                    <p><strong>Título:</strong> {{$work->title}} </p>
                    <p><strong>Resumo:</strong> {{$work->abstract}} </p>
                    <p><strong>Temática abordada:</strong> {{$work->theme}} </p>
                    <p><strong>Objetivo:</strong> {{$work->objective}} </p>
                    <p><strong>Referencial Teórico:</strong> {{$work->theoretical_references}} </p>
                    <p><strong>Metodologia:</strong> {{$work->methodology}} </p>
                    <p><strong>Resultado:</strong> {{$work->results}} </p>
                    <p><strong>Materiais Enviados:</strong> {{$work->sent_material}} </p>
                @endif

                @if($work->modality_id == \OBSMA\Modality::TEXT_PRODUCTION)
                    <p><strong>Título:</strong> {{$work->title}} </p>
                    <p><strong>Temática abordada:</strong> {{$work->theme}} </p>
                    <p><strong>Metodologia:</strong> {{$work->methodology}} </p>
                    <p><strong>Gênero:</strong> {{$work->genre}} </p>
                    <p><strong>Número de páginas:</strong> {{$work->number_of_pages}} </p>
                    <p><strong>Utiliza Imagens:</strong> {{$work->uses_images ? 'Sim' : 'Não'}} </p>
                    @if($work->uses_images)
                        <p><strong>Técnicas:</strong> {{$work->techniques }} </p>
                    @endif
                @endif
                <p><strong>Palavras-chave:</strong> {{$work->keywords}} </p>
            </div>
        </div>

        <h3 class="text-center">Prêmio Ano Oswaldo Cruz 2017</h3>
        <br>
        <div class="row">
            <div class="col-sm-12">
            <p><strong>Participando do Prêmio Ano Oswaldo Cruz 2017:</strong> {{$work->has_fiocruz_material ? 'Sim' : 'Não'}} </p>
            @if($work->has_fiocruz_material)
                <p><strong>Fontes:</strong> {{$work->fiocruz_fonts}} </p>
            @endif
            </div>
        </div>

        <div class="row">
            <div class=" col-sm-12">
                @if(!$work->is_valid && $work->teacher_id == Auth::user()->userable_id && !$isClosed)
                    {!! link_to_route('work.edit', 'Editar', array('id' => $work->id), array('class' => 'btn btn-default')) !!}
                @endif
                @if(Auth::user()->role_id == \OBSMA\Role::TEACHER)
                    {!! link_to('work', "Voltar", array('class' => 'btn btn-default')) !!}
                @else
                    {!! link_to(URL::route('report.works', session('report_inputs')), "Voltar", array('class' => 'btn btn-default')) !!}
                @endif
            </div>
        </div>
    </div>
@endsection