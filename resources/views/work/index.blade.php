@extends('layouts.internal')

@section('header_title')
Trabalhos
@endsection

@section('content')
<div class="container-fluid">
  @if(!$isClosed)
    {!! link_to('work/create', "Novo", array('class' => 'btn btn-primary')) !!}
    <br>
    <br>
  @endif
  <div class="row">
    <div class="col-xs-12 text-right">
      {!! "Mostrando <b>" . $works->count() . "</b> de <b>" . $works->total() . "</b> resultados" !!}
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Validado</th>
            <th>Nr. Inscrição</th>
            @if(Auth::user()->role_id != \OBSMA\Role::TEACHER)
              <th>Professor</th>
            @endif
            <th>Título</th>
            <th>Escola</th>
            <th>Situação</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @forelse ($works as $work)
            <tr>
              <td>{!! $work->is_valid ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-times text-danger"></i>' !!}</td>
              <td>{{ $work->inscription_number }}</td>
              @if(Auth::user()->role_id != \OBSMA\Role::TEACHER)
                <td><a target="_blank" href="{{url('user',[$work->teacher->user])}}"> <i class="fa fa-eye"> </i> {!! $work->teacher->user->name !!} </a></td>
              @endif
              <td>{{ str_limit($work->title, 50) }}</td>
              <td>{{ $work->school === null ? '' : $work->school->name }}</td>
              <td>{{ $work->workStatus === null ? '' : $work->workStatus->name }}</td>
              <td>
                <a class="btn btn-xs btn-default" href="{{url('work',[$work])}}"> <i class="fa fa-eye"> </i> Ver </a>
                @if(!$work->is_valid && $work->teacher_id == Auth::user()->userable_id && !$isClosed)
                  <a class="btn btn-xs btn-default" href="{{route('work.edit',[$work])}}"> <i class="fa fa-pencil"> </i> Editar </a>
                @endif
              </td>
            </tr>
          @empty
            <tr>
              <td colspan="2">Nenhum trabalho cadastrado</td>
            </tr>
          @endforelse
        </tbody>
      </table>
    </div>
  </div>
  {!! $works->render() !!}

  {!! link_to('/', "Voltar", array('class' => 'btn btn-default')) !!}

</div>
@endsection
