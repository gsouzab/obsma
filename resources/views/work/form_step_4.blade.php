{!! Form::model($work, ['method' => 'POST','url' => "work/store_step/{$step}",'class' => 'form']) !!}

<div class='row'>
  <div class="form-group {{ $errors->has('has_fiocruz_material') ? 'has-error' : ''}}">
    {!! Form::label('has_fiocuz_material', '*O trabalho utilizou como referência bibliográfica artigos, capítulos, livros, teses, dissertações e/ou recursos educacionais (obras intelectuais utilizadas para fins pedagógicos e afins, tais como multimídias, jogos educacionais, sítios virtuais, entre outros) produzidos pela Fundação Oswaldo Cruz?: ', ['class' => 'col-sm-12 control-label']) !!}
    <div class="col-sm-2">
      {!! Form::select('has_fiocruz_material', [1 => 'Sim', 0 => 'Nāo'],  null, array('placeholder' => 'Selecione...', 'class' => 'form-control fiocruz-prize-selector')) !!}
      {!! $errors->first('has_fiocruz_material', '<p class="help-block">:message</p>') !!}
    </div>
  </div>
</div>
<div class="row">
  <div class='col-sm-12'>
  <lead>Ao responder sim, o professor confirma que deseja concorrer ao Prêmio Ano Oswaldo Cruz 2017.</lead>
  </div>
</div>
<br>

<div id='fonts-wrapper' class='row @if ($work->has_fiocruz_material == 1) visible @endif'>
  <div class="form-group {{ $errors->has('fiocruz_fonts') ? 'has-error' : ''}}">
    {!! Form::label('fiocruz_fonts', '*Cite as fontes (referências e /ou recursos educacionais): ', ['class' => 'col-sm-12 control-label']) !!}
    <div class="col-sm-8">
      {!! Form::textarea('fiocruz_fonts',  null, array('class' => 'form-control')) !!}
      {!! $errors->first('fiocruz_fonts', '<p class="help-block">:message</p>') !!}
    </div>
  </div>
</div>
<br>
<br>
<div class="row">
  <div class="col-sm-12">
    <textarea disabled class="form-control" rows="10">

REGULAMENTO 9ª EDIÇÃO 
Promovida pela Fundação Oswaldo Cruz (Fiocruz), a Olimpíada Brasileira de Saúde e Meio Ambiente (Obsma) é um projeto educacional, bienal, voltado para alunos e professores da educação básica. A Obsma tem como finalidade estimular projetos, ações e atividades educativas voltadas para os temas transversais Saúde e Meio Ambiente. Cada um dos trabalhos inscritos pode contar com a participação de professores de diferentes disciplinas, mas a inscrição na 9ª edição deverá ser realizada por apenas um professor responsável, atuante nos Ensinos Fundamental II ou Médio, incluindo a modalidade Educação de Jovens e Adultos (EJA). Os trabalhos devem ser desenvolvidos por alunos em sala de aula nos anos de 2017 a 2018. 
Nesta edição, reafirmando o objetivo que tem sido central em seu trabalho desde sua criação em 2001 – a promoção da melhoria das condições de vida e saúde da população brasileira –, a Obsma buscará também divulgar os 17 Objetivos de Desenvolvimento Sustentável que compõe a agenda mundial adotada durante a Cúpula das Nações Unidas sobre o Desenvolvimento Sustentável de 2015. Os ODS incluem 169 metas que deverão ser atingidas até 2030. 
 
  
Capítulo I – Do concurso e dos objetivos 
[Art. 1°] A Olimpíada Brasileira de Saúde e Meio Ambiente, em sua nona edição, tem por objetivo estimular professores e alunos do 6º ao 9º ano do Ensino Fundamental e do Ensino Médio, de escolas públicas e privadas, reconhecidas pelo Ministério da Educação (MEC), a refletirem de forma crítica sobre questões relacionadas à saúde, ao meio ambiente e suas interfaces com a educação e a ciência e tecnologia (C&T). 
[Art. 2º] São objetivos específicos da Olimpíada: 
incentivar a realização de projetos que possam contribuir para melhorar as condições de vida e saúde no Brasil; 
disseminar conceitos e práticas em educação ambiental nas escolas de educação básica; 
 estimular intervenções socioambientais que respeitem as diretrizes, os princípios e a missão que orientam as ações do Programa Nacional de Educação Ambiental (ProNEA); 
valorizar o trabalho do professor que desenvolve atividades criativas e inovadoras na escola; 
valorizar o trabalho pedagógico desenvolvido de forma integrada pela escola de educação básica; 
incentivar a capacidade de reflexão e a criatividade dos alunos; 
despertar o interesse pela ciência e tecnologia; 
promover a valorização e a difusão do conhecimento científico e tecnológico; 
contribuir para o processo de construção da cidadania plena no Brasil. 
  
Capítulo II - Do Trabalho e da Inscrição 
[Art. 3º] O trabalho inscrito na Olimpíada deve ser original e inédito e abordar temas sobre saúde e meio ambiente. 
[Art. 4º] O trabalho deve ser realizado durante os anos de 2017 e 2018, período vigente da 9ª edição.  
[Art. 5º] O trabalho deve ser desenvolvido por alunos de escolas da rede de ensino brasileira e inscrito por um professor vinculado a uma instituição de ensino da Educação Básica. 
[Art. 6º] O trabalho deve ser inscrito em uma das duas categorias: Ensino Fundamental ou Ensino Médio (Art.14); e em uma das três modalidades: Produção Audiovisual, Produção de Texto e Projeto de Ciências (Art. 15). 
[Art. 7º] Se o trabalho for orientado por mais de um professor, deve ser escolhido apenas um representante dos docentes para efetuar a inscrição. Esse professor torna-se o representante oficial do trabalho, doravante designado professor responsável. 
 
Parágrafo Único – O professor responsável deve estar cadastrado no sítio oficial da Obsma – www.olimpiada.fiocruz.br – e cabe ao mesmo apresentar e agir em nome do grupo. 
 
[Art. 8°] A inscrição do trabalho é gratuita, realizada em duas etapas: preenchimento do formulário eletrônico disponível no sítio oficial da Obsma; e postagem do material original, via correio convencional, a uma das Coordenações Regionais da Olimpíada. 
[Art. 9º] O prazo para inscrição de trabalhos no sítio da Olimpíada inicia-se no dia 01 de agosto de 2017 e encerra-se às 17h (hora de Brasília) do dia 31 de julho 2018. O prazo para postagem do material original, via correio convencional, encerra-se dia 31 de agosto de 2018 (o selo dos Correios vale como referência). 
 
Parágrafo Único – Todo material – textos, documentos, fotografias, vídeos, pendrives, CDs, DVDs etc. – deve ser enviado para o endereço da Coordenação Regional correspondente ao estado de origem da escola participante (Art. 10 e 34). 
  
Capítulo III – Da Organização 
[Art. 10] A Olimpíada está organizada em seis (6) Coordenações Regionais em território brasileiro. 
Parágrafo Único – O trabalho inscrito fica obrigatoriamente vinculado a uma (1) das seis (6) Coordenações Regionais, de acordo com a cidade e o estado de origem da escola. 
I. Regional Centro-Oeste: Goiás, Mato Grosso, Mato Grosso do Sul, Tocantins e Distrito Federal; 
II. Regional Minas-Sul: Minas Gerais, Paraná, Santa Catarina e Rio Grande do Sul; 
III. Regional Nordeste I: Ceará, Maranhão, Paraíba, Pernambuco, Piauí e Rio Grande do Norte; 
IV. Regional Nordeste II: Alagoas, Bahia e Sergipe; 
V. Regional Norte: Acre, Amapá, Amazonas, Pará, Rondônia e Roraima; 
VI. Regional Sudeste: Espírito Santo, Rio de Janeiro e São Paulo. 
 
[Art. 11] O trabalho e/ou material enviado deve, obrigatoriamente, estar identificado com o título, modalidade, categoria, nome da escola, nome do professor responsável pela inscrição e nome(s) do(s) aluno(s) participantes. 
[Art. 12] As inscrições com dados ou informações incompletas ou inconsistentes, ou os trabalhos que não respeitem o(s) objetivos(s) e as normas da Olimpíada não serão avaliados. 
[Art. 13] Não serão encaminhados às comissões de avaliação: 
a) trabalhos que explorem temas religiosos, partidários e discriminatórios, ferindo os Direitos Humanos; 
b) trabalhos ganhadores de outro(s) prêmio(s) ou concurso(s) realizado(s) no país. 
  
Capítulo IV – Das Categorias e Modalidades 
[Art. 14] A Olimpíada está dividida em duas categorias: 1) Ensino Fundamental – destinada aos alunos do 6º ao 9º ano e 2) Ensino Médio. 
[Art. 15] Cada trabalho deve ser inscrito em uma das seguintes modalidades: 
I. Produção Audiovisual – serão aceitos trabalhos coletivos desenvolvidos por um grupo de alunos, turma ou escola. O tema a ser abordado é de livre escolha dos autores, devendo estar relacionado ao objeto desta Olimpíada. Como critério de avaliação serão observados: o desenvolvimento do tema na linguagem audiovisual escolhida e os impactos do trabalho. 
Os materiais apresentados devem utilizar a linguagem audiovisual e a duração máxima de cada vídeo é de dez (10) minutos. Os vídeos que ultrapassarem este limite de tempo não serão avaliados. 
Os trabalhos podem ser produzidos nos seguintes gêneros audiovisuais: animação, documentário, ficção, vídeo-arte, programa de entrevistas, spot de propaganda, videorreportagem, videoclipe, entre outros. 
As formas de captação aceitas são: AVI, MPEG, WMV, MP4. Para captação de imagens podem ser utilizados: celulares, câmeras fotográficas digitais, filmadoras e/ou todo equipamento profissional que houver. 
II. Produção de Texto – serão aceitos trabalhos individuais ou coletivos. O tema a ser abordado é de livre escolha do(s) autor(es), devendo, necessariamente, ter relação com o objeto desta Olimpíada. O texto deve ser inédito e original, pode ter no máximo 10 (dez) páginas, pode ser ilustrado (literatura de cordel, história em quadrinhos, pinturas, colagens, fotografias, desenhos etc.) e/ou acompanhado de material(is) que complemente(m) as informações sobre o projeto desenvolvido na escola. 
Além do impacto que o trabalho pedagógico obtenha junto à comunidade escolar, as Comissões de Avaliação analisam a capacidade de expressão e a clareza das ideias e dos argumentos, valorizando os trabalhos redigidos na forma padrão da Língua Portuguesa. A capacidade do(s) autor(es) em compreender e expressar as questões ou os problemas referentes à promoção da saúde e/ou preservação do meio ambiente no Brasil e no mundo é considerada fundamental. 
Nessa modalidade, os trabalhos podem explorar quaisquer dos usos, das formas e dos estilos literários existentes. Serão aceitos contos, composições livres, dissertações argumentativas, ensaios, histórias, poemas, crônicas, novelas, reportagens jornalísticas, paródias etc. 
III. Projeto de Ciências – serão aceitos somente projetos coletivos desenvolvidos por um grupo de alunos, turma ou escola, que busquem tornar interessante, dinâmico e inovador o processo de ensino-aprendizagem das diversas disciplinas do Ensino Fundamental e Médio. O tema a ser abordado é de livre escolha dos autores devendo, necessariamente, ter relação com o objeto desta Olimpíada. 
Para efeito de avaliação, será levada em consideração a indicação/enumeração do conjunto de métodos, técnicas ou processos utilizados, os resultados obtidos e o impacto do projeto junto à comunidade escolar. Nesta modalidade, podem ser apresentados projetos diversos, incluindo experimentos científicos destinados à verificação de fenômenos naturais ligados aos temas saúde e/ou meio ambiente. É obrigatória a remessa de registros que contenham, de forma clara e precisa, informações sobre o projeto desenvolvido (fotografias, audiovisuais, áudios etc.). 
[Art. 16] Após a realização da etapa nacional de avaliação, os trabalhos não premiados ficarão à disposição do inscrito por um período de 03 (três) meses para serem retirados, com ônus do professor responsável. Caso sua devolução não seja reclamada nesse período, a Olimpíada não se responsabilizará pela guarda dos mesmos. 
  
 
 
Capítulo V – Da Avaliação e Premiação dos Trabalhos 
[Art. 17] O processo de avaliação dos trabalhos está dividido em duas etapas: regional e nacional. As comissões avaliadoras serão formadas a critério das respectivas coordenações. 
[Art. 18] Na etapa regional as comissões avaliadoras escolhem os vencedores de cada Regional, por categoria e nas 03 (três) modalidades. 
[Art. 19] Os premiados nas etapas regionais concorrerão à etapa nacional. 
[Art. 20] Na etapa nacional são escolhidos os destaques da 9ª Olimpíada Brasileira de Saúde e Meio Ambiente. 
[Art. 21] As decisões das comissões avaliadoras, consignadas em ata, não serão suscetíveis de recursos ou impugnação. 
[Art. 22] Os autores (um professor e um aluno) dos trabalhos vencedores na etapa regional participarão dos eventos programados e da cerimônia de premiação nacional a ser realizada na Fundação Oswaldo Cruz, no Rio de Janeiro, em data a ser definida. 
[Art. 23] Os prêmios da 9ª Olimpíada Brasileira de Saúde e Meio Ambiente, distribuídos por categoria e por modalidade, são definidos pelo Conselho Nacional da Olimpíada/Fiocruz. 
[Art. 24] As Comissões Avaliadoras não estabelecem classificações. 
  
Capítulo VI – Direitos Autorais de Imagem e Som 
[Art. 25] Os professores inscritos concordam com a eventual publicação, pela Fiocruz/Ministério da Saúde de imagens e/ou da totalidade do trabalho inscrito, em forma e suporte a serem definidos, sem que sejam devidas autorização e qualquer remuneração aos participantes e autores, obrigando-se, todavia, a mencionar o crédito integral do trabalho. 
[Art. 26] O realizador do trabalho é responsável exclusivo pela utilização não-autorizada de imagens ou sons de terceiros. Quaisquer problemas de direitos autorais recairão unicamente no responsável pelo trabalho inscrito. 
[Art. 27] Os participantes da Olimpíada autorizam automaticamente o uso de suas imagens e vozes, em todo e qualquer material entre fotos, documentos e outros meios de comunicação. A presente autorização é concedida a título gratuito, abrangendo o uso da imagem anteriormente mencionada, em todo o território nacional e no exterior. 
[Art. 28] O(s) trabalho(s) inscrito(s), validado(s) e/ou premiado(s) na modalidade Audiovisual fica(m) autorizado(s) a ser(em) veiculado(s) no Canal Saúde – emissora de televisão vinculada à Presidência da Fiocruz – e pela VideoSaúde Distribuidora – órgão vinculado ao Instituto de Comunicação e Informação Científica e Tecnológica da Fiocruz. 
  
Capítulo VII - Disposições gerais 
[Art. 29] O Conselho Nacional da Olimpíada Brasileira de Saúde e Meio Ambiente é composto pelos coordenadores nacionais, regionais e três representantes da Presidência da Fiocruz. Os casos omissos serão resolvidos pelo Conselho Nacional da Olimpíada Brasileira de Saúde e Meio Ambiente. 
[Art. 30] A divulgação das listas de trabalhos premiados nas etapas regionais e nacional ocorre por meio dos canais oficiais de comunicação da OBSMA (sítio eletrônico e redes sociais) e os professores responsáveis são comunicados por telefone (através do número informado pelo mesmo no momento de cadastro no sistema de inscrições da Olimpíada). 
[Art. 31] As informações prestadas na inscrição são de inteira responsabilidade do professor responsável. Aquele que não preencher corretamente o formulário ou que fornecer dados comprovadamente inverídicos terá sua inscrição cancelada e excluída. 
[Art. 32] A participação de professores e alunos na 9ª Olimpíada Brasileira de Saúde e Meio Ambiente implica no conhecimento deste Regulamento e na sua aceitação. 
 
 
Parágrafo Único – Cabe ao inscrito a manutenção e atualização de seu cadastro. 
 
[Art. 33] Excepcionalmente, será conferido o Prêmio Ano Oswaldo Cruz 2017 a um trabalho que tenha utilizado como referência bibliográfica artigos, capítulos, livros, teses, dissertações e/ou recursos educacionais (obras intelectuais utilizadas para fins pedagógicos e afins, tais como multimídias, jogos educacionais, sítios virtuais, entre outros) produzidos pela Fundação Oswaldo Cruz. O trabalho deverá obrigatoriamente informar as fontes consultadas. Apenas um trabalho receberá o Prêmio na etapa nacional. Para fins de premiação, não serão consideradas as divisões em Categorias, Modalidades e/ou Regionais. Todos os demais critérios de seleção e avaliação seguirão as normas e procedimentos descritos neste Regulamento. 
Parágrafo Único: Para concorrer ao Prêmio Ano Oswaldo Cruz 2017, o professor responsável deverá assinalar na ficha de inscrição online que seu trabalho está de acordo com o art. 33 do presente Regulamento.  
 
[Art. 34] A Coordenação Nacional e as Coordenações Regionais estão localizadas nas seguintes sedes: 
I. Coordenação Nacional: Vice Presidência de Educação, Informação e Comunicação – Fundação Oswaldo Cruz (Fiocruz). Endereço: Av. Brasil, 4036, Manguinhos, Rio de Janeiro – RJ – CEP: 21041-361 - Telefone: (21) 2560-8259. E-mail: olimpiada@fiocruz.br. 
II. Regional Centro-Oeste: Gerência Regional de Brasília (Gereb) – Fiocruz Brasília. Endereço: Campus Universitário Darcy Ribeiro, Gleba A – Brasília-DF – Caixa Postal: 04311 – CEP: 70904-970 ATENÇÃO: Para envios por SEDEX 10, use o CEP 70910-900 – Telefone: (61) 3329-4522. E-mail: olimpiadacentroeste@fiocruz.br. 
III. Regional Minas/Sul: Instituto René Rachou (IRR) – Fiocruz Minas. Endereço: Av. Augusto Lima, 1.715, Barro Preto, Belo Horizonte – MG – CEP: 30190-000 – Telefone: (31) 3349-7872. E-mail: olimpiada@cpqrr.fiocruz.br. 
IV. Regional Nordeste I: Instituto Aggeu Magalhães (IAM) – Fiocruz Pernambuco. Endereço: Av. Morais Rego s/n, Campus da UFPE, Cidade Universitária, Recife-PE - CEP: 50740-456 – Telefone: (81) 2101-2667. E-mail: olimpiada@cpqam.fiocruz.br. 
V. Regional Nordeste II: Instituto Gonçalo Moniz (IGM) – Fiocruz Bahia. Endereço: Rua Waldemar Falcão, 121, Candeal, Salvador – BA – CEP: 40296-710 – Telefone: (71) 3176-2236. E-mail: olimpiada@cpqgm.fiocruz.br. 
VI. Regional NorteInstituto Leônidas e Maria Deane (ILMD) ​– Fiocruz Amazônia. Endereço: Rua Teresina, 476, Adrianópolis, Manaus – AM – CEP: 69057-070 – Telefone: (92) 3621-2323 E-mail: olimpiada.regionalnorte@amazonia.fiocruz.br 
VII. Regional Sudeste: Escola Politécnica de Saúde Joaquim Venâncio (EPSJV) – Fiocruz. Endereço: Av. Brasil 4365, Manguinhos, Rio de Janeiro – RJ – CEP: 21040-360 – Telefones: (21) 2560-8259 / 3865-9738 / 3865-9740 / 3865-9741. E-mail: olimpiada@fiocruz.br 
    </textarea>
  </div>
</div>

<br>

<div class="form-group {{ $errors->has('agreed') ? 'has-error' : ''}}">
  <div class="col-sm-12">
    {!! Form::checkbox('agreed') !!}
    Li e concordo com o regulamento.
    {!! $errors->first('agreed', '<p class="help-block">:message</p>') !!}
  </div>
</div>


<div class="form-group">
  <div class=" col-sm-12">
    {!! Form::submit('Salvar', array('class' => 'btn btn-primary')) !!}
    {!! link_to('work/create_step/3', "Voltar", array('class' => 'btn btn-default')) !!}
    {!! link_to('work/discard', "Descartar", array('class' => 'btn btn-warning')) !!}
  </div>
</div>

{!! Form::close() !!}