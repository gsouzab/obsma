@extends('layouts.internal')

@section('header_title')
Relatórios
@endsection

@section('content')
<div class="container-fluid">

  <div class="row">
    <div class="col-sm-12">
      <div class="well">
        {!! Form::open(['method' => 'get', 'url' => 'report']) !!}
        {!! Form::hidden('_excel',null, ['id' => "_excel"]) !!}

        <div class="row">
          <div class="col-sm-4">
            <div class="form-group">
              {!! Form::label('s_title', 'Título: ', ['class' => 'control-label']) !!}
              {!! Form::text('s_title', old('s_title'), ['class' => 'form-control', 'placeholder' => 'Título do trabalho']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('s_teacher_id', 'Professor: ', ['class' => 'control-label']) !!}
              {!! Form::select('s_teacher_id', [], old('s_teacher_id'), ['class' => 'form-control select-2-teachers', 'placeholder' => 'Selecione o professor']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('s_regional_id', 'Regional: ', ['class' => 'control-label']) !!}
              {!! Form::select('s_regional_id', \OBSMA\Regional::lists('name', 'id'), old('s_regional_id'), ['class' => 'form-control', 'placeholder' => 'Selecione a regional']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('s_state_id', 'Estado: ', ['class' => 'control-label']) !!}
              {!! Form::select('s_state_id', \OBSMA\State::lists('name', 'id'), old('s_state_id'), ['class' => 'form-control state-selector', 'placeholder' => 'Selecione o estado']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('s_city_id', 'Município: ', ['class' => 'control-label']) !!}
              {!! Form::select('s_city_id', old('s_state_id') ? \OBSMA\State::find(old('s_state_id'))->citiesOrdered->lists('name', 'id') : [], old('s_city_id'), ['class' => 'form-control city-selector', 'placeholder' => 'Selecione o município']) !!}
            </div>


          </div>
          <div class="col-sm-4">
            <div class="form-group">
              {!! Form::label('s_edition_id', 'Edição: ', ['class' => 'control-label']) !!}
              {!! Form::select('s_edition_id', \OBSMA\Edition::lists('name', 'id') , old('edition_id'), ['class' => 'form-control select-2', 'placeholder' => 'Selecione a edição']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('s_category_id', 'Categoria: ', ['class' => 'control-label']) !!}
              {!! Form::select('s_category_id', \OBSMA\Category::lists('name', 'id') , old('s_category_id'), ['class' => 'form-control', 'placeholder' => 'Selecione a categoria']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('s_modality_id', 'Modalidade: ', ['class' => 'control-label']) !!}
              {!! Form::select('s_modality_id', \OBSMA\Modality::lists('name', 'id') , old('s_modality_id'), ['class' => 'form-control', 'placeholder' => 'Selecione a modalidade']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('s_work_status_id', 'Situação: ', ['class' => 'control-label']) !!}
              {!! Form::select('s_work_status_id', \OBSMA\WorkStatus::lists('name', 'id') , old('s_work_status_id'), ['class' => 'form-control', 'placeholder' => 'Selecione a situação']) !!}
            </div>
          </div>

          <div class="col-sm-4">

            <div class="form-group">
              {!! Form::label('s_school', 'Escola: ', ['class' => 'control-label']) !!}
              {!! Form::text('s_school', old('s_school'), ['class' => 'form-control', 'placeholder' => 'Nome da escola']) !!}
            </div>

            <div class='form-group'>
              {!! Form::label('s_administrative_department', "Dept. Administrativo") !!}
              {!! Form::select('s_administrative_department', ['Estadual' => 'Estadual', 'Federal' => 'Federal', 'Municipal' => 'Municipal', 'Particular' => 'Particular', 'Privada' => 'Privada'], null,array('class' => 'form-control','placeholder' => 'Departamento Adm. da escola')) !!}
            </div>


            <div class="form-group">
              {!! Form::label('s_participation_type_id', 'Participação: ', ['class' => 'control-label']) !!}
              {!! Form::select('s_participation_type_id', \OBSMA\ParticipationType::lists('name', 'id') , old('s_participation_type_id'), ['class' => 'form-control', 'placeholder' => 'Selecione a participação']) !!}
            </div>

            <div class="form-group">
              <label class="control-label">Validado:</label>
              <br>
              <label>
                {!! Form::radio('s_is_valid', 1) !!}
                Sim</label>
              <label>
                {!! Form::radio('s_is_valid', 0) !!}
                Não</label>
            </div>


            <div class="form-group">
              <label class="control-label">Destaque:</label>
              <br>
              <label>
                {!! Form::radio('s_is_winner', 1) !!}
                Sim</label>
              <label>
                {!! Form::radio('s_is_winner', 0) !!}
                Não</label>
            </div>

          </div>
        </div>

        <div class="row">
          <div class="col-sm-12">
            {!! Form::button("<i class='fa fa-search'></i> Pesquisar", ['id' => 'search', 'class' => 'btn btn-primary btn-lg']) !!}

            {!! Form::button("<i class='fa fa-file-excel-o'></i> Exportar", ['id' => 'export-works', 'class' => 'btn btn-default btn-lg'])  !!}

            {!! link_to('report',"Todos", ['class' => 'btn btn-default btn-lg'])  !!}
          </div>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>

  <div class="row">

    @if(session('show_boxes'))
      <div class="col-sm-4">
        <div class="bs-callout bs-callout-info">
          <h3 class="panel-title">Professores participantes</h3>
          <h2>{!! $teachers_total !!}</h2>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="bs-callout bs-callout-info">
          <h3 class="panel-title">Alunos participantes</h3>
          <h2>{!! $students_total !!}</h2>
        </div>
      </div>
    @endif
    <div class="col-sm-4">
      <div class="bs-callout bs-callout-info">
        <h3 class="panel-title">Trabalhos enviados</h3>
        <h2>{!! $works_count !!}</h2>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12 text-right">
      {!! "Mostrando <b>" . $works->count() . "</b> de <b>" . $works->total() . "</b> resultados" !!}
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <table class="table table-hover">
        <thead>
        <tr>
          <th>Validado</th>
          <th>Nr. Inscrição</th>
          <th>Professor</th>
          <th>Título</th>
          <th>Escola</th>
          <th>Situação</th>
          <th></th>
        </tr>
        </thead>
        <tbody>
        @forelse ($works as $work)
          <tr>
            {{--{!! dd($work) !!}--}}
            <td>{!! $work->is_valid ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-times text-danger"></i>' !!}</td>
            <td>{{ $work->inscription_number }}</td>
            <td><a target="_blank" href="{{url('user',[$work->teacher->user])}}"> <i class="fa fa-eye"> </i> {!! $work->teacher->user->name !!} </a></td>
            <td>{{ str_limit($work->title, 50) }}</td>
            <td>{{ $work->school === null ? '' : $work->school->name }}</td>
            <td>{{ $work->workStatus === null ? '' : $work->workStatus->name }}</td>
            <td>
              <a class="btn btn-xs btn-default" href="{{url('work',[$work->id])}}"> <i class="fa fa-eye"> </i> Ver </a>
              @if(!$work->is_valid)
                @if(Auth::user()->role_id == \OBSMA\Role::NATIONAL_COORDINATOR)
                  <a class="btn btn-xs btn-success" href="{{route('work.validate',[$work->id])}}"> Validar</a>
                @endif
              @endif
            </td>
          </tr>
        @empty
          <tr>
            <td colspan="2">Nenhum trabalho cadastrado</td>
          </tr>
        @endforelse
        </tbody>
      </table>
    </div>
  </div>
  {!! $works->appends(Input::except('page','_excel'))->render() !!}
</div>
@endsection
