@extends('layouts.internal')

@section('header_title')
Relatórios - Professores
@endsection

@section('content')
<div class="container-fluid">

  <div class="row">
    <div class="col-sm-12">
      <div class="well">
        {!! Form::open(['method' => 'get', 'url' => 'report/teachers']) !!}
        {!! Form::hidden('_excel',null, ['id' => "_excel"]) !!}

        <div class="row">
          <div class="col-sm-4">

            <div class="form-group">
              {!! Form::label('s_regional_id', 'Regional: ', ['class' => 'control-label']) !!}
              {!! Form::select('s_regional_id', \OBSMA\Regional::lists('name', 'id'), old('s_regional_id'), ['class' => 'form-control', 'placeholder' => 'Selecione a regional']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('s_state_id', 'Estado: ', ['class' => 'control-label']) !!}
              {!! Form::select('s_state_id', \OBSMA\State::lists('name', 'id') , old('s_state_id'), ['class' => 'form-control state-selector select-2', 'placeholder' => 'Selecione o estado']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('s_city_id', 'Município: ', ['class' => 'control-label']) !!}
              {!! Form::select('s_city_id', old('s_state_id') == null ? [] : OBSMA\State::find(old('s_state_id'))->citiesOrdered->lists('name','id'),  null, array('placeholder' => 'Selecione o município...', 'class' => 'form-control city-selector select-2')) !!}
            </div>

          </div>
          <div class="col-sm-4">
            <div class="form-group">
              {!! Form::label('s_academic_qualification_id', 'Titulação: ', ['class' => 'control-label']) !!}
              {!! Form::select('s_academic_qualification_id', \OBSMA\AcademicQualification::lists('name', 'id') , old('s_academic_qualification_id'), ['class' => 'form-control', 'placeholder' => 'Selecione a titulação']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('s_graduation_course', 'Curso de Formação: ', ['class' => 'control-label']) !!}
              {!! Form::text('s_graduation_course', old('s_graduation_course'), ['class' => 'form-control', 'placeholder' => 'Curso de formação']) !!}
            </div>
          </div>

          <div class="col-sm-4">
            <div class="form-group">
              {!! Form::label('s_gender', 'Gênero: ', ['class' => 'control-label']) !!}
              {!! Form::select('s_gender', ['1' => 'Masculino',
                                            '2' => 'Feminino',
                                            '3' => 'Outro'] , old('s_gender'), ['class' => 'form-control', 'placeholder' => 'Selecione o gênero']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('s_race', 'Cor / Raça: ', ['class' => 'control-label']) !!}
              {!! Form::select('s_race', ['1' => 'Brancos',
                                          '2' => 'Pardos',
                                          '3' => 'Pretos',
                                          '4' => 'Amarelos',
                                          '5' => 'Indígenas'] , old('s_race'), ['class' => 'form-control', 'placeholder' => 'Selecione a cor/raça']) !!}
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-12">
            {!! Form::button("<i class='fa fa-search'></i> Pesquisar", ['id' => 'search', 'class' => 'btn btn-primary btn-lg']) !!}

            {!! Form::button("<i class='fa fa-file-excel-o'></i> Exportar", ['id' => 'export-works', 'class' => 'btn btn-default btn-lg'])  !!}

            {!! link_to('report/teachers',"Todos", ['class' => 'btn btn-default btn-lg'])  !!}
          </div>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12 text-right">
      {!! "Mostrando <b>" . $teachers->count() . "</b> de <b>" . $teachers->total() . "</b> resultados" !!}
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <table class="table table-hover">
        <thead>
        <tr>
          <th>Nome</th>
          <th>Regional</th>
          <th>Município</th>
          <th>Titulaçao</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($teachers as $teacher)
          <tr>
            <td><a target="_blank" href="{{url('user',[$teacher->id])}}"> <i class="fa fa-eye"> </i> {!! $teacher->name !!} </a></td>
            <td>{{ $teacher->regional }}</td>
            <td>{{ $teacher->city ? $teacher->city->name : "" }}</td>
            <td>{{ $teacher->userable->academicQualification ? $teacher->userable->academicQualification->name : "" }}</td>
          </tr>
        @empty
          <tr>
            <td colspan="2">Nenhum professor cadastrado</td>
          </tr>
        @endforelse
        </tbody>
      </table>
    </div>
  </div>
  {!! $teachers->appends(Input::except('page','_excel'))->render() !!}
</div>
@endsection
