@extends('layouts.external')

@section('header_title')
Recuperação de senha
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-xs-12 login-text-container">
      <h3>Esqueceu a sua senha?</h3>
      <p>Digite no campo abaixo o seu Email. Você receberá instruções para redefinir sua senha.</p>
      <p> Caso tenha dúvidas, escreva para olimpiada@fiocruz.br ou telefone: (21)2560-8259</p>

    </div>
  </div>
  <div class="row">
    <div class="col-md-12 login-form-container">

      {!! Form::open(array('url' => 'password/email','method' => 'POST')) !!}

        <div class="row">
          <div class=" col-sm-5 @if ($errors->has('email')) has-error @endif">
            {!! Form::text('email', null,array('placeholder' => 'Email')) !!}
            @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
          </div>
          <div class="col-sm-3">
            {!! Form::submit('Enviar', array('class' => 'green-button')) !!}
          </div>
        </div>
      {!! Form::close() !!}

    </div>
  </div>
</div>
@endsection
