@extends('layouts.external')

@section('header_title')
Recuperação de senha
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-xs-12 login-text-container">
      <p>Preencha os dados para criar uma nova senha de acesso.</p>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 login-form-container">

      {!! Form::open(array('url' => 'password/reset','method' => 'POST')) !!}
        {!! csrf_field() !!}
        <input type="hidden" name="token" value="{{ $token }}">
        <div class="row">
          <div class="col-sm-2">
            {!! Form::label('email', 'Email') !!}
          </div>
          <div class="col-sm-8 @if ($errors->has('email')) has-error @endif">
            {!! Form::text('email') !!}
            @if ($errors->has('email')) <p class="help-block">{{ $errors->first('cpf') }}</p> @endif
          </div>

        </div>
        <br>
        <div class="row">
          <div class="col-sm-2">
            {!! Form::label('password', 'Senha') !!}
          </div>
          <div class="col-sm-5 @if ($errors->has('password')) has-error @endif">
            {!! Form::password('password') !!}
            @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
          </div>

        </div>
      <br>
        <div class="row">
          <div class="col-sm-2">
            {!! Form::label('password_confirmation', 'Confirma senha') !!}
          </div>
          <div class="col-sm-5 @if ($errors->has('password_confirmation')) has-error @endif">
            {!! Form::password('password_confirmation') !!}
            @if ($errors->has('password_confirmation')) <p class="help-block">{{ $errors->first('password_confirmation') }}</p> @endif
          </div>
        </div>
      <br>
        <div class="row">
          <div class="col-sm-3">
            {!! Form::submit('Enviar', array('class' => 'green-button')) !!}
          </div>
        </div>

      {!! Form::close() !!}

    </div>
  </div>
  <div class="row">
    <div class="col-ms-12 login-text-container">
      <p>*Caso tenha duvidas, ligue para (21) 2560-8259 e solicite a sua senha.</p>
    </div>
  </div>
</div>
@endsection
