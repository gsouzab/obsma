@extends('layouts.external')

@section('header_title')
  Autenticação de certificados
@endsection

@section('content')
<div class="container-fluid">

  @if(isset($isAuthentic))
    <div class="row">
      <div class="col-sm-12">
        @if($isAuthentic)

          <div style="width: 128px; margin: 0 auto;">
            <img class="img-responsive" src="/img/icons/valid-icon.png">
          </div>
          <p class="text-center">Certitificado autêntico</p>

        @else

          <div style="width: 128px; margin: 0 auto;">
            <img class="img-responsive" src="/img/icons/invalid-icon.png">
          </div>
          <p class="text-center">Certitificado inválido</p>

        @endif
      </div>
    </div>
  @endif

  <div class="row">
    <div class="col-md-12 login-form-container">

      {!! Form::open(array('url' => 'autenticar','method' => 'POST')) !!}

        <div class="row">
          <div class=" col-sm-5 @if ($errors->has('authentication_code')) has-error @endif">
            {!! Form::text('authentication_code', null,array('placeholder' => 'Código de autenticação')) !!}
            @if ($errors->has('authentication_code')) <p class="help-block">{{ $errors->first('authentication_code') }}</p> @endif
          </div>
          <div class="col-sm-3">
            {!! Form::submit('Autenticar', array('class' => 'green-button')) !!}
          </div>
        </div>
      {!! Form::close() !!}
    </div>
  </div>

</div>
@endsection
