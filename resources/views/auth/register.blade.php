
@extends('layouts.external')
@section('header_title')
Cadastro de Professores
@endsection

@section('content')
<div class='container-fluid'>
  {!! Form::model($teacher, array('url' => 'auth/register','method' =>'POST', 'id' => 'register-form') )!!}
    <div class="row">
      <div class="col-sm-2 text-right">
        {!! Form::label('name', "*Nome") !!}
      </div>
      <div class="col-sm-8">
        <div class='form-group @if ($errors->has('name')) has-error @endif'>
          {!! Form::text('name', null,array('class' => 'form-control')) !!}
          @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-2 text-right">
        {!! Form::label('email', "*Email") !!}
      </div>
      <div class="col-sm-6">
        <div class='form-group @if ($errors->has('email')) has-error @endif'>
          {!! Form::text('email', null,array('class' => 'form-control')) !!}
          @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-2 text-right">
        {!! Form::label('cpf', "*CPF") !!}
      </div>
      <div class="col-sm-4">
        <div class='form-group @if ($errors->has('cpf')) has-error @endif'>
          {!! Form::text('cpf', null,array('class' => 'form-control cpf')) !!}
          @if ($errors->has('cpf')) <p class="help-block">{{ $errors->first('cpf') }}</p> @endif
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-2 text-right">
        {!! Form::label('password', "*Senha") !!}
      </div>
      <div class="col-sm-4">
        <div class='form-group @if ($errors->has('password')) has-error @endif'>
          {!! Form::password('password',array('class' => 'form-control')) !!}
          @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-2 text-right">
        {!! Form::label('password_confirmation', "*Confirmar senha") !!}
      </div>
      <div class="col-sm-4">
        <div class='form-group @if ($errors->has('password_confirmation')) has-error @endif'>
          {!! Form::password('password_confirmation',array('class' => 'form-control')) !!}
          @if ($errors->has('password_confirmation')) <p class="help-block">{{ $errors->first('password_confirmation') }}</p> @endif
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-2 text-right">
        {!! Form::label('phone_number', "*Telefone") !!}
      </div>
      <div class="col-sm-3">
        <div class='form-group @if ($errors->has('phone_number')) has-error @endif'>
          {!! Form::text('phone_number',null,array('class' => 'form-control phone_number')) !!}
          @if ($errors->has('phone_number')) <p class="help-block">{{ $errors->first('phone_number') }}</p> @endif
        </div>
      </div>

      <div class="col-sm-2 text-right">
        {!! Form::label('mobile_phone_number', "Celular") !!}
      </div>
      <div class="col-sm-3">
        <div class='form-group @if ($errors->has('mobile_phone_number')) has-error @endif'>
          {!! Form::text('mobile_phone_number',null,array('class' => 'form-control cellphone_number')) !!}
          @if ($errors->has('mobile_phone_number')) <p class="help-block">{{ $errors->first('mobile_phone_number') }}</p> @endif
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-2 text-right">
        {!! Form::label('state_id', "*Estado") !!}
      </div>
      <div class="col-sm-3">
        <div class='form-group @if ($errors->has('state_id')) has-error @endif'>
          {!! Form::select('state_id', $states,  null, array('placeholder' => 'Selecione o estado...', 'class' => 'form-control state-selector')) !!}
          @if ($errors->has('state_id')) <p class="help-block">{{ $errors->first('state_id') }}</p> @endif
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-2 text-right">
        {!! Form::label('city_id', "*Município") !!}
      </div>
      <div class="col-sm-3">
        <div class='form-group @if ($errors->has('city_id')) has-error @endif'>
          {!! Form::select('city_id', old('state_id') == null ? [] : OBSMA\State::find(old('state_id'))->citiesOrdered->lists('name','id'),  null, array('placeholder' => 'Selecione o município...', 'class' => 'form-control city-selector')) !!}
          @if ($errors->has('city_id')) <p class="help-block">{{ $errors->first('city_id') }}</p> @endif
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-2 text-right">
        {!! Form::label('address', "*Logradouro") !!}
      </div>
      <div class="col-sm-4">
        <div class='form-group @if ($errors->has('address')) has-error @endif'>
          {!! Form::text('address', null, array('class' => 'form-control')) !!}
          @if ($errors->has('address')) <p class="help-block">{{ $errors->first('address') }}</p> @endif
        </div>
      </div>

      <div class="col-sm-2 text-right">
        {!! Form::label('complement', "Complemento") !!}
      </div>
      <div class="col-sm-2">
        <div class='form-group @if ($errors->has('complement')) has-error @endif'>
          {!! Form::text('complement', null, array('class' => 'form-control')) !!}
          @if ($errors->has('complement')) <p class="help-block">{{ $errors->first('complement') }}</p> @endif
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-2 text-right">
        {!! Form::label('nighborhood', "Bairro") !!}
      </div>
      <div class="col-sm-3">
        <div class='form-group @if ($errors->has('nighborhood')) has-error @endif'>
          {!! Form::text('neighborhood', null, array('class' => 'form-control')) !!}
          @if ($errors->has('neighborhood')) <p class="help-block">{{ $errors->first('neighborhood') }}</p> @endif
        </div>
      </div>

      <div class="col-sm-2 text-right">
        {!! Form::label('zip_code', "* CEP") !!}
      </div>
      <div class="col-sm-3">
        <div class='form-group @if ($errors->has('zip_code')) has-error @endif'>
          {!! Form::text('zip_code', null, array('class' => 'form-control zip_code')) !!}
          @if ($errors->has('zip_code')) <p class="help-block">{{ $errors->first('zip_code') }}</p> @endif
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-2 text-right">
        {!! Form::label('birthdate', "*Data de Nascimento") !!}
      </div>
      <div class="col-sm-3">
        <div class='form-group @if ($errors->has('birthdate')) has-error @endif'>
          {!! Form::text('birthdate', null, array('class' => 'form-control date')) !!}
          @if ($errors->has('birthdate')) <p class="help-block">{{ $errors->first('birthdate') }}</p> @endif
        </div>
      </div>

      <div class="col-sm-2 text-right">
        {!! Form::label('gender', "* Gênero") !!}
      </div>
      <div class="col-sm-3">
        <div class='form-group @if ($errors->has('gender')) has-error @endif'>
          {!! Form::select('gender', [1 => 'Masculino', 2 => 'Feminino', 3 => 'Outro'], null , array('placeholder' => 'Selecione o gênero...','class' => 'form-control gender-selector')) !!}
          @if ($errors->has('gender')) <p class="help-block">{{ $errors->first('gender') }}</p> @endif
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-2 text-right">
        {!! Form::label('race', "*Cor / Raça") !!}
      </div>
      <div class="col-sm-3">
        <div class='form-group @if ($errors->has('race')) has-error @endif'>
          {!! Form::select('race', [ 1 => 'Brancos',
                                     2 => 'Pardos',
                                     3 => 'Pretos',
                                     4 => 'Amarelos',
                                     5 => 'Indígenas'],  null, array('placeholder' => 'Selecione a cor / raça...', 'class' => 'form-control')) !!}
          @if ($errors->has('race')) <p class="help-block">{{ $errors->first('race') }}</p> @endif
        </div>
      </div>
      
      <div id='other-gender-wrapper' class='@if (old('gender') == 3) visible @endif'>
        <div class="col-sm-2 text-right">
          {!! Form::label('other_gender', "* Genêro") !!}
        </div>
        <div class="col-sm-3">
          <div class='form-group @if ($errors->has('other_gender')) has-error @endif'>
            {!! Form::text('other_gender', null, array('class' => 'form-control')) !!}
            @if ($errors->has('other_gender')) <p class="help-block">{{ $errors->first('other_gender') }}</p> @endif
          </div>
        </div> 
      </div> 
    </div>

    <div class="row">
      <div class="col-sm-2 text-right">
        {!! Form::label('graduation_type', "*Formação") !!}
      </div>
      <div class="col-sm-3">
        <div class='form-group @if ($errors->has('graduation_type')) has-error @endif'>
          {!! Form::select('graduation_type', [1 => 'Superior Completo', 2 => 'Superior Incompleto'],  null, array('placeholder' => 'Selecione a formação...', 'class' => 'form-control')) !!}
          @if ($errors->has('graduation_type')) <p class="help-block">{{ $errors->first('graduation_type') }}</p> @endif
        </div>
      </div>
      <div class="col-sm-2 text-right">
        {!! Form::label('academic_qualification_id', "Titulação") !!}
      </div>
      <div class="col-sm-3">
        <div class='form-group @if ($errors->has('academic_qualification_id')) has-error @endif'>
          {!! Form::select('academic_qualification_id', $academicQualifications,  null, array('placeholder' => 'Selecione a titulação...', 'class' => 'form-control')) !!}
          @if ($errors->has('academic_qualification_id')) <p class="help-block">{{ $errors->first('academic_qualification_id') }}</p> @endif
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-2 text-right">
        {!! Form::label('graduation_course', "Curso de Formação") !!}
      </div>
      <div class="col-sm-4">
        <div class='form-group @if ($errors->has('graduation_course')) has-error @endif'>
          {!! Form::text('graduation_course', null, array('class' => 'form-control')) !!}
          @if ($errors->has('graduation_course')) <p class="help-block">{{ $errors->first('graduation_course') }}</p> @endif
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-2 text-right">
        {!! Form::label('subjects', "*Disciplinas") !!}
      </div>
      <div class="col-sm-6">
        <div class='form-group @if ($errors->has('subjects')) has-error @endif'>
          {!! Form::select('subjects[]', $subjects,  null, array('multiple' => true, 'class' => 'form-control select-2', 'id' => 'subjects', 'data-placeholder' => 'Selecione as disciplinas')) !!}
          @if ($errors->has('subjects')) <p class="help-block">{{ $errors->first('subjects') }}</p> @endif
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-2"></div>

      <div class="col-sm-6">
        {!! Form::submit('Registrar', array('class' => 'btn green-button')) !!}
      </div>
    </div>
  {!! Form::close() !!}
</div>
@endsection
