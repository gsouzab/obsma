@extends('layouts.external')

@section('header_title')
Área de login
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-xs-12 login-text-container">

      <h3>Seja bem vindo(a) professor(a)!</h3>
      <p>Faça parte dessa rede: <a class="green-button" href="{!! url("auth/register") !!}">clique aqui e cadastre-se!</a></p>
      <p>Ou acesse seu perfil:</p>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 login-form-container">

      {!! Form::open(array('url' => 'auth/login','method' => 'POST')) !!}
        <div class="row">
          <div class="col-sm-offset-2 col-sm-3 @if ($errors->has('cpf')) has-error @endif">
            {!! Form::text('cpf', null,array('class' => 'cpf','placeholder' => 'CPF')) !!}
            @if ($errors->has('cpf')) <p class="help-block">{{ $errors->first('cpf') }}</p> @endif
          </div>
          <div class="col-sm-3 @if ($errors->has('password')) has-error @endif">
            {!! Form::password('password', array('placeholder' => 'Senha')) !!}
            @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
          </div>
          <div class="col-sm-3">
            {!! Form::submit('Login', array('class' => 'green-button')) !!}
          </div>
        </div>
        <p class="text-center">{!! link_to('password/email', 'Esqueci a senha') !!}</p>
      {!! Form::close() !!}

    </div>
  </div>
  <div class="row">
    <div class="col-ms-12 login-text-container">
      <p>	ATENÇÃO: apenas professores podem efetuar o cadastro no site. Se você não é professor(a) mas está interessado(a) na Olimpíada Brasileira de Saúde e Meio Ambiente, acompanhe as novidades no nosso site e nas redes sociais.</p>
    </div>
  </div>
</div>
@endsection
