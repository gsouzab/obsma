@extends('layouts.internal')
@section('header_title')
    Atualizar senha
@endsection

@section('content')
    <div class='container-fluid'>
        {!! Form::model($user, array('url' => 'profile/update_password','method' =>'POST') )!!}
        <div class="row">
            <div class="col-sm-2 text-right">
                {!! Form::label('current_password', "*Senha atual") !!}
            </div>
            <div class="col-sm-6">
                <div class='form-group @if ($errors->has('current_password')) has-error @endif'>
                    {!! Form::password('current_password',array('class' => 'form-control')) !!}
                    @if ($errors->has('current_password')) <p class="help-block">{{ $errors->first('current_password') }}</p> @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-2 text-right">
                {!! Form::label('password', "*Nova senha") !!}
            </div>
            <div class="col-sm-6">
                <div class='form-group @if ($errors->has('password')) has-error @endif'>
                    {!! Form::password('password',array('class' => 'form-control')) !!}
                    @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-2 text-right">
                {!! Form::label('password_confirmation', "*Confirmar nova senha") !!}
            </div>
            <div class="col-sm-6">
                <div class='form-group @if ($errors->has('confirm_password')) has-error @endif'>
                    {!! Form::password('password_confirmation',array('class' => 'form-control')) !!}
                    @if ($errors->has('password_confirmation')) <p class="help-block">{{ $errors->first('password_confirmation') }}</p> @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-2"></div>

            <div class="col-sm-6">
                {!! Form::submit('Atualizar', array('class' => 'btn green-button')) !!}
                {!! link_to('home', 'Voltar', array('class' => 'btn green-button')) !!}

            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
