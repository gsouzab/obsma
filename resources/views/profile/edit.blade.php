@extends('layouts.internal')
@section('header_title')
Atualizar cadastro
@endsection

@section('content')
<div class='container-fluid'>
  {!! Form::model($user, array('url' => 'profile/update','method' =>'POST') )!!}
    <div class="row">
      <div class="col-sm-2 text-right">
        {!! Form::label('name', "* Nome") !!}
      </div>
      <div class="col-sm-8">
        <div class='form-group @if ($errors->has('name')) has-error @endif'>
          {!! Form::text('name', null,array('class' => 'form-control')) !!}
          @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-2 text-right">
        {!! Form::label('email', "* Email") !!}
      </div>
      <div class="col-sm-6">
        <div class='form-group @if ($errors->has('email')) has-error @endif'>
          {!! Form::text('email', null,array('class' => 'form-control')) !!}
          @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-2 text-right">
        {!! Form::label('cpf', "* CPF") !!}
      </div>
      <div class="col-sm-4">
        <div class='form-group @if ($errors->has('cpf')) has-error @endif'>
          {!! Form::text('cpf', null,array('class' => 'form-control cpf')) !!}
          @if ($errors->has('cpf')) <p class="help-block">{{ $errors->first('cpf') }}</p> @endif
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-2 text-right">
        {!! Form::label('state_id', "* Estado") !!}
      </div>
      <div class="col-sm-3">
        <div class='form-group @if ($errors->has('state_id')) has-error @endif'>
          {!! Form::select('state_id', $states,  $user->city->state->id, array('placeholder' => 'Selecione o estado...', 'class' => 'form-control state-selector')) !!}
          @if ($errors->has('state_id')) <p class="help-block">{{ $errors->first('state_id') }}</p> @endif
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-2 text-right">
        {!! Form::label('city_id', "* Município") !!}
      </div>
      <div class="col-sm-3">
        <div class='form-group @if ($errors->has('city_id')) has-error @endif'>
          {!! Form::select('city_id', $user->city->state->citiesOrdered->lists('name','id'),  null, array('placeholder' => 'Selecione o município...', 'class' => 'form-control city-selector')) !!}
          @if ($errors->has('city_id')) <p class="help-block">{{ $errors->first('city_id') }}</p> @endif
        </div>
      </div>
    </div>

    @if($user->role_id == OBSMA\Role::TEACHER)
      <div class="row">
        <div class="col-sm-2 text-right">
          {!! Form::label('phone_number', "* Telefone") !!}
        </div>
        <div class="col-sm-3">
          <div class='form-group @if ($errors->has('userable.phone_number')) has-error @endif'>
            {!! Form::text('userable[phone_number]',$user->userable->phone_number,array('class' => 'form-control phone_number')) !!}
            @if ($errors->has('userable.phone_number')) <p class="help-block">{{ $errors->first('userable.phone_number') }}</p> @endif
          </div>
        </div>

        <div class="col-sm-2 text-right">
          {!! Form::label('mobile_phone_number', "Celular") !!}
        </div>
        <div class="col-sm-3">
          <div class='form-group @if ($errors->has('userable.mobile_phone_number')) has-error @endif'>
            {!! Form::text('userable[mobile_phone_number]',$user->userable->mobile_phone_number,array('class' => 'form-control cellphone_number')) !!}
            @if ($errors->has('userable.mobile_phone_number')) <p class="help-block">{{ $errors->first('userable.mobile_phone_number') }}</p> @endif
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-2 text-right">
          {!! Form::label('address', "* Logradouro") !!}
        </div>
        <div class="col-sm-4">
          <div class='form-group @if ($errors->has('userable.address')) has-error @endif'>
            {!! Form::text('userable[address]', $user->userable->address, array('class' => 'form-control')) !!}
            @if ($errors->has('userable.address')) <p class="help-block">{{ $errors->first('userable.address') }}</p> @endif
          </div>
        </div>

        <div class="col-sm-2 text-right">
          {!! Form::label('complement', "Complemento") !!}
        </div>
        <div class="col-sm-2">
          <div class='form-group @if ($errors->has('userable.complement')) has-error @endif'>
            {!! Form::text('userable[complement]', $user->userable->complement, array('class' => 'form-control')) !!}
            @if ($errors->has('userable.complement')) <p class="help-block">{{ $errors->first('userable.complement') }}</p> @endif
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-2 text-right">
          {!! Form::label('nighborhood', "* Bairro") !!}
        </div>
        <div class="col-sm-3">
          <div class='form-group @if ($errors->has('userable.nighborhood')) has-error @endif'>
            {!! Form::text('userable[neighborhood]', $user->userable->neighborhood, array('class' => 'form-control')) !!}
            @if ($errors->has('userable.neighborhood')) <p class="help-block">{{ $errors->first('userable.neighborhood') }}</p> @endif
          </div>
        </div>

        <div class="col-sm-2 text-right">
          {!! Form::label('zip_code', "* CEP") !!}
        </div>
        <div class="col-sm-3">
          <div class='form-group @if ($errors->has('userable.zip_code')) has-error @endif'>
            {!! Form::text('userable[zip_code]', $user->userable->zip_code, array('class' => 'form-control zip_code')) !!}
            @if ($errors->has('userable.zip_code')) <p class="help-block">{{ $errors->first('userable.zip_code') }}</p> @endif
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-2 text-right">
          {!! Form::label('birthdate', "* Data de Nascimento") !!}
        </div>
        <div class="col-sm-3">
          <div class='form-group @if ($errors->has('userable.birthdate')) has-error @endif'>
            {!! Form::text('userable[birthdate]', $user->userable->birthdate->format('d/m/Y'), array('class' => 'form-control date')) !!}
            @if ($errors->has('userable.birthdate')) <p class="help-block">{{ $errors->first('userable.birthdate') }}</p> @endif
          </div>
        </div>

        <div class="col-sm-2 text-right">
          {!! Form::label('gender', "* Gênero") !!}
        </div>
        <div class="col-sm-3">
          <div class='form-group @if ($errors->has('userable.gender')) has-error @endif'>
            {!! Form::select('userable[gender]', [1 => 'Masculino', 2 => 'Feminino', 3 => 'Outro'], $user->userable->gender , array('placeholder' => 'Selecione o gênero...','class' => 'form-control gender-selector')) !!}
            @if ($errors->has('userable.gender')) <p class="help-block">{{ $errors->first('userable.gender') }}</p> @endif
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-sm-2 text-right">
          {!! Form::label('race', "*Cor / Raça") !!}
        </div>
        <div class="col-sm-3">
          <div class='form-group @if ($errors->has('userable.race')) has-error @endif'>
            {!! Form::select('userable[race]', [ 1 => 'Brancos',
                                      2 => 'Pardos',
                                      3 => 'Pretos',
                                      4 => 'Amarelos',
                                      5 => 'Indígenas'],  $user->userable->race, array('placeholder' => 'Selecione a cor / raça...', 'class' => 'form-control')) !!}
            @if ($errors->has('userable.race')) <p class="help-block">{{ $errors->first('userable.race') }}</p> @endif
          </div>
        </div>
        
        <div id='other-gender-wrapper' class='@if ($user->userable->gender == 3) visible @endif'>
          <div class="col-sm-2 text-right">
            {!! Form::label('other_gender', "* Genêro") !!}
          </div>
          <div class="col-sm-3">
            <div class='form-group @if ($errors->has('userable.other_gender')) has-error @endif'>
              {!! Form::text('userable[other_gender]', $user->userable->other_gender, array('class' => 'form-control')) !!}
              @if ($errors->has('userable.other_gender')) <p class="help-block">{{ $errors->first('userable.other_gender') }}</p> @endif
            </div>
          </div> 
        </div> 
      </div>

      <div class="row">
        <div class="col-sm-2 text-right">
          {!! Form::label('graduation_type', "* Formação") !!}
        </div>
        <div class="col-sm-3">
          <div class='form-group @if ($errors->has('userable.graduation_type')) has-error @endif'>
            {!! Form::select('userable[graduation_type]', [1 => 'Superior Completo', 2 => 'Superior Incompleto'],  $user->userable->graduation_type, array('placeholder' => 'Selecione a formação...', 'class' => 'form-control')) !!}
            @if ($errors->has('userable.graduation_type')) <p class="help-block">{{ $errors->first('userable.graduation_type') }}</p> @endif
          </div>
        </div>
        <div class="col-sm-2 text-right">
          {!! Form::label('academic_qualification_id', "Titulação") !!}
        </div>
        <div class="col-sm-3">
          <div class='form-group @if ($errors->has('userable.academic_qualification_id')) has-error @endif'>
            {!! Form::select('userable[academic_qualification_id]', $academicQualifications,  $user->userable->academic_qualification_id, array('placeholder' => 'Selecione a titulação...', 'class' => 'form-control')) !!}
            @if ($errors->has('userable.academic_qualification_id')) <p class="help-block">{{ $errors->first('userable.academic_qualification_id') }}</p> @endif
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-2 text-right">
          {!! Form::label('graduation_course', "Curso de Formação") !!}
        </div>
        <div class="col-sm-4">
          <div class='form-group @if ($errors->has('userable.graduation_course')) has-error @endif'>
            {!! Form::text('userable[graduation_course]', $user->userable->graduation_course, array('class' => 'form-control')) !!}
            @if ($errors->has('userable.graduation_course')) <p class="help-block">{{ $errors->first('userable.graduation_course') }}</p> @endif
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-2">
          {!! Form::label('subjects_list', "*Disciplinas") !!}
        </div>
        <div class="col-sm-6">
          <div class='form-group @if ($errors->has('userable.subjects')) has-error @endif'>
            {!! Form::select('userable[subjects][]', $subjects ,$user->userable->subjects()->lists('id')->toArray(), array('multiple' => true, 'class' => 'form-control select-2', 'id' => 'subjects')) !!}
            @if ($errors->has('userable.subjects')) <p class="help-block">{{ $errors->first('userable.subjects') }}</p> @endif
          </div>
        </div>
      </div>
    @endif


    <div class="row">
      <div class="col-sm-2"></div>

      <div class="col-sm-6">
        {!! Form::submit('Atualizar', array('class' => 'btn green-button')) !!}
        {!! link_to('home', 'Voltar', array('class' => 'btn green-button')) !!}
      </div>
    </div>
  {!! Form::close() !!}
</div>
@endsection
