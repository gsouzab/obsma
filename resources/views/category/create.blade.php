@extends('layouts.internal')

@section('header_title')
Nova Categoria
@endsection

@section('content')
<div class="container-fluid">
  @include('category/form')
</div>
@endsection
