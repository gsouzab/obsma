@extends('layouts.internal')

@section('header_title')
Editar Categoria
@endsection

@section('content')
<div class="container-fluid">
  @include('category/form')
</div>
@endsection
