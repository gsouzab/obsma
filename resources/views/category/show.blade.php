@extends('layouts.internal')

@section('header_title')
Categoria
@endsection

@section('content')
<div class="container-fluid">
  <fieldset>
    <legend>{{$category->name}}</legend>

    <div class="row">
      <div class="col-sm-12">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Anos</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($category->yearsPaginated as $year)
              <tr>
                <td>{{ $year->name }}</td>
              </tr>
            @empty
              <tr>
                <td>Nenhumo ano cadastrada</td>
              </tr>
            @endforelse
          </tbody>
        </table>
        {!! $category->yearsPaginated->render() !!}
      </div>
    </div>
  </fieldset>

  {!! link_to('category', "Voltar", array('class' => 'btn btn-default')) !!}
  {!! link_to_route('category.edit', "Editar", array('id' => $category->id), array('class' => 'btn btn-default')) !!}
</div>
@endsection
