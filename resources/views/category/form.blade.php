{!! Form::model($category, array('route' => ($category->exists ? array('category.update', $category->id) : 'category.store'),'method' => ($category->exists ? 'PUT' : 'POST')) )!!}
  <div class="row">
    <div class="col-sm-6">
      <div class='form-group @if ($errors->has('name')) has-error @endif'>
        {!! Form::label('name', "* Nome") !!}
        {!! Form::text('name', null,array('class' => 'form-control')) !!}
        @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
      </div>
    </div>
  </div>

  {!! Form::submit('Salvar', array('class' => 'btn btn-primary')) !!}
  {!! link_to('category', "Voltar", array('class' => 'btn btn-default')) !!}
{!! Form::close() !!}
