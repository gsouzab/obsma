@extends('layouts.internal')

@section('header_title')
Categorias
@endsection

@section('content')
<div class="container-fluid">
  {!! link_to('category/create', "Novo", array('class' => 'btn btn-primary')) !!}
  <br>
  <br>

  <div class="row">
    <div class="col-xs-12 text-right">
      {!! "Mostrando <b>" . $categories->count() . "</b> de <b>" . $categories->total() . "</b> resultados" !!}
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Nome</th>
            <th>Ações</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($categories as $category)
            <tr>
              <td>{{ $category->name }}</td>
              <td>
                <a class="btn btn-xs btn-default" href="{{url('category',[$category])}}"> <i class="fa fa-eye"> </i> Ver </a>
                <a class="btn btn-xs btn-default" href="{{route('category.edit',[$category])}}"> <i class="fa fa-pencil"> </i> Editar </a>
              </td>
            </tr>
          @empty
            <tr>
              <td colspan="2">Nenhuma categoria cadastrada</td>
            </tr>
          @endforelse
        </tbody>
      </table>
    </div>
  </div>
  {!! $categories->render() !!}
</div>
@endsection
