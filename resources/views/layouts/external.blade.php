<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>OBSMA - Olimipíada Brasileira de Saúde e Meio Ambiente</title>

        <!-- CSS And JavaScript -->
        <link href="/css/app.css" rel="stylesheet" type="text/css">
        <link href="/css/select2.min.css" rel="stylesheet" type="text/css">
        <script src="/js/all.js"> </script>
        <script src="/js/app.js" ></script>
    </head>

    <body>
      <div id="barra-brasil"></div>
      <!--inicio barra-->
      <script defer="defer" async="async" src="//barra.brasil.gov.br/barra.js" type="text/javascript"></script>
      <!--fim barra-->
      <div id="page-wrapper">
        @include('layouts.header')
        @include('helpers.messages')
        <div class="container">
          <div class="content">
            @yield('content')
          </div>
        </div>
        @include('layouts.footer')
      </div>
    </body>
</html>
