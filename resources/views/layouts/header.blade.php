<div class="header">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="logo">
          <a target="_blank" href="http://www.olimpiada.fiocruz.br">
            <img src="/img/logo.png">
          </a>
        </div>
        <div class="titulo hidden-xs">
          <h1>@yield('header_title')</h1>
          <p>{{ Auth::guest() ? '' : Auth::user()->name }}</p>
        </div>
      </div>
    </div>
  </div>
</div>
