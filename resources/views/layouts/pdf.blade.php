<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>OBSMA - Olimipíada Brasileira de Saúde e Meio Ambiente</title>
        <!-- CSS And JavaScript -->

        <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="/css/summernote.min.css" rel="stylesheet" type="text/css">
        <link href="/css/select2.min.css" rel="stylesheet" type="text/css">
        <link href="/css/app.css" rel="stylesheet" type="text/css">
        <script src="/js/all.js"> </script>
        <script src="/js/app.js" ></script>
    </head>

    <body>
        <div class="container">
          <div class="content">
            @yield('content')
          </div>
        </div>
    </body>
</html>
