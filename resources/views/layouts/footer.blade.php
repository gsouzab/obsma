<div class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <h2>Realização</h2>
        <p><img src="/img/fiocruz.jpg"></p>
        <p>Vice-Presidência de Ensino, Informação e Comunicação (VPEIC)</p>
        <p>&nbsp;</p>
        <h2>Apoio</h2>
        <p><img src="/img/cnpq.jpg"></p>
      </div>
      <div class="col-md-4">
        <h2>Olimpíada Brasileira de Saúde e Meio Ambiente</h2>
        <p>E-mail: olimpiada@fiocruz.br</p>
        <p>Av. Brasil, 4365  Manguinhos</p>
        <p>Rio de Janeiro, Brasil</p>
        <p>CEP: 21040-360</p>
        <p>Fax/Tel.: +55 (21) 2560.8259</p>
        <h2>Redes Sociais</h2>
        <div>
          <a href="https://www.facebook.com/obsma"><img src="/img/facebook.png"></a>
          <a href="https://twitter.com/obsma"><img src="/img/twitter.png"></a>
          <a href="https://www.youtube.com/user/obsma"><img src="/img/youtube.png"></a>
          <a href="https://www.flickr.com/photos/obsma"><img src="/img/flickr.png"></a>
        </div>
      </div>
      <div class="col-md-4">
        <h2>Contato</h2>
        {!! Form::open(array('url' => 'mail/contact','method' => 'POST', 'id' => 'contact-form', 'data-remote' => 'true'))!!}
          <div class="form-group">
            {!! Form::text('name', null,array('placeholder' => 'Nome', 'id' => 'name')) !!}
          </div>
          <div class="form-group">
            {!! Form::text('email', null,array('placeholder' => 'Email' , 'id' => 'email')) !!}
          </div>
          <div class="form-group">
            {!! Form::text('subject', null,array('placeholder' => 'Assunto' , 'id' => 'subject')) !!}
          </div>
          <div class="form-group">
            {!! Form::textarea('message', null, array('placeholder' => 'Mensagem', 'rows' => 2 , 'id' => 'message')) !!}
          </div>
          {!! Form::submit('Confirma') !!}
        {!! Form::close() !!}
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row copyright">
      <div class="col-xs-12">
        <div class="container text-center">
          <p>Copyright 2013-2014 FIOCRUZ - Fundação Oswaldo Cruz - Todos os direitos reservados</p>
        </div>
      </div>
    </div>
  </div>
</div>
