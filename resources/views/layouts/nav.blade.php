<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{url('/')}}">OBSMA</a>
      </div>

      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Usuários <span class="caret"></span></a>
            <ul class="dropdown-menu">
              @if(Auth::user()->role_id == \OBSMA\Role::NATIONAL_COORDINATOR)
                <li>{!! link_to('user/create', "Novo usuário") !!}</li>
              @endif
              <li>{!! link_to('user', "Listar usuários") !!}</li>
            </ul>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Trabalhos <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li>{!! link_to('work/create', "Incluir trabalho") !!}</li>
              <li>{!! link_to('work', "Listar trabalhos") !!}</li>
            </ul>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Relatórios <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li>{!! link_to('report/works', "Trabalhos") !!}</li>
              <li>{!! link_to('report/teachers', "Professores") !!}</li>
            </ul>
          </li>
          <li><a href="{{url('email')}}">Enviar emails</a></li>

          @if(Auth::user()->role_id == \OBSMA\Role::NATIONAL_COORDINATOR)
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Configurações <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li>{!! link_to('edition', "Edições") !!}</li>
                <li>{!! link_to('category', "Categorias") !!}</li>
                <li>{!! link_to('category_year', "Anos") !!}</li>
                <li role="separator" class="divider"></li>
                <li>{!! link_to('state', "Estados") !!}</li>
                <li>{!! link_to('city', "Municípios") !!}</li>
                <li>{!! link_to('regional', "Regionais") !!}</li>
                <li role="separator" class="divider"></li>
                <li>{!! link_to('academic_qualification', "Titulações") !!}</li>
                <li>{!! link_to('subject', "Disciplinas") !!}</li>
                <li>{!! link_to('school', "Escolas") !!}</li>
              </ul>
            </li>
          @endif
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{!! Auth::user()->name !!} <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li>{!! link_to('profile', "Alterar Perfil") !!}</li>
              <li>{!! link_to('profile/password', "Alterar Senha") !!}</li>
              <li role="separator" class="divider"></li>
              <li >{!! link_to('auth/logout', "Sair") !!}</li>
            </ul>
          </li>
        </ul>
      </div>
  </div>
</nav>
