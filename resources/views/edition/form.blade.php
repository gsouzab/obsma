{!! Form::model($edition, ['method' => $edition->exists ? 'PATCH' : 'POST','route' => $edition->exists ? ['edition.update', $edition->id] : 'edition.store','class' => 'form-horizontal']) !!}

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
  {!! Form::label('name', '*Nome: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group {{ $errors->has('year') ? 'has-error' : ''}}">
  {!! Form::label('year', '*Ano: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::text('year', $edition->year ? $edition->year->format('Y') : null, ['class' => 'form-control']) !!}
    {!! $errors->first('year', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group {{ $errors->has('start_date') ? 'has-error' : ''}}">
  {!! Form::label('start_date', '*Data de início: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::text('start_date',$edition->start_date ? $edition->start_date->format('d/m/Y') : null, ['class' => 'form-control date']) !!}
    {!! $errors->first('start_date', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="form-group {{ $errors->has('end_date') ? 'has-error' : ''}}">
  {!! Form::label('end_date', '*Data de fim: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::text('end_date', $edition->end_date ? $edition->end_date->format('d/m/Y') : null, ['class' => 'form-control date']) !!}
    {!! $errors->first('end_date', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group">
  <div class="col-sm-offset-3 col-sm-3">
    {!! Form::submit('Salvar', array('class' => 'btn btn-primary')) !!}
    {!! link_to('edition', "Voltar", array('class' => 'btn btn-default')) !!}
  </div>
</div>
{!! Form::close() !!}