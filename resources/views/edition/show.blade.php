@extends('layouts.internal')

@section('header_title')
    Edição
@endsection

@section('content')
    <div class="container-fluid">
        <fieldset>
            <legend>{{$edition->name}}</legend>
            <div class="row">
                <div class="col-sm-6">
                    <p><strong>Ano:</strong> {{$edition->year->format('Y')}} </p>
                    <p><strong>Data de início:</strong> {{$edition->start_date->format('d/m/Y')}} </p>
                    <p><strong>Data de fim:</strong> {{$edition->end_date->format('d/m/Y')}} </p>
                </div>
            </div>

            {{--<div class="row">--}}
                {{--<div class="col-sm-12">--}}
                    {{--<table class="table table-hover">--}}
                        {{--<thead>--}}
                        {{--<tr>--}}
                            {{--<th>Trabalhos</th>--}}
                        {{--</tr>--}}
                        {{--</thead>--}}
                        {{--<tbody>--}}
                        {{--@forelse ($edition->cities_paginated as $city)--}}
                            {{--<tr>--}}
                                {{--<td>{{ $city->name }}</td>--}}
                            {{--</tr>--}}
                        {{--@empty--}}
                            {{--<tr>--}}
                                {{--<td>Nenhuma cidade cadastrada</td>--}}
                            {{--</tr>--}}
                        {{--@endforelse--}}
                        {{--</tbody>--}}
                    {{--</table>--}}
                    {{--{!! $edition->cities_paginated->render() !!}--}}
                {{--</div>--}}
            {{--</div>--}}
        </fieldset>

        {!! link_to('edition', "Voltar", array('class' => 'btn btn-default')) !!}
        {!! link_to_route('edition.edit', "Editar", array('id' => $edition->id), array('class' => 'btn btn-default')) !!}
    </div>
@endsection
