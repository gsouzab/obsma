@extends('layouts.internal')

@section('header_title')
    Nova Edição
@endsection

@section('content')
    <div class="container-fluid">
        @include('edition/form')
    </div>
@endsection