@extends('layouts.internal')

@section('header_title')
Edições
@endsection

@section('content')
<div class="container-fluid">

  {!! link_to('edition/create', "Novo", array('class' => 'btn btn-primary')) !!}
  <br>
  <br>

  <div class="row">
    <div class="col-xs-12 text-right">
      {!! "Mostrando <b>" . $editions->count() . "</b> de <b>" . $editions->total() . "</b> resultados" !!}
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <table class="table table-hover">
        <thead>
          <tr>
            <th></th>
            <th>Nome</th>
            <th>Ano</th>
            <th>Data de início</th>
            <th>Data de fim</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($editions as $edition)
            <tr>
              <td>{!! $edition->is_active ? "<i class='fa fa-check text-success'></i>" : "<i class='fa fa-times text-danger'></i>" !!} </td>
              <td>{{ $edition->name }}</td>
              <td>{{ $edition->year->format('Y') }}</td>
              <td>{{ $edition->start_date->format('d/m/Y') }}</td>
              <td>{{ $edition->end_date->format('d/m/Y') }}</td>
              <td>
                <a class="btn btn-xs btn-default" href="{{url('edition',[$edition])}}"> <i class="fa fa-eye"> </i> Ver </a>
                <a class="btn btn-xs btn-default" href="{{route('edition.edit',[$edition])}}"> <i class="fa fa-pencil"> </i> Editar </a>
                @if (!$edition->is_active) <a class="btn btn-xs btn-success" href="{{route('edition.activate',[$edition])}}"> Ativar </a> @endif
              </td>
            </tr>
          @empty
            <tr>
              <td colspan="2">Nenhuma edição cadastrada</td>
            </tr>
          @endforelse
        </tbody>
      </table>
    </div>
  </div>
  {!! $editions->render() !!}

  {!! link_to('/', "Voltar", array('class' => 'btn btn-default')) !!}

</div>
@endsection
