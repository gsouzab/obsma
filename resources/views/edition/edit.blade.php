@extends('layouts.internal')

@section('header_title')
    Editar Edição
@endsection

@section('content')
    <div class="container-fluid">
        @include('edition/form')
    </div>
@endsection