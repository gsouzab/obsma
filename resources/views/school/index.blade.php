@extends('layouts.internal')

@section('header_title')
Escolas
@endsection

@section('content')
<div class="container-fluid">

  <div class="row">
    <div class="col-sm-12">
      <div class="well">
        {!! Form::open(['method' => 'get', 'url' => 'school']) !!}

        <div class="row">
          <div class="col-sm-4">
            <div class="form-group">
              {!! Form::label('s_name', 'Nome: ', ['class' => 'control-label']) !!}
              {!! Form::text('s_name', old('s_name'), ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              {!! Form::label('s_code', 'Código INEP: ', ['class' => 'control-label']) !!}
              {!! Form::text('s_code', old('s_code'), ['class' => 'form-control', 'placeholder' => 'Código INEP']) !!}
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            {!! Form::button("<i class='fa fa-search'></i> Pesquisar", ['id' => 'search', 'class' => 'btn btn-primary btn-lg']) !!}

            {!! link_to('school',"Todos", ['class' => 'btn btn-default btn-lg'])  !!}
          </div>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>

  {!! link_to('school/create', "Novo", array('class' => 'btn btn-primary')) !!}
  <br>
  <br>

  <div class="row">
    <div class="col-xs-12 text-right">
      {!! "Mostrando <b>" . $schools->count() . "</b> de <b>" . $schools->total() . "</b> resultados" !!}
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Código INEP</th>
            <th>Ano</th>
            <th>Nome</th>
            <th>Estado</th>
            <th>Cidade</th>
            <th>Telefone</th>
            {{--<th>Ações</th>--}}
          </tr>
        </thead>
        <tbody>
          @forelse ($schools as $school)
            <tr>
              <td>{{ $school->code }}</td>
              <td>{{ $school->year->format('Y') }}</td>
              <td>{{ $school->name }}</td>
              <td>{{ $school->state->name }}</td>
              <td>{{ $school->city->name }}</td>
              <td>{{ $school->phone_number }}</td>
              <td>
                {{--<a class="btn btn-xs btn-default" href="{{url('school',[$school])}}"> <i class="fa fa-eye"> </i> Ver </a>--}}
                {{--<a class="btn btn-xs btn-default" href="{{route('school.edit',[$school])}}"> <i class="fa fa-pencil"> </i> Editar </a>--}}
                {{-- <a class="btn btn-xs btn-default" href="{{url('school',[$school])}}"> <i class="fa fa-eye"> </i> Ver </a> --}}
              </td>
            </tr>
          @empty
            <tr>
              <td colspan="2">Nenhuma escola cadastrada</td>
            </tr>
          @endforelse
        </tbody>
      </table>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      {!! $schools->appends(Input::except('page'))->render() !!}
    </div>
  </div>

  {!! link_to('/', "Voltar", array('class' => 'btn btn-default')) !!}

</div>
@endsection
