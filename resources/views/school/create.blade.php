@extends('layouts.internal')

@section('header_title')
Nova Escola
@endsection

@section('content')
<div class="container-fluid">
  @include('school/form')
</div>
@endsection
