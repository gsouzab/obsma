@extends('layouts.internal')

@section('header_title')
Editar Escola
@endsection

@section('content')
<div class="container-fluid">
  @include('school/form')
</div>
@endsection
