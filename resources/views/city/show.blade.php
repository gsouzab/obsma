@extends('layouts.internal')

@section('header_title')
Município
@endsection

@section('content')
<div class="container-fluid">
  <fieldset>
    <legend>{{$city->name}}</legend>
    <div class="row">
      <div class="col-sm-6">
        <p><strong>Estado:</strong> {{$city->state->name}} </p>
      </div>
    </div>
  </fieldset>

  {!! link_to('city', "Voltar", array('class' => 'btn btn-default')) !!}
  {!! link_to_route('city.edit', "Editar", array('id' => $city->id), array('class' => 'btn btn-default')) !!}
</div>
@endsection
