{!! Form::model($city, array('route' => ($city->exists ? array('city.update', $city->id) : 'city.store'),'method' => ($city->exists ? 'PUT' : 'POST')) )!!}
  <div class="row">

    <div class="col-sm-6">
      <div class='form-group @if ($errors->has('state_id')) has-error @endif'>
        {!! Form::label('state_id', "* Estado") !!}
        {!! Form::select('state_id', $states,  null, array('placeholder' => 'Selecione o estado...', 'class' => 'form-control state-selector')) !!}
        @if ($errors->has('state_id')) <p class="help-block">{{ $errors->first('state_id') }}</p> @endif
      </div>

      <div class='form-group @if ($errors->has('name')) has-error @endif'>
        {!! Form::label('name', "* Nome") !!}
        {!! Form::text('name', null,array('class' => 'form-control')) !!}
        @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
      </div>


    </div>
  </div>

  {!! Form::submit('Salvar', array('class' => 'btn btn-primary')) !!}
  {!! link_to('city', "Voltar", array('class' => 'btn btn-default')) !!}
{!! Form::close() !!}
