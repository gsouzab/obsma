@extends('layouts.internal')

@section('header_title')
Municípios
@endsection

@section('content')
<div class="container-fluid">
  {!! link_to('city/create', "Novo", array('class' => 'btn btn-primary')) !!}
  <br>
  <br>

  <div class="row">
    <div class="col-xs-12 text-right">
      {!! "Mostrando <b>" . $cities->count() . "</b> de <b>" . $cities->total() . "</b> resultados" !!}
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Nome</th>
            <th>Estado</th>
            <th>Ações</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($cities as $city)
            <tr>
              <td>{{ $city->name }}</td>
              <td>{{ $city->state->name }}</td>
              <td>
                <a class="btn btn-xs btn-default" href="{{url('city',[$city])}}"> <i class="fa fa-eye"> </i> Ver </a>
                <a class="btn btn-xs btn-default" href="{{route('city.edit',[$city])}}"> <i class="fa fa-pencil"> </i> Editar </a>
              </td>
            </tr>
          @empty
            <tr>
              <td colspan="2">Nenhuma cidade cadastrada</td>
            </tr>
          @endforelse
        </tbody>
      </table>
    </div>
  </div>
  {!! $cities->render() !!}
</div>
@endsection
