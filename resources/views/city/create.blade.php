@extends('layouts.internal')

@section('header_title')
Novo Município
@endsection

@section('content')
<div class="container-fluid">
  @include('city/form')
</div>
@endsection
