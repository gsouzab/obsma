<p>Prezado Usuário {!! $user->name !!},</p>
<p>Seu perfil foi atualizado com sucesso!</p>
<p>OLIMPÍADA BRASILEIRA DE SAÚDE E MEIO AMBIENTE / FUNDAÇÃO OSWALDO CRUZ</p>
<br>
<p>Mensagem enviada através do sistema OBSMA - {{date('d/m/Y \à\s H:i:s ')}}</p>