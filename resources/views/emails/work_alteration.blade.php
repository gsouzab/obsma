<p>Prezado Professor,</p>
<p>O trabalho {!! $work->title !!} foi alterado com sucesso!</p>
<p>Atenção: o material físico relativo ao seu trabalho deve ser encaminhado à sua Regional até o dia {!! $activeEdition->end_date->format('d/m/Y') !!} (o selo dos Correios vale como comprovante).</p>
<p>Confira o endereço no <a href="http://www.olimpiada.fiocruz.br/regionais">www.olimpiada.fiocruz.br/regionais</a></p>
<p>BOA SORTE!</p>
<p>OLIMPÍADA BRASILEIRA DE SAÚDE E MEIO AMBIENTE / FUNDAÇÃO OSWALDO CRUZ</p>
<br>
<p>Mensagem enviada através do sistema OBSMA - {{date('d/m/Y \à\s H:i:s ')}}</p>