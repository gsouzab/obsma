<p>Prezado Professor</p>
<p>{!! $user->name !!},</p>
<p>Seu cadastro foi realizado com sucesso! Inscreva seu trabalho em <a href="http://www.olimpiada2.fiocruz.br">www.olimpiada2.fiocruz.br</a>.</p>
<p>OLIMPÍADA BRASILEIRA DE SAÚDE E MEIO AMBIENTE / FUNDAÇÃO OSWALDO CRUZ</p>
<br>
<p>Mensagem enviada através do sistema OBSMA - {{date('d/m/Y \à\s H:i:s ')}}</p>