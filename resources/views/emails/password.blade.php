<p>Você solicitou a recuperação de senha.</p>

<p>Clique no link para resetar sua senha: {{ url('password/reset/'.$token) }}</p>
<p>Mensagem enviada através do sistema OBSMA - {{date('d/m/Y \à\s H:i:s ')}}</p>