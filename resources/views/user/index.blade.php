@extends('layouts.internal')

@section('header_title')
Usuários
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="well">
        {!! Form::open(['method' => 'get', 'url' => 'user']) !!}
        {!! Form::hidden('_excel',null, ['id' => "_excel"]) !!}

        {!! Form::hidden('o_name', old('o_name'), ['id' => "o_name", 'class' => 'sort-field']) !!}
        {!! Form::hidden('o_created_at', old('o_created_at'), ['id' => "o_created_at", 'class' => 'sort-field']) !!}

        <div class="row">
          <div class="col-sm-4">
            <div class="form-group">
              {!! Form::label('s_name', 'Nome: ', ['class' => 'control-label']) !!}
              {!! Form::text('s_name', old('s_name'), ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              {!! Form::label('s_cpf', 'CPF: ', ['class' => 'control-label']) !!}
              {!! Form::text('s_cpf', old('s_cpf'), ['class' => 'form-control cpf', 'placeholder' => 'CPF']) !!}
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              {!! Form::label('s_email', 'Email: ', ['class' => 'control-label']) !!}
              {!! Form::text('s_email', old('s_email'), ['class' => 'form-control', 'placeholder' => 'Email']) !!}
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-4">
            <div class="form-group">
              {!! Form::label('s_role_id', 'Perfil: ', ['class' => 'control-label']) !!}
              {!! Form::select('s_role_id',\OBSMA\Role::lists('name','id'), old('s_role_id'), ['class' => 'form-control', 'placeholder' => 'Selecione o perfil']) !!}
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-12">
            {!! Form::button("<i class='fa fa-search'></i> Pesquisar", ['id' => 'search', 'class' => 'btn btn-primary btn-lg']) !!}

            {!! Form::button("<i class='fa fa-file-excel-o'></i> Exportar", ['id' => 'export-works', 'class' => 'btn btn-default btn-lg'])  !!}

            {!! link_to('user',"Todos", ['class' => 'btn btn-default btn-lg'])  !!}
          </div>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>

  @if(Auth::user()->role_id == \OBSMA\Role::NATIONAL_COORDINATOR)
    {!! link_to('user/create', "Novo", array('class' => 'btn btn-primary')) !!}
  @endif
  <br>
  <br>

  <div class="row">
    <div class="col-xs-12 text-right">
      {!! "Mostrando <b>" . $users->count() . "</b> de <b>" . $users->total() . "</b> resultados" !!}
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>CPF</th>
            <th><a class="sort {{ old('o_name') }}" href="#" id="sort-o_name"><strong>Nome</strong></a></th>
            <th>Email</th>
            <th>Perfil</th>
            <th><a class="sort {{ old('o_created_at') }}" href="#" id="sort-o_created_at"><strong>Dt. Criação</strong></a></th>
            <th>Ações</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($users as $user)
            <tr>
              <td>{{ $user->cpf }}</td>
              <td>{{ $user->name }}</td>
              <td>{{ $user->email }}</td>              
              <td>{{ $user->role->name }}</td>
              <td>{{ $user->created_at->format('d/m/Y \à\s H:i:s') }}</td>
              <td>
                <a class="btn btn-xs btn-default" href="{{url('user',[$user])}}"> <i class="fa fa-eye"> </i> Ver </a>
                @if(Auth::user()->role_id == \OBSMA\Role::NATIONAL_COORDINATOR)
                  <a class="btn btn-xs btn-default" href="{{route('user.edit',[$user])}}"> <i class="fa fa-pencil"> </i> Editar </a>
                  @if ($user->is_blocked)
                    <a class="btn btn-xs btn-success" href="{{route('user.unblock',[$user])}}"> Desbloquear </a>
                  @else
                    <a class="btn btn-xs btn-danger" href="{{route('user.block',[$user])}}"> Bloquear </a>
                  @endif
                @endif
              </td>
            </tr>
          @empty
            <tr>
              <td colspan="2">Nenhum usuário cadastrado</td>
            </tr>
          @endforelse
        </tbody>
      </table>
    </div>
  </div>
  {!! $users->appends(Input::except('page'))->render() !!}
</div>
@endsection
