@extends('layouts.internal')

@section('header_title')
Editar Usuário
@endsection

@section('content')
<div class="container-fluid">
  @include('user/form')
</div>
@endsection
