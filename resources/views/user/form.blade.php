{!! Form::model($user, array('route' => ($user->exists ? array('user.update', $user->id) : 'user.store'),'method' => ($user->exists ? 'PUT' : 'POST'),'class' => 'form-horizontal') )!!}

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
  {!! Form::label('name', '*Nome: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('cpf') ? 'has-error' : ''}}">
  {!! Form::label('cpf', '*CPF: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::text('cpf', null, ['class' => 'form-control cpf']) !!}
    {!! $errors->first('cpf', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
  {!! Form::label('email', '*Email: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::text('email', null, ['class' => 'form-control email']) !!}
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('state_id') ? 'has-error' : ''}}">
  {!! Form::label('state_id', '*Estado: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::select('state_id', $states,  null, array('placeholder' => 'Selecione o estado...', 'class' => 'form-control state-selector')) !!}
    {!! $errors->first('state_id', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('city_id') ? 'has-error' : ''}}">
  {!! Form::label('city_id', '*Cidade: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::select('city_id', old('state_id') == null ? $cities : OBSMA\State::find(old('state_id'))->citiesOrdered->lists('name','id'),  null, array('placeholder' => 'Selecione a cidade...', 'class' => 'form-control city-selector')) !!}
    {!! $errors->first('city_id', '<p class="help-block">:message</p>') !!}
  </div>
</div>


<div class="form-group {{ $errors->has('role_id') ? 'has-error' : ''}}">
  {!! Form::label('role_id', '*Perfil de acesso: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::select('role_id', $roles,  null, array('placeholder' => 'Selecione o perfil...', 'class' => 'form-control')) !!}
    {!! $errors->first('role_id', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
  {!! Form::label('password', '*Senha: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::password('password',array('class' => 'form-control')) !!}
    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : ''}}">
  {!! Form::label('password_confirmation', '*Confirmação de Senha: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::password('password_confirmation',array('class' => 'form-control')) !!}
    {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
  </div>
</div>


<div class="form-group">
  <div class="col-sm-offset-3 col-sm-3">
    {!! Form::submit('Salvar', array('class' => 'btn btn-primary')) !!}
    {!! link_to(URL::route('user.index', session('user_inputs')), "Voltar", array('class' => 'btn btn-default')) !!}
  </div>
</div>

{!! Form::close() !!}
