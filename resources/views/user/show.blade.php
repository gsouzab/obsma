@extends('layouts.internal')

@section('header_title')
Usuário
@endsection

@section('content')
<div class="container-fluid">
  <fieldset>
    <legend>{{$user->name}}</legend>
    <div class="row">
      @if ($user->role_id == \OBSMA\Role::TEACHER)
        <div class="col-sm-6">
          <p><strong>Formação:</strong> {{ $user->userable->graduationTypeLabel }} </p>
          <p><strong>Titulação:</strong> {{$user->userable->academicQualification !== null ? $user->userable->academicQualification->name : ''}} </p>
          <p><strong>Curso de Formação:</strong> {{$user->userable->graduation_course}} </p>
          <p><strong>Gênero:</strong> {{$user->userable->genderLabel}} </p>
          <p><strong>Cor / Raça:</strong> {{$user->userable->raceLabel}} </p>
          <p><strong>Data de nascimento:</strong> {{$user->userable->birthdate->format('d/m/Y')}} </p>
          <p><strong>Telefone:</strong> {{$user->userable->phone_number}} </p>
          <p><strong>Celular:</strong> {{$user->userable->mobile_phone_number}} </p>
          <p><strong>Endereço:</strong> {{$user->userable->address}} - {{$user->userable->complement}} </p>
          <p><strong>Bairro:</strong> {{$user->userable->neighborhood}} </p>
          <p><strong>CEP:</strong> {{$user->userable->zip_code}} </p>
        </div>
      @endif
      <div class="col-sm-6">
        <p><strong>Perfil:</strong> {{$user->role->name}} </p>
        <p><strong>CPF:</strong> {{$user->cpf}} </p>
        <p><strong>Email:</strong> {{$user->email}} </p>
        <p><strong>Estado:</strong> {{$user->city->state->name}} </p>
        <p><strong>Cidade:</strong> {{$user->city->name}} </p>
        <p><strong>Regional:</strong> {{$user->city->state->regional->name}} </p>
      </div>
    </div>
    @if ($user->role_id == \OBSMA\Role::TEACHER)
      <br>
      <h4>Disciplinas</h4>
      <table class="table table-hover">
        <tbody>
          @foreach ($user->userable->subjects as $subject)
            <tr>
              <td>{{ $subject->name }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    @endif

  </fieldset>

  {!! link_to(URL::route('user.index', session('user_inputs')), "Voltar", array('class' => 'btn btn-default')) !!}
  @if(Auth::user()->role_id == \OBSMA\Role::NATIONAL_COORDINATOR)
    {!! link_to_route('user.edit', "Editar", array('id' => $user->id), array('class' => 'btn btn-default')) !!}
  @endif
</div>
@endsection
