@extends('layouts.internal')

@section('header_title')
Titulação
@endsection

@section('content')
<div class="container-fluid">
  <fieldset>
    <legend>{{$academic_qualification->name}}</legend>
{{--
    <div class="row">
      <div class="col-sm-12">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Professores</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($academic_qualification->cities_paginated as $city)
              <tr>
                <td>{{ $city->name }}</td>
              </tr>
            @empty
              <tr>
                <td>Nenhuma cidade cadastrada</td>
              </tr>
            @endforelse
          </tbody>
        </table>
        {!! $academic_qualification->cities_paginated->render() !!}
      </div>
    </div> --}}
  </fieldset>

  {!! link_to('academic_qualification', "Voltar", array('class' => 'btn btn-default')) !!}
  {!! link_to_route('academic_qualification.edit', "Editar", array('id' => $academic_qualification->id), array('class' => 'btn btn-default')) !!}
</div>
@endsection
