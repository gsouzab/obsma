@extends('layouts.internal')

@section('header_title')
Editar Titulação
@endsection

@section('content')
<div class="container-fluid">
  @include('academic_qualification/form')
</div>
@endsection
