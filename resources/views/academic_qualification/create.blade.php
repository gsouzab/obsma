@extends('layouts.internal')

@section('header_title')
Nova Titulação
@endsection

@section('content')
<div class="container-fluid">
  @include('academic_qualification/form')
</div>
@endsection
