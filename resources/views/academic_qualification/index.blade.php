@extends('layouts.internal')

@section('header_title')
Titulações
@endsection

@section('content')
<div class="container-fluid">
  {!! link_to('academic_qualification/create', "Novo", array('class' => 'btn btn-primary')) !!}
  <br>
  <br>

  <div class="row">
    <div class="col-xs-12 text-right">
      {!! "Mostrando <b>" . $academic_qualifications->count() . "</b> de <b>" . $academic_qualifications->total() . "</b> resultados" !!}
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Nome</th>
            <th>Ações</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($academic_qualifications as $academic_qualification)
            <tr>
              <td>{{ $academic_qualification->name }}</td>
              <td>
                <a class="btn btn-xs btn-default" href="{{url('academic_qualification',[$academic_qualification])}}"> <i class="fa fa-eye"> </i> Ver </a>
                <a class="btn btn-xs btn-default" href="{{route('academic_qualification.edit',[$academic_qualification])}}"> <i class="fa fa-pencil"> </i> Editar </a>
              </td>
            </tr>
          @empty
            <tr>
              <td colspan="2">Nenhuma titulação cadastrada</td>
            </tr>
          @endforelse
        </tbody>
      </table>
    </div>
  </div>
  {!! $academic_qualifications->render() !!}
</div>
@endsection
