@extends('layouts.internal')

@section('header_title')
Menu
@endsection

@section('content')
<div class="container-fluid">
  <div id="main-menu" class="row">
    <div class="main-menu-item col-md-2 col-xs-4 text-center">
      <a href="{{url('profile')}}">
        <img src="/img/icons/alterar_dados.jpg">
        <p >Alterar dados pessoais</p>
      </a>
    </div>
    @if(!$isClosed)
      <div class="main-menu-item col-md-2 col-xs-4 text-center">
        <a href="{{url('work/create')}}">
          <img src="/img/icons/novo_trabalho.jpg">
          <p >Novo trabalho</p>
        </a>
      </div>
    @endif
    <div class="main-menu-item col-md-2 col-xs-4 text-center">
      <a href="{{url('work')}}">
        <img src="/img/icons/pesquisar_arquivo.jpg">
        <p>Consultar trabalhos</p>
      </a>
    </div>
    @if($showCertificates)
      <div class="main-menu-item col-md-2 col-xs-4 text-center">
        <a href="{{url('certificate')}}">
          <img src="/img/icons/botao_certificado.jpg">
          <p>Certificados</p>
        </a>
      </div>
    @endif
    <div class="main-menu-item col-md-2 col-xs-4 text-center">
      <a href="{{url('profile/password')}}">
        <img src="/img/icons/alterar_senha.jpg">
        <p>Alterar senha</p>
      </a>
    </div>
    <div class="main-menu-item col-md-2 col-xs-4 text-center">
      <a href="{{url('auth/logout')}}">
        <img src="/img/icons/logout.jpg">
        <p>Sair</p>
      </a>
    </div>
  </div>
</div>
@endsection
