<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute deve ser aceito.',
    'active_url'           => ':attribute não é uma URL válida.',
    'after'                => ':attribute deve ser uma data depois de :date.',
    'alpha'                => ':attribute deve conter somente letras.',
    'alpha_dash'           => ':attribute deve conter letras, números e traços.',
    'alpha_num'            => ':attribute deve conter somente letras e números.',
    'array'                => ':attribute deve ser um array.',
    'before'               => ':attribute deve ser uma data antes de :date.',
    'between'              => [
        'numeric' => ':attribute deve estar entre :min e :max.',
        'file'    => ':attribute deve estar entre :min e :max kilobytes.',
        'string'  => ':attribute deve estar entre :min e :max caracteres.',
        'array'   => ':attribute deve ter entre :min e :max itens.',
    ],
    'boolean'              => ':attribute deve ser verdadeiro ou falso.',
    'confirmed'            => 'A confirmação de :attribute não confere.',
    'date'                 => ':attribute não é uma data válida.',
    'date_format'          => ':attribute não confere com o formato :format.',
    'different'            => ':attribute e :other devem ser diferentes.',
    'digits'               => ':attribute deve ter :digits dígitos.',
    'digits_between'       => ':attribute deve ter entre :min e :max dígitos.',
    'email'                => ':attribute deve ser um endereço de e-mail válido.',
    'filled'               => ':attribute é um campo obrigatório.',
    'exists'               => 'O :attribute selecionado é inválido.',
    'image'                => ':attribute deve ser uma imagem.',
    'in'                   => ':attribute é inválido.',
    'integer'              => ':attribute deve ser um inteiro.',
    'ip'                   => ':attribute deve ser um endereço IP válido.',
    'max'                  => [
        'numeric' => ':attribute não deve ser maior que :max.',
        'file'    => ':attribute não deve ter mais que :max kilobytes.',
        'string'  => ':attribute não deve ter mais que :max caracteres.',
        'array'   => ':attribute não pode ter mais que :max itens.',
    ],
    'mimes'                => ':attribute deve ser um arquivo do tipo: :values.',
    'min'                  => [
        'numeric' => ':attribute deve ser no mínimo :min.',
        'file'    => ':attribute deve ter no mínimo :min kilobytes.',
        'string'  => ':attribute deve ter no mínimo :min caracteres.',
        'array'   => ':attribute deve ter no mínimo :min itens.',
    ],
    'not_in'               => 'O :attribute selecionado é inválido.',
    'numeric'              => ':attribute deve ser um número.',
    'regex'                => 'O formato de :attribute é inválido.',
    'required'             => 'O campo :attribute é obrigatório.',
    'required_if'          => 'O campo :attribute é obrigatório quando :other é :value.',
    'required_with'        => 'O campo :attribute é obrigatório quando :values está presente.',
    'required_with_all'    => 'O campo :attribute é obrigatório quando :values estão presentes.',
    'required_without'     => 'O campo :attribute é obrigatório quando :values não está presente.',
    'required_without_all' => 'O campo :attribute é obrigatório quando nenhum destes estão presentes: :values.',
    'same'                 => ':attribute e :other devem ser iguais.',
    'size'                 => [
        'numeric' => ':attribute deve ser :size.',
        'file'    => ':attribute deve ter :size kilobytes.',
        'string'  => ':attribute deve ter :size caracteres.',
        'array'   => ':attribute deve conter :size itens.',
    ],
    'timezone'             => ':attribute deve ser uma timezone válida.',
    'unique'               => ':attribute já está em uso.',
    'url'                  => 'O formato de :attribute é inválido.',
    'cpf' => 'O formato de :attribute é inválido.',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'students' => [
            'min' => ':attribute deve ter no mínimo :min participantes',
        ],
        'title' => [
            'unique' => ':attribute do trabalho duplicado, favor verifique.'
        ],
        'userable' => [
            'other_gender' => [
                'required_if' => 'Descreva o gênero.'
            ],
        ],
        'other_gender' => [
            'required_if' => 'Descreva o gênero.'
        ],
        'fiocruz_fonts' => [
            'required_if' => 'Este campo é obrigatório.'
        ],
         
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'userable' => [
            'academic_qualification_id' => 'Titulação',
            'subjects' => 'Disciplinas',
            'phone_number' => 'Telefone',
            'mobile_phone_number' => 'Celular',
            'graduation_type' => 'Formação',
            'graduation_course' => 'Curso de Formação',
            'gender' => 'Gênero',
            'race' => 'Cor / Raça',
            'birthdate' => 'Data de Nascimento',
            'address' => 'Endereço',
            'neighborhood' => 'Bairro',
            'complement' => 'Complemento',
            'zip_code' => 'CEP',
            'other_gender' => 'Gênero',
        ],
        'academic_qualification_id' => 'Titulação',
        'subjects' => 'Disciplinas',
        'subject' => 'Assunto',
        'message' => 'Mensagem',
        'phone_number' => 'Telefone',
        'mobile_phone_number' => 'Celular',
        'graduation_type' => 'Formação',
        'graduation_course' => 'Curso de Formação',
        'gender' => 'Gênero',
        'other_gender' => 'Gênero',
        'race' => 'Cor / Raça',
        'birthdate' => 'Data de Nascimento',
        'cpf' => 'CPF',
        'password' => 'Senha',
        'current_password' => 'Senha atual',
        'name' => 'Nome',
        'place' => 'Local',
        'address' => 'Endereço',
        'neighborhood' => 'Bairro',
        'complement' => 'Complemento',
        'coordinator_name' => 'Coordenador',
        'role_id' => 'Perfil de acesso',
        'city_id' => 'Cidade',
        'state_id' => 'Estado',
        'zip_code' => 'CEP',
        'fax_number' => 'Fax',
        'email' => 'Email',
        'created_at' => 'Data de Criação',
        'updated_at' => 'Data de Atualização',
        'school_id' => 'Escola',
        'teacher_id' => 'Professor',
        'teachers_total' => 'Total de professores',
        'students_total' => 'Total de alunos',
        'category_id' => 'Categoria',
        'modality_id' => 'Modalidade',
        'participation_type_id' => 'Participação',
        'title' => 'Título',
        'abstract' => 'Resumo/Sinopse',
        'theme' => 'Temática abordada',
        'duration' => 'Tempo de duração',
        'students' => 'Alunos',
        'years' => 'Anos',
        'objective' => 'Objetivo',
        'theoretical_references' => 'Referencial Teórico',
        'methodology' => 'Metodologia',
        'results' => 'Resultado',
        'sent_material' => 'Materias Enviados',
        'number_of_pages' => 'Número de Páginas',
        'genre' => 'Gêneros',
        'techniques' => 'Técnicas',
        'agreed' => 'Li e concordo com o regulamento',
        'authentication_code' => 'Código de autenticação',
        'keywords' => 'Palavras-chave',
        'has_fiocuz_material' => 'acima'
    ],

];
