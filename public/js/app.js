(function() {
  $(function() {
    var cellphoneOptions, commonOptions, phoneOptions;
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $('.select-2').select2();
    $('.select-2-teachers').select2({
      ajax: {
        url: "/users/search-teachers",
        dataType: 'json',
        delay: 250,
        data: function(params) {
          return {
            q: params.term,
            page: params.page
          };
        },
        processResults: function(data, params) {
          params.page = params.page || 1;
          return {
            results: data.items,
            pagination: {
              more: (params.page * 30) < data.total_count
            }
          };
        },
        cache: true
      },
      minimumInputLength: 3
    });
    phoneOptions = {
      clearIfNotMatch: true,
      onKeyPress: function(number, e, field, options) {
        var mask, masks;
        masks = ['(00) 0000-00009', '(00) 00000-0000'];
        mask = masks[0];
        if (number.replace(/\D/g, '').length === 11) {
          mask = masks[1];
        }
        return $('.phone_number').mask(mask, options);
      }
    };
    cellphoneOptions = {
      clearIfNotMatch: true,
      onKeyPress: function(number, e, field, options) {
        var mask, masks;
        masks = ['(00) 0000-00009', '(00) 00000-0000'];
        mask = masks[0];
        if (number.replace(/\D/g, '').length === 11) {
          mask = masks[1];
        }
        return $('.cellphone_number').mask(mask, options);
      }
    };
    commonOptions = {
      clearIfNotMatch: true
    };
    $('.modality-wrapper').find('input').prop('disabled', true);
    $('.modality-wrapper').find('select').prop('disabled', true);
    $('.modality-wrapper').find('textarea').prop('disabled', true);
    $;
    $(".modality-selected").find('input').prop('disabled', false);
    $(".modality-selected").find('select').prop('disabled', false);
    $(".modality-selected").find('textarea').prop('disabled', false);
    $('.phone_number').mask('(00) 0000-00009', phoneOptions);
    $('.cellphone_number').mask('(00) 0000-00009', cellphoneOptions);
    $('.cpf').mask("000.000.000-00", commonOptions);
    $('.zip_code').mask("00000-000", commonOptions);
    $('.date').mask("00/00/0000", commonOptions);
    $(':not(.visible)#other-gender-wrapper').hide();
    $(':not(.visible)#other-area-wrapper').hide();
    $(':not(.visible)#fonts-wrapper').hide();
    $('.summernote').summernote({
      height: 300,
      lang: 'pt-BR',
      fontNames: ['Proxima Nova Light', 'Arial', 'Arial Black', 'Comic Sans MS', 'Helvetica', 'Helvetica Neue', 'Impact', 'Courier New', 'Lucida Grande', 'Merriweather', 'Times New Roman', 'Tahoma', 'Verdana'],
      callbacks: {
        onImageUpload: function(files) {
          return sendFile(files[0]);
        }
      },
      toolbar: [['style', ['style', 'bold', 'italic', 'underline', 'clear']], ['fontname', ['fontname']], ['fontsize', ['fontsize']], ['color', ['color']], ['para', ['ul', 'ol', 'paragraph']], ['height', ['height']], ['insert', ['link', 'picture', 'video']], ['text', ['undo', 'redo']], ['misc', ['fullscreen', 'codeview', 'help']]]
    });
    $('#contact-form').bind("ajax:success", function(e, data, status, xhr) {
      $(".form-group.has-error .help-block").remove();
      $(".has-error").toggleClass("has-error");
      $('#message').attr('placeholder', 'Mensagem enviada com sucesso');
      $('#name').val('');
      $('#email').val('');
      $('#subject').val('');
      return $('#message').val('');
    });
    $('#contact-form').bind("ajax:error", function(e, xhr, status, error) {
      var $email, $message, $name, $subject, response;
      $(".form-group.has-error .help-block").remove();
      $(".has-error").toggleClass("has-error");
      response = xhr.responseJSON;
      $name = $('#name');
      $email = $('#email');
      $subject = $('#subject');
      $message = $('#message');
      if (response.hasOwnProperty('name')) {
        appendError($name, response['name']);
      }
      if (response.hasOwnProperty('email')) {
        appendError($email, response['email']);
      }
      if (response.hasOwnProperty('subject')) {
        appendError($subject, response['subject']);
      }
      if (response.hasOwnProperty('message')) {
        return appendError($message, response['message']);
      }
    });
    $(".state-selector").change(function(e) {
      var $select, stateID;
      stateID = $(this).val();
      $select = $('.city-selector');
      return $.ajax({
        type: "GET",
        url: "/state/" + stateID + "/cities",
        success: function(data) {
          $select.empty().append("<option value=''>Selecione o município...</option>");
          return $.each(data.cities, function(key, value) {
            return $select.append('<option value=' + value.id + '>' + value.name + '</option>');
          });
        }
      }, "json");
    });
    $('.btn-find-school').click(function(e) {
      var $select, cityID, query, stateID;
      e.preventDefault();
      $select = $('.school-selector');
      stateID = $('.state-selector').val();
      cityID = $('.city-selector').val();
      query = $('.school-query').val();
      return $.ajax({
        type: "GET",
        url: "/school/find/" + stateID + "/" + cityID + "/" + query,
        success: function(data) {
          $select.empty().append("<option value=''>Selecione a escola...</option>");
          $.each(data.schools, function(key, value) {
            return $select.append('<option value=' + value.id + '>' + value.name + '</option>');
          });
          return $select.focus();
        }
      }, "json");
    });
    $('.btn-add-student').click(function(e) {
      var $studentBirthdate, $studentGender, $studentName, $table, index, newRow;
      e.preventDefault();
      $table = $('#students-list');
      $studentName = $('#student_name');
      $studentBirthdate = $('#student_birthdate');
      $studentGender = $('#student_gender :selected');
      if ($studentName.val() !== '' && $studentGender.val() !== '') {
        index = $table.find('tr').length;
        newRow = "<tr>\n  <td><input type='hidden' name=\"students[" + index + "][name]\" value=\"" + ($studentName.val()) + "\"> " + ($studentName.val()) + "</td>\n  <td><input type='hidden' name=\"students[" + index + "][birthdate]\" value=\"" + ($studentBirthdate.val()) + "\"> " + ($studentBirthdate.val()) + "</td>\n  <td><input type='hidden' name=\"students[" + index + "][gender]\" value=\"" + ($studentGender.val()) + "\"> " + ($studentGender.text()) + "</td>\n  <td><a class=\"btn btn-danger btn-xs btn-remove-student\">[-] Remover</a></td>\n</tr> ";
        $table.prepend(newRow);
        $studentName.val('');
        $studentBirthdate.val('');
        return $studentGender.removeAttr("selected");
      } else {
        return $studentName.attr('placeholder', 'Preencha o nome do aluno.');
      }
    });
    $(document).on('click', '.btn-remove-student', function(e) {
      e.preventDefault();
      return $(this).closest("tr").remove();
    });
    $(".school-selector").change(function(e) {
      var schoolID;
      schoolID = $(this).val();
      return $.ajax({
        type: "GET",
        url: "/school/" + schoolID + "/adm_dept",
        success: function(data) {
          $("#adm_dept").val(data.school.administrative_department);
          return $("#type").val(data.school.type);
        }
      }, "json");
    });
    $(".gender-selector").change(function(e) {
      var genderId;
      genderId = $(this).val();
      if (genderId === '3' || genderId === 3) {
        return $("#other-gender-wrapper").show();
      } else {
        return $("#other-gender-wrapper").hide();
      }
    });
    $(".area-selector").change(function(e) {
      var areaId;
      areaId = $(this).val();
      if (areaId === '3' || areaId === 3) {
        return $("#other-area-wrapper").show();
      } else {
        return $("#other-area-wrapper").hide();
      }
    });
    $(".fiocruz-prize-selector").change(function(e) {
      var answer;
      answer = $(this).val();
      if (answer === '1' || answer === 1) {
        return $("#fonts-wrapper").show();
      } else {
        return $("#fonts-wrapper").hide();
      }
    });
    $(".modality-selector").change(function(e) {
      var modalityID;
      modalityID = $(this).val();
      $('.modality-wrapper').hide();
      $('.modality-wrapper').find('input').prop('disabled', true);
      $('.modality-wrapper').find('select').prop('disabled', true);
      $('.modality-wrapper').find('textarea').prop('disabled', true);
      $("#modality-" + modalityID + "-wrapper").removeClass('hidden');
      $("#modality-" + modalityID + "-wrapper").show();
      $("#modality-" + modalityID + "-wrapper").find('input').prop('disabled', false);
      $("#modality-" + modalityID + "-wrapper").find('select').prop('disabled', false);
      return $("#modality-" + modalityID + "-wrapper").find('textarea').prop('disabled', false);
    });
    $(".category-selector").change(function(e) {
      var $select, categoryID;
      categoryID = $(this).val();
      $select = $('.years-selector');
      return $.ajax({
        type: "GET",
        url: "/category/" + categoryID + "/years",
        success: function(data) {
          $select.empty();
          $.each(data.years, function(key, value) {
            return $select.append('<option value=' + value.id + '>' + value.name + '</option>');
          });
          $select.val(null).trigger("change");
          return $select.focus();
        }
      }, "json");
    });
    $("#export-works").click(function(e) {
      var form;
      e.preventDefault();
      $("#_excel").val(1);
      form = $(this).parents('form:first');
      return form.submit();
    });
    $("#search").click(function(e) {
      var form;
      e.preventDefault();
      $("#_excel").val(0);
      form = $(this).parents('form:first');
      return form.submit();
    });
    return $(".sort").click(function(e) {
      var field, form, value;
      e.preventDefault();
      value = null;
      if ($(this).hasClass('desc')) {
        $(this).toggleClass('desc');
      } else if ($(this).hasClass('asc')) {
        $(this).toggleClass('asc');
        $(this).toggleClass('desc');
        value = "desc";
      } else {
        $(this).toggleClass('asc');
        value = "asc";
      }
      form = $('form:first');
      field = $(this).attr('id').split("-")[1];
      $('.sort-field').val(null);
      $("#" + field).val(value);
      return form.submit();
    });
  });

  this.appendError = function(selector, error) {
    selector.parent().addClass('has-error');
    selector.addClass('has-error');
    return selector.after("<span class=\"help-block\">" + error + "</span>");
  };

  this.sendFile = function(file) {
    var data;
    data = new FormData();
    data.append("file", file);
    return $.ajax({
      data: data,
      type: "POST",
      url: "/email/upload",
      cache: false,
      contentType: false,
      processData: false,
      success: function(url) {
        return $('.summernote').summernote('insertImage', url);
      }
    });
  };

}).call(this);

(function() {
  $(function() {
    var $addSubjectButton, $removeSubjectButton, $selectedSubjects, $subjectsList;
    $addSubjectButton = $('#add-subject');
    $removeSubjectButton = $('#remove-subject');
    $subjectsList = $("#subjects_list");
    $selectedSubjects = $("#subjects");
    $addSubjectButton.click(function(e) {
      var selected;
      selected = $subjectsList.find(':selected');
      if ((selected != null) && selected.val() !== '') {
        return $selectedSubjects.append(selected);
      }
    });
    return $removeSubjectButton.click(function(e) {
      var selected;
      selected = $selectedSubjects.find(':selected');
      if ((selected != null) && selected.val() !== '') {
        return $subjectsList.append(selected);
      }
    });
  });

}).call(this);

//# sourceMappingURL=app.js.map
