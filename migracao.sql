USE obsma;

ALTER TABLE obsma.works
    ADD COLUMN keywords VARCHAR(200),
    ADD COLUMN area TINYINT(1),
    ADD COLUMN other_area VARCHAR(255),
    ADD COLUMN has_fiocruz_material TINYINT(1),
    ADD COLUMN fiocruz_fonts TEXT;

ALTER TABLE obsma.users_teachers
    ADD COLUMN race VARCHAR(200),
    ADD COLUMN other_gender VARCHAR(255);